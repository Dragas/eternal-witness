package lt.saltyjuice.dragas.eternalwitness.persistence.common;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionFactoryProvider extends Thread implements SessionFactoryService
{
    private static final Logger LOG = LoggerFactory.getLogger(SessionFactoryProvider.class);
    private static SessionFactory sessionFactory;

    @Override
    public SessionFactory getSessionFactory()
    {
        if (sessionFactory == null)
        {
            StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure()
                    .build();
            try
            {
                sessionFactory = new MetadataSources(registry)
                        .buildMetadata()
                        .buildSessionFactory();
            }
            catch (Exception e)
            {
                LOG.error("Failed to load session factory", e);
                StandardServiceRegistryBuilder.destroy(registry);
            }
        }
        return sessionFactory;
    }

    @Override
    public void run()
    {
        sessionFactory.close();
    }
}
