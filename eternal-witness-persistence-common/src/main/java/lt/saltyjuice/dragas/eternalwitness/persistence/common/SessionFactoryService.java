package lt.saltyjuice.dragas.eternalwitness.persistence.common;

import org.hibernate.SessionFactory;

public interface SessionFactoryService
{
    public SessionFactory getSessionFactory();
}
