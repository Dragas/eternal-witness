package lt.saltyjuice.dragas.eternalwitness.persistence.common;

import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.RollbackException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class AbstractHibernateService
{
    private final SessionFactory sessionFactory;

    public AbstractHibernateService(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    protected SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    protected <T> T performReadingRunnable(ReadingSessionRunnable<T> runnable) throws RollbackException
    {
        T result = null;
        Session s = getSessionFactory().getCurrentSession();
        try {
            result = runnable.apply(s);
        }
        catch(Throwable e) {
            restartTransaction();
            throw e;
        }
        continueTransaction();
        return result;

    }

    protected void performSimpleRunnable(SessionRunnable runnable)
    {
        performReadingRunnable((session) ->
        {
            runnable.accept(session);
            return null;
        });
    }

    protected void continueTransaction()
    {
        Session session = getSessionFactory().getCurrentSession();
        session
                .getTransaction()
                .commit();
        session.beginTransaction();
    }

    protected void restartTransaction()
    {
        Session session = getSessionFactory().getCurrentSession();
        session
                .getTransaction()
                .rollback();
        session.beginTransaction();
    }

    protected <T> T getFirstResult(List<T> resultList, String errorMessage)
    {
        if(resultList.isEmpty()) {
            throw new NoSuchEntityException(errorMessage);
        }
        return resultList.get(0);
    }

    protected static interface SessionRunnable extends Consumer<Session>
    {
    }

    protected static interface ReadingSessionRunnable<T> extends Function<Session, T>
    {

    }
}
