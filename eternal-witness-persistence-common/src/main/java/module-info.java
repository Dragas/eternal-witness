import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryProvider;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

open module eternal.witness.persistence.common {
    exports lt.saltyjuice.dragas.eternalwitness.persistence.common;
    requires transitive org.hibernate.orm.core;
    requires transitive java.persistence;
    requires transitive java.naming;
    requires slf4j.api;
    requires eternal.witness.api;
    provides SessionFactoryService with SessionFactoryProvider;
}
