# EW-Deck-Downloader BOM

Fetches the deck from provided url using one of the
installed.

## Implementing downloader

Downloaders shouldn't be used on their own, but instead
installed into the downloader factory. By default,
downloaders use [OkHttp](https://square.github.io/okhttp/)
to download the decks. There's already a preconfigured
workflow in `AbstractOKHTTPDeckDownloader`, which you should
extend in general case. There are several methods that
might be interesting for implementations:

* `AbstractOKHTTPDeckDownloader#download(String)`
  * By default creates an `okhttp3.Call` object, which
  is wrapped by `#createCallable(okhttp3.Call)` and then
  submitted to underlying executor, returning a `Future<Decklist>`
  * Should be overrided to rewrite URLs in case they need
  some further adaption to actually download the decklist 
* `AbstractOKHTTPDeckDownloader#createCallable(okhttp3.Call)`
  * creates an callable object wrapping `okhttp3.Call`
  * By default returns a `AbstractOKHTTPDeckDownloader.DownloaderCallableWrapper`
  which holds references to provided `DeckService` and in general is the
  parsing and workhorse of the entire API.
* `DownloaderCallableWrapper#createDeckZones(String)`
  * Should return an array of up to two elements
  * Used to split the text blob between mainboard and sideboard blobs
  * By default splits at double newline
* `DownloaderCallableWrapper#createDeckZone(String, boolean)`
  * Transforms deck zone string to actual collection of MtgDeckCard by 
  line by calling `#splitToCountAndNameCombo`
  * Determines which lines should not be used by calling `#removeTrash`
* `DownloaderCallableWrapper#splitToCountAndNameCombo(String)`
  * Transforms a line from deck string to MtgDeckCard
  * by default the line is split at first space, first element
  used as count, while the second element is used as the card name
  * depends on presence of `DeckService` implementation
* `DownloaderCallableWrapper#removeTrash(String)`
  * Filters out the lines that cannot be used or do not impact the
  structure of the deck
  * By default, anything starting with `//` is filtered out

Unlike for Validators, these modules are too complex to be instantiated
via `ServiceLoader`, and as a result, do not need to implement such
loading mechanisms.
