package lt.saltyjuice.dragas.eternalwitness.deckdownloader.tappedout;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.api.AbstractOKHTTPDeckDownloader;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class TappedoutDeckDownloader extends AbstractOKHTTPDeckDownloader
{
    private static final Logger LOG = LoggerFactory.getLogger(TappedoutDeckDownloader.class);

    public TappedoutDeckDownloader(OkHttpClient client, ExecutorService executor)
    {
        super(
                client,
                executor
        );
    }

    @Override
    public Future<Decklist> download(String url)
    {
        String original = url;
        int questionMarkIndex = url.indexOf("?");
        if (questionMarkIndex != -1)
        {
            url = url.substring(0, questionMarkIndex);
        }
        url = String.format(
                "%s?fmt=dek",
                url
        );
        LOG.debug(
                "Rewriting url from {} to {}",
                original,
                url
        );
        return super.download(url);
    }

    @Override
    protected Callable<Decklist> createCallable(Call call)
    {
        return new TappedoutDownloaderWrapper(call);
    }

    @Override
    public String baseUrl()
    {
        return "https://tappedout.net/";
    }

    protected static class TappedoutDownloaderWrapper extends AbstractOKHTTPDeckDownloader.DownloaderCallableWrapper {

        public TappedoutDownloaderWrapper(Call call)
        {
            super(
                    call
            );
        }

        @Override
        public boolean removeTrash(String s)
        {
            return !s.startsWith("Sideboard:");
        }
    }
}
