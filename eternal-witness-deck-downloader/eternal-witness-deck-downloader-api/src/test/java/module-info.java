open module eternal.witness.deck.downloader.api {
    requires transitive eternal.witness.api;
    requires transitive okhttp3;
    requires okhttp3.mockwebserver;
    requires transitive slf4j.api;
    exports lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
