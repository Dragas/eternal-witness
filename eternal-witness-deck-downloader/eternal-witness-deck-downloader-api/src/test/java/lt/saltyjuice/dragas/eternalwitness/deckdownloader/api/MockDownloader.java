package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import okhttp3.OkHttpClient;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class MockDownloader extends GenericOKHTTPDownloader
{
    public MockDownloader(DeckService deckService, OkHttpClient client, ExecutorService executor, String baseUrl)
    {
        super(
                deckService,
                client,
                executor,
                baseUrl
        );
    }

    @Override
    public Future<Decklist> download(String url)
    {
        Decklist d = new Decklist();
        return CompletableFuture.completedFuture(d);
    }
}
