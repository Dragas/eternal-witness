package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MockDeckService implements DeckService
{
    private final Map<String, MtgCard> cardMap = new HashMap<>();
    @Override
    public Decklist createDecklist(Player player, String name, String deckUrl)
    {
        return null;
    }

    @Override
    public void removeDecklist(Decklist decklist)
    {

    }

    @Override
    public void attachCard(Decklist decklist, MtgCard card, int count, boolean sideboard)
    {

    }

    @Override
    public void detachCard(Decklist decklist, MtgCard card, boolean sideboard)
    {

    }

    @Override
    public Decklist getDeckList(Player player, String name)
    {
        return null;
    }

    @Override
    public MtgCard fetchCard(String cardName)
    {
        return cardMap.get(cardName);
    }

    public void putCard(MtgCard card) {
        cardMap.put(card.getName(), card);
    }

    @Override
    public Collection<Decklist> getPlayerDecklists(Player player)
    {
        return null;
    }
}
