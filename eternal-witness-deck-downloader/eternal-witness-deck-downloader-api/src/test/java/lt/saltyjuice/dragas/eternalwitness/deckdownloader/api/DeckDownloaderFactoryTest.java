package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DeckDownloaderFactoryTest
{
    private static final String TLD = "dicks.lan";
    private DeckDownloaderFactory deckDownloaderFactory;
    private static final String URL = String.format("https://%s/deck1", TLD);
    private DeckDownloader downloader;

    @BeforeEach
    public void setUp()
    {
        deckDownloaderFactory = new GenericDeckDownloaderFactory();
        downloader = new MockDownloader(
                null,
                null,
                null,
                URL
        );
        deckDownloaderFactory.install(downloader);
    }

    @Test
    public void factory_forUrlWithInstalledTld_returnsProperFuture() throws ExecutionException, InterruptedException
    {
        Future<Decklist> future = deckDownloaderFactory.download(URL);
        assertNotNull(future);
        Decklist result = future.get();
        assertNotNull(result);
    }

    @Test
    public void factory_forUrlWithoutInstalledTld_returnsNull() {
        Future<Decklist> future = deckDownloaderFactory.download(URL.replace("lan", "memes"));
        assertNull(future);
    }

    @Test
    public void factory_forUninstalledTld_returnsNull() {
        deckDownloaderFactory.uninstall(downloader);
        Future<Decklist> future = deckDownloaderFactory.download(URL.replace("lan", "memes"));
        assertNull(future);
    }

    @Test
    public void factory_forNotInstalledDownloader_returnsFuture() {
        DeckDownloader downloader = new MockDownloader(null, null, null, URL);
        deckDownloaderFactory.uninstall(downloader);
        Future<Decklist> future = deckDownloaderFactory.download(URL);
        assertNotNull(future);
    }
}
