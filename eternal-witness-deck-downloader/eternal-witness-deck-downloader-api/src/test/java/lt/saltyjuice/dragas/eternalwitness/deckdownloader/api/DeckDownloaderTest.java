package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DeckDownloaderTest
{
    private static final String TLD = "memes.tld";
    private static final MockDeckService MOCK_DECK_SERVICE = new MockDeckService();
    private static final OkHttpClient OKHTTP_CLIENT = new OkHttpClient.Builder().build();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private static final String DECK_1 = "/deck/1";
    private DeckDownloader deckDownloader;
    private MockWebServer webServer;
    private HttpUrl baseUrl;

    @BeforeAll
    public static void setUpSuite()
    {
        appendCard("Expedition Map");
        appendCard("Scuttling Doom Engine");
        appendCard("Forest");
        appendCard("Plains");
        appendCard("Swamp");
        appendCard("Siege Rhino");
        appendCard("Abrupt Decay");
        appendCard("Assassin's Trophy");
        appendCard("Ephemerate");
        appendCard("Path to Exile");
        appendCard("Path to Exile");
        appendCard("Cascading Cataracts");
        appendCard("Hissing Quagmire");
        appendCard("Marsh Flats");
        appendCard("Shambling Vent");
        appendCard("Stirring Wildwood");
        appendCard("Verdant Catacombs");
        appendCard("Windswept Heath");
        appendCard("Urza's Mine");
        appendCard("Urza's Power Plant");
        appendCard("Urza's Tower");
        appendCard("Golos, Tireless Pilgrim");
        appendCard("Sylvan Scrying");
        appendCard("All Is Dust");
        appendCard("All Is Dust");
        appendCard("Leyline of Sanctity");
        appendCard("Rest in Peace");
        appendCard("Dismember");
        appendCard("Fracturing Gust");

    }

    private static void appendCard(String cardName)
    {
        MtgCard card = new MtgCard();
        card.setName(cardName);
        MOCK_DECK_SERVICE.putCard(card);
    }

    @BeforeEach
    public void setUp() throws IOException
    {
        StringBuilder builder = new StringBuilder();
        try (final InputStream resourceAsStream = getClass()
                .getResourceAsStream("/deck.dck"))
        {
            Scanner sin = new Scanner(resourceAsStream);
            while (sin.hasNextLine())
            {
                builder.append(sin.nextLine());
                builder.append(System.lineSeparator());
            }
        }
        String body = builder.toString();
        webServer = new MockWebServer();
        webServer.start();
        baseUrl = webServer.url(DECK_1);
        deckDownloader = new GenericOKHTTPDownloader(
                MOCK_DECK_SERVICE,
                OKHTTP_CLIENT,
                EXECUTOR_SERVICE,
                TLD
        );
        webServer.enqueue(new MockResponse().setBody(body));
    }

    @Test
    public void downloader_forPreconfiguredUrl_fetchesDeck() throws ExecutionException, InterruptedException
    {
        Future<Decklist> dekclist = deckDownloader.download(baseUrl.toString());
        Decklist decklist = dekclist.get();
        assertNotNull(decklist);
        assertEquals(baseUrl.toString(), decklist.getDeckUrl());
        assertEquals(29, decklist.getCards().size());
    }

    @AfterEach
    public void tearDown() throws IOException, InterruptedException
    {
        RecordedRequest request = webServer.takeRequest();
        assertEquals(
                DECK_1,
                request.getPath()
        );
        webServer.shutdown();
    }
}
