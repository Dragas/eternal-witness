module eternal.witness.deck.downloader.api {
    requires transitive eternal.witness.api;
    requires transitive okhttp3;
    requires transitive slf4j.api;
    exports lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;
}
