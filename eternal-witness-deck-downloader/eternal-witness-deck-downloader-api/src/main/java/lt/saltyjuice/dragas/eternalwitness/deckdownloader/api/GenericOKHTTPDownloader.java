package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import okhttp3.OkHttpClient;

import java.util.concurrent.ExecutorService;

/**
 * It is entirely possible that default configuration is more than enough
 * to your average website to download a deck and parse it.
 */
public class GenericOKHTTPDownloader extends AbstractOKHTTPDeckDownloader
{
    private final String baseUrl;

    public GenericOKHTTPDownloader(DeckService deckService, OkHttpClient client, ExecutorService executor, String baseUrl)
    {
        super(
                client,
                executor
        );
        this.baseUrl = baseUrl;
    }

    @Override
    public String baseUrl()
    {
        return baseUrl;
    }
}
