package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Future;

/**
 * Synchronized tree map backed deck downloader factory that tests
 * the provided url against installed downloaders and attempts
 * to fetch the decklist using one of the downloaders.
 */
public class GenericDeckDownloaderFactory implements DeckDownloaderFactory
{
    private static final Logger LOG = LoggerFactory.getLogger(GenericDeckDownloaderFactory.class);

    private final Map<String, DeckDownloader> downloaderMap;

    public GenericDeckDownloaderFactory()
    {
        this(new TreeMap<>());
    }

    public GenericDeckDownloaderFactory(Map<String, DeckDownloader> backingMap)
    {
        this.downloaderMap = Collections.synchronizedNavigableMap(new TreeMap<>(backingMap));
    }

    @Override
    public void install(DeckDownloader downloader)
    {
        String baseUrl = getAuthorityString(downloader.baseUrl());
        if (downloaderMap.containsKey(baseUrl))
        {
            LOG.warn(
                    "There's already a downloader installed for {}",
                    baseUrl
            );
        }
        else
        {
            downloaderMap.put(
                    baseUrl,
                    downloader
            );
        }
    }

    private String getAuthorityString(String baseUrl)
    {
        URI uri = URI.create(baseUrl);
        return uri.getAuthority();
    }

    @Override
    public void uninstall(DeckDownloader downloader)
    {
        String baseUrl = getAuthorityString(downloader.baseUrl());
        DeckDownloader installed = downloaderMap.get(baseUrl);
        if (installed == downloader)
        {
            downloaderMap.remove(baseUrl);
            LOG.info(
                    "Removed downloader {} for {}",
                    downloader,
                    downloader.baseUrl()
            );
        }
        else
        {
            LOG.warn("Trying to uninstall a non installed downloader for {}",
                    downloader.baseUrl());
        }
    }

    @Override
    public Future<Decklist> download(String url)
    {
        String authority = getAuthorityString(url);
        DeckDownloader downloader = downloaderMap.get(authority);
        if (downloader != null)
        {
            return downloader.download(url);
        }
        return null;//return super.download(uri.toString());
    }
}
