package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Deck downloader main interface.
 *
 * Deck downloaders are adapters of provided HTTP URLs that
 * perform a get request and adapt the result into a decklist.
 * Since the operation might take a while (due to network
 * being weird), the implementations must return a future
 * of the decklist which would eventually return the actual
 * decklist behind it.
 *
 * Implementations should return the base url, in start of
 * string format, to identify which
 *
 * Should not be used directly, but instead via {@link DeckDownloaderFactory},
 * unless you are absolutely sure that only a single type of
 * URLs will come your way.
 */
public interface DeckDownloader
{
    /**
     * Attempts to fetch the URL and parse it to the decklist
     * so that it could be ingested in application friendly
     * way.
     * @param url url to download
     * @return future of decklist
     * @throws RuntimeException when it can't handle the url
     */
    Future<Decklist> download(String url);

    String baseUrl();

    /**
     * Defines the interface for parsing the text blob that was somehow
     * fetched from external source into {@link Decklist} object.
     *
     */
    interface CallableDownloaderWrapper extends Callable<Decklist> {
        /**
         * Provides the means to create an array of "zones"
         * where particular cards reside in a deck. Implementations
         * may opt to change how the zones are created. By
         * default, an empty line is used as a separator to
         * in the response body to determine the split between
         * mainboard and sideboard of the deck.
         * @param body response body as a string from the provided URL
         * @return array of two elements: [main deck string, side deck string]
         */
        String[] createDeckZones(String body);
        /**
         * Converts a zoned string into a collection of {@link MtgDeckCard}.
         * By default accepts the format defined in {@link #splitToCountAndNameCombo(String)}
         * and uses that format to split the text blob into lines
         * of count and card name combos. By default, depends on {@link DeckService}
         * to get the actual card from somewhere.
         * @param body string body to convert
         * @param sideboard denotes whether or not cards should have sideboard flag
         * @return collection of mtg deck cards
         */
        Collection<MtgDeckCard> createDeckZone(String body, boolean sideboard);
        /**
         * Maps the line as extracted from the deck zone blob
         * into an actual card in a deck that can be used by
         * the application in a more meaningful way.
         * By default accepts the following format:
         * [int, count] [String, cardName][end of line].
         * As a result, the first integer that appears in the
         * line is taken as is, while the rest of the line
         * is used as a card name to get from provided {@link DeckService}
         * @param s the string line
         * @return {@link MtgDeckCard} object representing the card from the line
         */
        MtgDeckCard splitToCountAndNameCombo(String s);
        /**
         * Helper method to filter out the trash from the
         * text blob so that the parsing parts would not break.
         * @param s string to test
         * @return true, when that string is usable by the parser, otherwise false
         */
        boolean removeTrash(String s);
    }
}
