package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;

import java.util.concurrent.Future;

/**
 * Deck downloader factory interface.
 *
 * Since there will be lot of implementations for
 * downloaders (each site has their own format, and
 * means to download them), there needs to be some
 * sort of implementation to handle how to pick
 * which downloader to use and invoke it to convert
 * the URL to a future.
 */
public interface DeckDownloaderFactory
{
    void install(DeckDownloader downloader);

    void uninstall(DeckDownloader downloader);

    Future<Decklist> download(String url);
}
