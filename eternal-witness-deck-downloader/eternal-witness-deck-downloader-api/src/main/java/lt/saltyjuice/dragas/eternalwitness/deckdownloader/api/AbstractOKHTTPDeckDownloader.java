package lt.saltyjuice.dragas.eternalwitness.deckdownloader.api;

import lt.saltyjuice.dragas.eternalwitness.api.exception.EternalWitnessExcepion;
import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Base class for all downloaders.
 *
 * This class defines some structure and helper methods to
 * download and parse the decklists.
 */
public abstract class AbstractOKHTTPDeckDownloader implements DeckDownloader
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractOKHTTPDeckDownloader.class);
    private final OkHttpClient client;
    private final ExecutorService executor;

    public AbstractOKHTTPDeckDownloader(OkHttpClient client, ExecutorService executor)
    {
        this.client = client;
        this.executor = executor;
    }

    protected ExecutorService getExecutor() {
        return executor;
    }

    @Override
    public Future<Decklist> download(String url)
    {
        LOG.debug("creating future for url {}", url);
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        return getExecutor().submit(
                createCallable(call)
        );
    }

    /**
     * Returns a callable wrapper for the decklist to be downloaded.
     * If implementations need specific handling in how their decks
     * should be parsed, they should implement {@link DownloaderCallableWrapper}
     * and override this call returning it.
     *
     * @param call call provided by okhttp
     * @return callable that can be used by {@link ExecutorService} to create a future.
     */
    protected Callable<Decklist> createCallable(Call call)
    {
        LOG.debug("creating wrapper for call {}", call);
        return new AbstractOKHTTPDeckDownloader.DownloaderCallableWrapper(
                call
        );
    }

    protected static class DownloaderCallableWrapper implements DeckDownloader.CallableDownloaderWrapper
    {
        private static final Logger LOG = LoggerFactory.getLogger(DownloaderCallableWrapper.class);
        private final Call okhttpcall;

        public DownloaderCallableWrapper(Call call)
        {
            this.okhttpcall = call;
        }

        protected Call getOkhttpcall() {
            return okhttpcall;
        }

        @Override
        public Decklist call() throws Exception
        {
            LOG.debug("Fetching deck...");
            final Response response = okhttpcall.execute();
            LOG.info("Request made");
            try (final ResponseBody responseBody = response
                    .body())
            {
                LOG.debug("Extracting response");
                if (!response.isSuccessful())
                {
                    throw new EternalWitnessExcepion(String.format(
                            "Failed to fetch %s. Reason: %s",
                            okhttpcall
                                    .request()
                                    .url()
                                    .uri(),
                            responseBody
                                    .string()
                    ));
                }
                LOG.info("Response extracted");
                String body = responseBody
                        .string();
                String[] mainSideCombo = createDeckZones(
                        body
                );
                Collection<MtgDeckCard> deck = new ArrayList<>();
                if(mainSideCombo.length > 0) {
                    deck = createDeckZone(
                            mainSideCombo[0],
                            false
                    );
                }
                else {
                    LOG.warn("Deck is empty?");
                }
                if (mainSideCombo.length == 2)
                {
                    Collection<MtgDeckCard> sideboard = createDeckZone(
                            mainSideCombo[1],
                            true
                    );
                    deck.addAll(sideboard);
                }
                else {
                    LOG.warn("Deck has no sideboard or more zones than 2, ignoring");
                }
                Decklist decklist = new Decklist();
                decklist.setCards(
                        deck
                );
                decklist.setDeckUrl(
                        okhttpcall
                                .request()
                                .url()
                                .uri()
                                .toString()
                );
                return decklist;
            }
        }


        public String[] createDeckZones(String body)
        {
            LOG.debug("Creating deck zones");
            return body.split(
                    "(\r?\n){2}",
                    2
            );
        }


        public Collection<MtgDeckCard> createDeckZone(String body, boolean sideboard)
        {
            LOG.debug("Creating zone for sideboard: {}", sideboard);
            Collection<MtgDeckCard> deck = Arrays
                    .stream(body.split("\r?\n"))
                    .filter(this::removeTrash)
                    .map(this::splitToCountAndNameCombo)
                    .peek(it -> it.setSideboard(sideboard))
                    .collect(Collectors.toList());
            return deck;
        }


        public MtgDeckCard splitToCountAndNameCombo(String s)
        {
            LOG.debug("Creating an MtgDeckCard from {}", s);
            String[] combo = s.split(
                    " ",
                    2
            );
            int count = Integer.parseInt(combo[0]);
            String name = combo[1];
            MtgCard card = new MtgCard();
            card.setName(name);
            MtgDeckCard deckCard = new MtgDeckCard();
            deckCard.setCard(card);
            deckCard.setCount(count);
            return deckCard;
        }


        public boolean removeTrash(String s)
        {
            return !s.startsWith("//");
        }
    }
}
