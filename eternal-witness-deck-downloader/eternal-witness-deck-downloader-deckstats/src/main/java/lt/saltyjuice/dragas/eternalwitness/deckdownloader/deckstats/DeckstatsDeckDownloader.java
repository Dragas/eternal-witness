package lt.saltyjuice.dragas.eternalwitness.deckdownloader.deckstats;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.api.AbstractOKHTTPDeckDownloader;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class DeckstatsDeckDownloader extends AbstractOKHTTPDeckDownloader
{
    private static final Logger LOG = LoggerFactory.getLogger(DeckstatsDeckDownloader.class);
    public DeckstatsDeckDownloader(OkHttpClient client, ExecutorService executor)
    {
        super(
                client,
                executor
        );
    }

    @Override
    public Future<Decklist> download(String url)
    {
        String original = url;
        int questionMarkIndex = url.indexOf("?");
        if (questionMarkIndex != -1)
        {
            url = url.substring(0, questionMarkIndex);
        }
        url = String.format(
                "%s?export_mtgo=1",
                url
        );
        LOG.debug(
                "Rewriting url from {} to {}",
                original,
                url
        );
        return super.download(url);
    }

    @Override
    public String baseUrl()
    {
        return "https://deckstats.net/";
    }
}
