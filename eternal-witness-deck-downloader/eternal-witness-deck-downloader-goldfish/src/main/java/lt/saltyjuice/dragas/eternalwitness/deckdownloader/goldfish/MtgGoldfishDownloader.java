package lt.saltyjuice.dragas.eternalwitness.deckdownloader.goldfish;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.api.AbstractOKHTTPDeckDownloader;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class MtgGoldfishDownloader extends AbstractOKHTTPDeckDownloader
{
    private static final Logger LOG = LoggerFactory.getLogger(MtgGoldfishDownloader.class);

    public MtgGoldfishDownloader(OkHttpClient client, ExecutorService executor)
    {
        super(
                client,
                executor
        );
    }

    @Override
    public Future<Decklist> download(String url)
    {
        String original = url;
        url = url.replace("/deck/", "/deck/download/");
        LOG.debug(
                "Rewriting url from {} to {}",
                original,
                url
        );
        return super.download(url);
    }

    @Override
    public String baseUrl()
    {
        return "https://www.mtggoldfish.com/";
    }
}
