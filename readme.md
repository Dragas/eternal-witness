# Eternal Witness
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=lt.saltyjuice.dragas%3Aeternal-witness-bom&metric=coverage)](https://sonarcloud.io/dashboard?id=lt.saltyjuice.dragas%3Aeternal-witness-bom)

Fantasy league manager for Magic the Gathering tournaments.

## Usage

Invite the bot using the following link:
```
https://discord.com/api/oauth2/authorize?client_id=708921443328458763&permissions=268503104&scope=bot
```

After you invite eternal witness, make sure that it can only
read one channel where you, the owner, can write. This is to
prevent sniping which channel would the bot be installed to
and prevent everyone and their mother from spam hosting the
tournaments.

To "install" the bot into your server, invoke the following

```
[mention eternal witness] install [#channel]
```

This configures the bot to write to `#channel` when you host
tournaments on your server. To actually host a tournament,
invoke the following:

```
[mention eternal witness] host [deadline, iso8601 date time format] [start, iso8601 date time format, defaults to deadline] [emoji] 
``` 
With the above you must upload a file with up to 2k characters
in length. This is to prevent discord from throwing a fit at
you for writing too long messages because the command on its
own takes quite a bit. In the text file, while there is no format,
since its just dumped to `#channel`, it's recommended that you
include the basic information that you wrote when creating the
tournament. Personally I do it in the following format:
```
DATE: [date]
TIME: [start time]
Deck submission deadline: [relative to start time]
React with [emoji] to join the tournament
```
The bot will notify you in the channel that you invoked install in
whenever someone joins the "queue" for that tournament. The queue
is a special table, which contains the people that bot expects
to receive submissions from via private message. Players are
removed from the queue when they respond in predefined
format and then are granted the role that was created by
the bot during the tournament creation. When the player submits
a deck, it is written in the channel that you invoked `install`
command in.

## Self hosting/Development

To fire up the bot you will need Postgresql database server 11.5+.
Then, change the hibernate.cfg.xml files to target your database.
In addition, change the database credentials in development profile
in pom.xml. Finally, invoke the following commands:

```shell script
./mvnw -f eternal-witness-persistence flyway:migrate -P development
./mvnw -f eternal-witness-discord/discord-persistence flyway:migrate -P development
```

## Preloading database with data

Scryfall.json file is a bit large too be versioned, so you will need
to get it yourself. Then, use the [Churner](churner) module to generate
sql scripts and run them via flyway against the database. 

