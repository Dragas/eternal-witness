package lt.saltyjuice.dragas.eternalwitness.utility.churner;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Main
{
    private static final Map<String, MtgCard> cards = new TreeMap<>();
    private static final Map<String, MtgSet> sets = new TreeMap<>();
    private static final Map<String, MtgType> types = new TreeMap<>();

    public static void main(String[] args) throws IOException
    {
        Map<MtgSet, List<MtgCardSet>> setsmap = extractCardsFromProvidedFile(args[0]);
        cards.clear();
        sets.clear();
        types.clear();
        VelocityEngine engine = getVelocityEngine();
        final Template setInsertTemplate = engine.getTemplate("set_insert_template.vm");
        final Template cardInsertTemplate = engine.getTemplate("cards_insert_template.vm");
        final Template typeInsertTemplate = engine.getTemplate("types_insert_template.vm");
        final Template cardToTypeInsertTemplate = engine.getTemplate("card_to_type_insert_template.vm");
        final Template cardToSetInsertTemplate = engine.getTemplate("card_to_set_insert_template.vm");
        setsmap
                .entrySet()
                .stream()
                .peek((it) ->
                {
                    String setShortName = it
                            .getKey()
                            .getShortName();
                    if("con".equals(setShortName)) {
                        setShortName = "c0n";
                    }
                    File topDirectory = new File(String.format(
                            "sets/%s",
                            setShortName
                    ));
                    if (!topDirectory.exists())
                    {
                        topDirectory.mkdirs();
                    }
                })
                .parallel()
                .flatMap((it) ->
                {
                    return Stream.of(
                            createCardsContext(
                                    it.getValue(),
                                    cardInsertTemplate
                            ),
                            createSetContext(
                                    it.getKey(),
                                    setInsertTemplate
                            ),
                            createTypeContext(
                                    it.getValue(),
                                    typeInsertTemplate
                            ),
                            createCardTypeContext(
                                    it.getValue(),
                                    cardToTypeInsertTemplate
                            ),
                            createCardSetContext(
                                    it.getValue(),
                                    cardToSetInsertTemplate
                            )
                    );
                })
                .forEach((it) ->
                {
                    try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(it
                            .remove("filename")
                            .toString()),
                            StandardCharsets.UTF_8
                    ))
                    {
                        Template template = (Template) it.remove("template");
                        template.merge(
                                it,
                                writer
                        );
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(
                                "Aborting. Please clean up by hand",
                                e
                        );
                    }
                });
    }

    private static VelocityContext createCardSetContext(List<MtgCardSet> cards, Template template)
    {
        VelocityContext context = new VelocityContext();
        String setShortName = cards
                .get(0)
                .getSet()
                .getShortName();
        List<Map<String, String>> cardsMetadata = cards
                .stream()
                .map(it ->
                {
                    Map<String, String> cardContext = new HashMap<>();
                    cardContext.put(
                            "card_name",
                            psqlEscapeString(it
                                    .getCard()
                                    .getName())
                    );
                    cardContext.put(
                            "card_number",
                            it.getCardNumber()
                    );
                    cardContext.put(
                            "card_rarity",
                            it.getRarity()
                    );
                    return cardContext;
                })
                .collect(Collectors.toList());
        context.put("set_short_name", setShortName);
        context.put(
                "cards",
                cardsMetadata
        );
        prepareContextMetadata(
                context,
                template,
                createFilename(
                        setShortName,
                        "5_insert_card_set_definition"
                )
        );
        return context;
    }

    private static VelocityContext createCardTypeContext(List<MtgCardSet> setCards, Template template)
    {
        VelocityContext context = new VelocityContext();
        List<Map<String, String>> cardToTypeCombos = setCards
                .stream()
                .flatMap((it) ->
                {
                    Map<String, String> metadataMap = new HashMap<>();
                    metadataMap.put(
                            "card_name",
                            psqlEscapeString(it
                                    .getCard()
                                    .getName())
                    );
                    return it
                            .getCard()
                            .getTypes()
                            .stream()
                            .map(MtgCardType::getType)
                            .map(type ->
                            {
                                Map<String, String> typeMetaMap = new HashMap<>(metadataMap);
                                typeMetaMap.put(
                                        "type_name",
                                        type.getName()
                                );
                                return typeMetaMap;
                            });
                })
                .collect(Collectors.toList());
        context.put(
                "card_type_combo",
                cardToTypeCombos
        );
        prepareContextMetadata(
                context,
                template,
                createFilename(
                        setCards
                                .get(0)
                                .getSet()
                                .getShortName(),
                        "4_insert_card_type_definition"
                )
        );
        return context;
    }

    private static VelocityContext createTypeContext(List<MtgCardSet> type, Template typeInsertTemplate)
    {
        VelocityContext context = new VelocityContext();
        List<String> types =
                type
                        .stream()
                        .map(MtgCardSet::getCard)
                        .map(MtgCard::getTypes)
                        .flatMap(Collection::stream)
                        .distinct()
                        .map(MtgCardType::getType)
                        .map(MtgType::getName)
                        .collect(Collectors.toList());
        context.put(
                "types",
                types
        );
        prepareContextMetadata(
                context,
                typeInsertTemplate,
                createFilename(
                        type
                                .get(0)
                                .getSet()
                                .getShortName(),
                        "3_insert_type_definition"
                )
        );
        return context;
    }

    private static Map<MtgSet, List<MtgCardSet>> extractCardsFromProvidedFile(String arg) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        File scryfallJson = new File(arg);
        try(JsonParser parser = factory.createParser(scryfallJson))
        {
            parser.nextToken();
            parser.nextToken();
            Iterator<HashMap<String, Object>> iterator = parser.readValuesAs(
                    new TypeReference<HashMap<String, Object>>()
                    {
                    }
            );
            return StreamSupport
                    .stream(
                            Spliterators.spliteratorUnknownSize(
                                    iterator,
                                    Spliterator.IMMUTABLE
                            ),
                            parser.canParseAsync()
                    )
                    .map(Main::createMtgCardSet)
                    .parallel()
                    .collect(Collectors.groupingBy(MtgCardSet::getSet));
        }
    }

    private static VelocityEngine getVelocityEngine() throws IOException
    {
        VelocityEngine engine = new VelocityEngine();
        Properties velocityProperties = new Properties();
        velocityProperties.load(Main.class.getResourceAsStream("/velocity.properties"));
        engine.setProperties(velocityProperties);
        engine.init();
        return engine;
    }

    private static VelocityContext createCardsContext(List<MtgCardSet> cards, Template cardInsertTemplate)
    {
        VelocityContext context = new VelocityContext();
        List<String> cardNames = cards
                .stream()
                .map(MtgCardSet::getCard)
                .map(MtgCard::getName)
                .distinct()
                .map(Main::psqlEscapeString)
                .collect(Collectors.toList());
        context.put(
                "names",
                cardNames
        );
        prepareContextMetadata(
                context,
                cardInsertTemplate,
                createFilename(
                        cards
                                .get(0)
                                .getSet()
                                .getShortName(),
                        "1_insert_card_definition"
                )
        );
        return context;
    }

    private static VelocityContext createSetContext(MtgSet it, Template template)
    {
        VelocityContext context = new VelocityContext();
        context.put(
                "full_name",
                psqlEscapeString(it.getName())
        );
        context.put(
                "short_name",
                it.getShortName()
        );
        context.put(
                "release",
                it
                        .getRelease()
                        .format(DateTimeFormatter.ISO_DATE)
        );
        prepareContextMetadata(
                context,
                template,
                createFilename(
                        it.getShortName(),
                        "2_insert_set_definition"
                )
        );
        return context;
    }

    private static void prepareContextMetadata(VelocityContext context, Template template, String filename)
    {
        context.put(
                "filename",
                filename
        );
        context.put(
                "template",
                template
        );
    }

    private static String createFilename(String shortName, String filename)
    {
        if ("con".equals(shortName))
        {
            shortName = "c0n";
        }
        return String.format(
                "sets/%1$s/R__%1$s_%2$s.sql",
                shortName,
                filename
        );
    }

    private static String psqlEscapeString(String value)
    {
        return value.replace(
                "'",
                "''"
        );
    }

    private static MtgCardSet createMtgCardSet(HashMap<String, Object> metadata)
    {
        MtgCard card = getMtgCard(metadata);
        MtgSet set = getMtgSet(metadata);
        MtgCardSet cardSet = new MtgCardSet();
        cardSet.setCard(card);
        cardSet.setSet(set);
        cardSet.setCardNumber(metadata
                .get("collector_number")
                .toString());
        cardSet.setRarity(metadata
                .get("rarity")
                .toString());
        return cardSet;
    }

    private static MtgSet getMtgSet(HashMap<String, Object> metadata)
    {
        String setName = metadata
                .get("set_name")
                .toString();
        MtgSet set = sets.get(setName);
        if (set == null)
        {
            set = createSet(
                    setName,
                    metadata
                            .get("released_at")
                            .toString(),
                    metadata
                            .get("set")
                            .toString()
            );
            sets.put(
                    setName,
                    set
            );
        }
        return set;
    }

    private static MtgCard getMtgCard(HashMap<String, Object> metadata)
    {
        String cardName = metadata
                .get("name")
                .toString();
        MtgCard mtgCard = cards.get(cardName);
        if (mtgCard == null)
        {
            mtgCard = createCard(
                    cardName,
                    getTypes(metadata
                            .get("type_line")
                            .toString())
            );
            cards.put(
                    cardName,
                    mtgCard
            );
        }
        return mtgCard;
    }

    private static Collection<MtgCardType> getTypes(String typeLine)
    {
        String[] types = typeLine.split(" ");
        List<String> typeList = Arrays.asList(types);
        return typeList
                .stream()
                .filter(it -> !"—".equals(it))
                .map(Main::getType)
                .map(Main::wrapType)
                .collect(Collectors.toList());
    }

    private static MtgCardType wrapType(MtgType mtgType)
    {
        MtgCardType type = new MtgCardType();
        type.setType(mtgType);
        return type;
    }

    private static MtgType getType(String typeName)
    {
        return types.computeIfAbsent(
                typeName,
                Main::createType
        );
    }

    private static MtgType createType(String typeName)
    {
        MtgType type = new MtgType();
        type.setName(typeName);
        return type;
    }

    private static MtgSet createSet(String set, String release, String setShorthand)
    {
        MtgSet mtgSet = new MtgSet();
        mtgSet.setName(set);
        mtgSet.setRelease(
                LocalDate.parse(release)
        );
        mtgSet.setShortName(setShorthand);
        return mtgSet;
    }

    private static MtgCard createCard(String cardName, Collection<MtgCardType> type_line)
    {
        MtgCard card = new MtgCard();
        card.setName(cardName);
        card.setTypes(type_line);
        type_line.forEach(it -> it.setCard(card));
        return card;
    }
}
