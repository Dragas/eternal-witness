# Churner

Churner is built for one thing and one thing only. 
It takes in scryfall data dump file (preferably 
single language) and converts it per set into flyway
migrations for use in eternal witness.

## Usage

In case you're building from source, you can just package
it:
```shell script
./mvnw package -f churner
```  
Fetch the binaries from target folder (along with dependency folder)
and run against the churner jar file like so
```shell script
java -jar churner-1.0-SNAPSHOT.jar [scryfall.json]
```
Where `[scryfall.json]` is path to scryfall json file that you want
to consume.

The output of application is a `sets` folder (in working dir), which contains
folders for sets that the provided json file contained. Each
folder contains 5 files:
```shell script
R__[set]_1_insert_card_definitions.sql -- contains cards that were printed in that set
R__[set]_2_insert_set_definitions.sql -- contains the sets that were printed in that set (kinda moot, i know)
R__[set]_3_insert_type_definitons.sql -- contains type definitions that were printed in that set
R__[set]_4_insert_card_type_definition.sql -- contains the many to many relationship between cards and their types
R__[set]_5_insert_card_set_definition.sql -- contains the many to many relationship between cards and them appearing in the set
```
Then you can use the eternal-witness-persistence module's flyway configuration
to insert them all into the database. These are repeatable migrations,
so they don't affect database version and are always run after the
regular database migrations. At the time of writing, migration takes 
about 5 minutes.
