package lt.saltyjuice.dragas.eternalwitness.persistence.service;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Format;
import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.api.model.Rule;
import lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class FormatServiceTest
{
    private static final SessionFactory SESSION_FACTORY = ServiceLoader
            .load(SessionFactoryService.class)
            .findFirst()
            .get()
            .getSessionFactory();
    private static final String FORMAT_NAME = "Vanguard";
    private static final String FORMAT_REPETITION = "format.repetition";
    private static final String FORMAT_VALUE = "memes";
    private static final int RULE_COUNT = 5;
    private static FormatService formatService;
    private static Session session;

    @BeforeAll
    public static void setUpSuite()
    {
        formatService = ServiceLoader
                .load(FormatService.class)
                .findFirst()
                .get();
        session = DatabaseTestUtility.getSession(SESSION_FACTORY);
    }

    @BeforeEach
    public void setUp()
    {
        session.beginTransaction();
    }

    @AfterEach
    public void tearDown()
    {
        Transaction t = session.getTransaction();
        Query q = session.createQuery("Delete Format");
        q.executeUpdate();
        session
                .getTransaction()
                .commit();
    }

    @Test
    public void service_createsFormatFromName_succeeds()
    {
        Format format = formatService.createFormat(FORMAT_NAME);
        assertEquals(
                FORMAT_NAME,
                format.getName(),
                "format name should not be modified"
        );
        assertNotEquals(
                0,
                format.getId(),
                "Id must not be 0"
        );
    }

    @Test
    public void service_deletesFormat_throwsWhenTryingToFindNonExisting()
    {
        Format format = formatService.createFormat(FORMAT_NAME);
        formatService.deleteFormat(format);
        SESSION_FACTORY.getCurrentSession().flush();
        executeFetchByNameThrowsTest(FORMAT_NAME);
    }

    @Test
    public void service_fetchesFormatByNonExistantName_throws() {
        executeFetchByNameThrowsTest(FORMAT_NAME);
    }

    @Test
    public void service_createFormatByDuplicateName_throws() {
        formatService.createFormat(FORMAT_NAME);
        assertThrows(
                DuplicateEntityException.class,
                () -> formatService.createFormat(FORMAT_NAME),
                "should throw"
        );
    }

    private void executeFetchByNameThrowsTest(String formatName)
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> formatService.fetchFormatByName(formatName),
                "should throw"
        );
    }

    @Test
    public void service_fetchesExistingFormat_returnsFormat()
    {
        Format format = formatService.createFormat(FORMAT_NAME);
        executeFetchByNameTest(
                format,
                FORMAT_NAME
        );
    }

    private void executeFetchByNameTest(Format format, String formatName)
    {
        Format fetched = formatService.fetchFormatByName(formatName);
        assertEquals(format.getId(), fetched.getId(), "ids should match");
    }

    @Test
    public void service_fetchesExistingRule_returnsRule()
    {
        Rule rule = formatService.getRule(FORMAT_REPETITION);
        assertNotEquals(
                0,
                rule.getId(),
                "id should not be 0"
        );
    }

    @Test
    public void service_attachesRule()
    {
        Format format = formatService.createFormat(FORMAT_NAME);
        Rule rule = formatService.getRule(FORMAT_REPETITION);

        formatService.attachRule(
                format,
                rule,
                FORMAT_VALUE
        );
        assertFalse(
                format
                        .getRules()
                        .isEmpty(),
                "should not be empty"
        );
        final FormatRule formatRule = format
                .getRules()
                .stream()
                .findFirst()
                .get();
        assertNotEquals(
                0,
                formatRule
                        .getId(),
                "Should not be 0"
        );
        assertEquals(
                FORMAT_VALUE,
                formatRule.getValue(),
                "value should be equal"
        );
        assertEquals(
                rule,
                formatRule.getRule(),
                "rule should be equal"
        );
        assertEquals(
                format,
                formatRule.getFormat(),
                "format should be equal"
        );
    }

    @Test
    public void service_attachesSameRuleMultipleTimes() {
        Format format = formatService.createFormat(FORMAT_NAME);
        Rule rule = formatService.getRule(FORMAT_REPETITION);
        for(int i = 0; i < RULE_COUNT; i++) {
            formatService.attachRule(
                    format,
                    rule,
                    FORMAT_VALUE
            );
        }
        assertEquals(RULE_COUNT, format.getRules().size(), "size should match");
    }

    @Test
    public void service_detachesSameRuleMultipleTimes() {
        Format format = formatService.createFormat(FORMAT_NAME);
        Rule rule = formatService.getRule(FORMAT_REPETITION);
        for(int i = 0; i < RULE_COUNT; i++) {
            formatService.attachRule(
                    format,
                    rule,
                    FORMAT_VALUE
            );
        }
        formatService.detachRule(format, rule);
        assertTrue(format.getRules().isEmpty(), "should be empty");
    }

    @Test
    public void service_detachesRule()
    {
        Format format = formatService.createFormat(FORMAT_NAME);
        Rule rule = formatService.getRule(FORMAT_REPETITION);
        formatService.attachRule(
                format,
                rule,
                FORMAT_VALUE
        );
        formatService.detachRule(
                format,
                rule
        );
        assertTrue(format.getRules().isEmpty(), "should be empty");
    }
}
