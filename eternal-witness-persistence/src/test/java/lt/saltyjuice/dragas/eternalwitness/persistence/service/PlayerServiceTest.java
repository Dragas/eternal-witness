package lt.saltyjuice.dragas.eternalwitness.persistence.service;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.util.ServiceLoader;

public class PlayerServiceTest
{
    private static final SessionFactory SESSION_FACTORY = ServiceLoader
            .load(SessionFactoryService.class)
            .findFirst()
            .get()
            .getSessionFactory();
    private static final String NAME = "Dragas";
    private static PlayerService service;
    private static Session session;

    @BeforeAll
    public static void setSuite()
    {
        service = ServiceLoader
                .load(PlayerService.class)
                .findFirst()
                .get();
        session = DatabaseTestUtility.getSession(SESSION_FACTORY);
    }

    @BeforeEach
    public void setUp() {
        session.beginTransaction();
    }

    @Test
    public void serviceCreatesPlayerWithProvidedName()
    {
        Player result = service.createPlayer(
                NAME,
                ""
        );
        Assertions.assertEquals(
                NAME,
                result.getName(),
                "Names are not equal"
        );
        Assertions.assertNotEquals(
                0L,
                result.getId(),
                "Player did not get assigned an id"
        );
    }

    @Test
    public void serviceThrowsWhenANonUniqueNameIsProvided()
    {
        service.createPlayer(
                NAME,
                ""
        );
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> service.createPlayer(
                        NAME,
                        ""
                ),
                "Service must throw duplicate name exception when such a player already exists"
        );
    }

    @Test
    public void serviceFetchesCreatedPlayerByName()
    {
        Player result = service.createPlayer(
                NAME,
                ""
        );
        Player fetched = service.fetchPlayerByName(NAME);
        Assertions.assertEquals(
                result.getId(),
                fetched.getId(),
                "The two obtained models are not the same"
        );
    }

    @Test
    public void serviceFetchesCreatedPlayerById()
    {
        Player result = service.createPlayer(
                NAME,
                ""
        );
        Player fetched = service.fetchPlayerById(result.getId());
        Assertions.assertEquals(
                result.getId(),
                fetched.getId(),
                "The two obtained models are not the same"
        );
    }

    @Test
    public void serviceThrowsWhenTryingToFetchNonExistingPlayerByName()
    {
        Assertions.assertThrows(
                NoSuchEntityException.class,
                () -> service.fetchPlayerByName(NAME),
                "Service must throw when player does not exist"
        );
    }

    @Test
    public void serviceThrowsWhenTryingToFetchNonExistingPlayerById()
    {
        Assertions.assertThrows(
                NoSuchEntityException.class,
                () -> service.fetchPlayerById(-1),
                "Service must throw when player does not exist"
        );
    }

    @Test
    public void nothingHappensWhenTryingToRemoveNonExistingPlayer()
    {
        Player unamanaged = new Player();
        service.removePlayer(unamanaged);
    }

    @AfterEach
    public void tearDown()
    {
        session.getTransaction();
        Query q = session.createQuery("Delete Player");
        q.executeUpdate();
        session
                .getTransaction()
                .commit();
    }

    @AfterAll
    public static void tearDownAll() {
        DatabaseTestUtility.removeSession(session);
    }
}
