package lt.saltyjuice.dragas.eternalwitness.persistence.service;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.util.Collection;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Assumes that player service is properly implemented
 */
public class DeckServiceTest
{
    private static final String DECK_NAME = "Dredge";
    private static final boolean SIDEBOARD = true;
    private static final int COUNT = 1;
    private static final int NISSAS_DOG = 1;
    private static final String WORT = "Manabarbs";
    private static final SessionFactory SESSION_FACTORY = ServiceLoader
            .load(SessionFactoryService.class)
            .findFirst()
            .get()
            .getSessionFactory();
    private static final String PARTIAL_FUSED_NAME = "Fae of Wishes";
    private static DeckService deckService;
    private static PlayerService playerService;
    private static Player PLAYER;
    private static Session session;

    @BeforeAll
    public static void setupSuite()
    {
        playerService = ServiceLoader
                .load(PlayerService.class)
                .findFirst()
                .get();
        deckService = ServiceLoader
                .load(DeckService.class)
                .findFirst()
                .get();
        session = DatabaseTestUtility.getSession(SESSION_FACTORY);
        session.beginTransaction();
        PLAYER = playerService.createPlayer(
                "Dragas",
                ""
        );
        session.getTransaction().commit();
    }

    @AfterAll
    public static void tearDownAll()
    {
        session.beginTransaction();
        session.update(PLAYER);
        playerService.removePlayer(PLAYER);
        session.flush();
        session.getTransaction().commit();
        DatabaseTestUtility.removeSession(session);
    }

    @BeforeEach
    public void setUp() {
        session.beginTransaction();
    }

    @Test
    public void createsDecklistFromName()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        assertEquals(
                DECK_NAME,
                decklist.getName(),
                "Names are not equal"
        );
        assertEquals(
                PLAYER,
                decklist.getPlayer(),
                "Players are not the same"
        );
        assertNotEquals(
                0,
                decklist.getId(),
                "Id must not be 0"
        );
    }

    @Test
    public void deckServiceAttachesCardToExistingDeck()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        MtgCard card = createMtgCard(NISSAS_DOG);
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        assertDeckCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
    }

    private MtgCard createMtgCard(long id)
    {
        MtgCard card = new MtgCard();
        card.setId(id);
        return card;
    }

    @Test
    public void deckServiceAttachesCardToExistingDecksSideboard()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        MtgCard card = createMtgCard(NISSAS_DOG);
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        assertDeckCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
    }

    @Test
    public void deckServiceUpdatesAmountsWhenAttachingExistingCards()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        MtgCard card = createMtgCard(NISSAS_DOG);
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        assertDeckCard(
                decklist,
                card,
                COUNT + 1,
                SIDEBOARD
        );
    }

    @Test
    public void deckServiceDoesNotUpdateAmountsWhenAttachingMixedCards()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        MtgCard card = createMtgCard(NISSAS_DOG);
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                !SIDEBOARD
        );
        assertDeckCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        assertDeckCard(
                decklist,
                card,
                COUNT,
                !SIDEBOARD
        );
    }

    @Test
    public void deckServiceThrowsWhenCreatingDuplicateDecklist()
    {
        deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> deckService.createDecklist(
                        PLAYER,
                        DECK_NAME,
                        ""
                ),
                "Deck service should throw when trying to find non existing deck"
        );
    }

    @Test
    public void deckServiceReturnsMultipleDecksFromPlayer()
    {
        deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        deckService.createDecklist(
                PLAYER,
                DECK_NAME + 1,
                ""
        );
        Collection<Decklist> decklists = deckService.getPlayerDecklists(PLAYER);
        assertEquals(
                2,
                decklists.size(),
                "Decklists collection must not be empty"
        );
    }

    private void assertDeckCardByMtgCard(Decklist decklist, MtgCard card, int count, boolean sideboard)
    {
        MtgDeckCard deckCard = getDeckCardByMtgCard(
                decklist,
                card
        );
        assertDeckCardByProvidedMtgCard(
                card,
                deckCard,
                count,
                sideboard
        );
    }

    private void assertDeckCard(Decklist decklist, MtgCard card, int count, boolean sideboard)
    {
        MtgDeckCard deckCard = getFirstCard(
                decklist,
                sideboard
        );
        assertDeckCardByProvidedMtgCard(
                card,
                deckCard,
                count,
                sideboard
        );
    }

    private void assertDeckCardByProvidedMtgCard(MtgCard card, MtgDeckCard deckCard, int count, boolean sideboard)
    {
        assertNotNull(
                deckCard,
                "Deck card was not added into the deck"
        );
        assertEquals(
                card,
                deckCard.getCard(),
                "Deck card does not reference same card object"
        );
        assertEquals(
                count,
                deckCard.getCount(),
                "Deck card count does not match provided count"
        );
        assertEquals(
                sideboard,
                deckCard.isSideboard(),
                "Cards sideboard status does not match"
        );
    }

    private MtgDeckCard getFirstCard(Decklist decklist, boolean sideboard)
    {
        return decklist
                .getCards()
                .stream()
                .filter(it -> it.isSideboard() == sideboard)
                .findFirst()
                .orElse(null);
    }

    private MtgDeckCard getDeckCardByMtgCard(Decklist decklist, MtgCard card)
    {
        return decklist
                .getCards()
                .stream()
                .filter(it -> it
                        .getCard()
                        .equals(card))
                .findFirst()
                .orElse(null);
    }

    @Test
    public void deckServiceReturnsDecklistByPlayerAndName()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        Decklist returned = deckService.getDeckList(
                PLAYER,
                DECK_NAME
        );
        assertEquals(
                decklist.getId(),
                returned.getId()
        );
    }

    @Test
    public void deckServiceDoesNotMixTwoDifferentCards()
    {
        MtgCard card1 = createMtgCard(NISSAS_DOG);
        MtgCard card2 = createMtgCard(NISSAS_DOG + 1);
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        deckService.attachCard(
                decklist,
                card1,
                COUNT,
                SIDEBOARD
        );
        deckService.attachCard(
                decklist,
                card2,
                COUNT,
                SIDEBOARD
        );
        assertDeckCardByMtgCard(
                decklist,
                card1,
                COUNT,
                SIDEBOARD
        );
        assertDeckCardByMtgCard(
                decklist,
                card2,
                COUNT,
                SIDEBOARD
        );
    }

    @Test
    public void deckServiceRemovesDecklist()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        deckService.removeDecklist(decklist);
    }

    @Test
    public void deckServiceDetachesCard()
    {
        Decklist decklist = deckService.createDecklist(
                PLAYER,
                DECK_NAME,
                ""
        );
        MtgCard card = createMtgCard(NISSAS_DOG);
        deckService.attachCard(
                decklist,
                card,
                COUNT,
                SIDEBOARD
        );
        deckService.detachCard(
                decklist,
                card,
                SIDEBOARD
        );
        assertEquals(
                0,
                decklist
                        .getCards()
                        .size(),
                "Decklist size should be empty"
        );
    }

    @Test
    public void deckServiceFetchesCardsByName()
    {
        MtgCard card = deckService.fetchCard(WORT);
        assertTrue(
                NISSAS_DOG<=
                card.getId()
        );
    }

    @Test
    public void deckService_fetchesCardByName_fetchesSets() {
        MtgCard card = deckService.fetchCard(WORT);
        assertFalse(card.getSets().isEmpty());
    }

    @Test
    public void deckService_fetchesCardByName_fetchesTypes() {
        MtgCard card = deckService.fetchCard(WORT);
        assertFalse(card.getTypes().isEmpty());
    }


    @Test
    public void deckService_partialFusedCardName_fetchesCard() {
        // regression test for https://gitlab.com/Dragas/eternal-witness/-/issues/33
        MtgCard card = deckService.fetchCard(PARTIAL_FUSED_NAME);
        assertNotEquals(0, card.getId());
        assertTrue(card.getName().contains("//"));
        assertTrue(card.getName().length() > PARTIAL_FUSED_NAME.length());
    }

    @AfterEach
    public void tearDown()
    {
        Transaction t = session.getTransaction();
        Query q = session.createQuery("Delete Decklist");
        q.executeUpdate();
        session
                .getTransaction()
                .commit();
    }

    @Test
    public void deckServiceThrowsWhenTryingToFetchNonExistingCard()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> deckService.fetchCard(WORT + WORT),
                "Should throw no such entity exception"
        );
    }
}
