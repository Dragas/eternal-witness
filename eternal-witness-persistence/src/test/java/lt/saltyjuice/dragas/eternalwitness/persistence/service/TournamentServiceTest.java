package lt.saltyjuice.dragas.eternalwitness.persistence.service;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.PastDeadlineException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.api.model.TournamentPlayer;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class TournamentServiceTest
{
    private static final SessionFactory SESSION_FACTORY = ServiceLoader
            .load(SessionFactoryService.class)
            .findFirst()
            .get()
            .getSessionFactory();
    private static final String NAME = "Eternal witness invitational";
    private static TournamentService tournamentService;
    private static PlayerService playerService;
    private static DeckService deckService;
    private static Player player;
    private static Decklist deck;
    private static Session session;

    @BeforeAll
    public static void setUpSuite()
    {
        playerService = ServiceLoader
                .load(PlayerService.class)
                .findFirst()
                .get();
        deckService = ServiceLoader
                .load(DeckService.class)
                .findFirst()
                .get();
        tournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
        session = DatabaseTestUtility.getSession(SESSION_FACTORY);
        session.beginTransaction();
        player = playerService.createPlayer(
                "Dragas",
                ""
        );
        deck = deckService.createDecklist(
                player,
                "Dredge",
                ""
        );
        session.getTransaction().commit();
    }

    @BeforeEach
    public void setUp() {
        session.beginTransaction();
    }

    @AfterAll
    public static void tearDownAll()
    {
        session.beginTransaction();
        session.update(player);
        playerService.removePlayer(player);
        session.flush();
        session.getTransaction().commit();
        DatabaseTestUtility.removeSession(session);
    }

    @Test
    public void tournamentServiceCreatesTournament()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        assertNotEquals(
                0,
                tournament.getId(),
                "the ID should not be 0"
        );
    }

    @Test
    public void tournamentServicePreventsAttachingPersonPastDeadline()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(1),
                LocalDateTime
                        .now()
                        .minusDays(1)
        );
        assertThrows(
                PastDeadlineException.class,
                () -> tournamentService.attachPlayer(
                        tournament,
                        player,
                        deck
                ),
                "Tournament service should prevent player getting attached past deadline"
        );
    }

    @Test
    public void tournamentServiceThrowsWhenDuplicateTournamentIsCreated()
    {
        tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> tournamentService.createTournament(
                        NAME,
                        LocalDateTime
                                .now()
                                .plusDays(2),
                        LocalDateTime
                                .now()
                                .plusDays(1)
                ),
                "Should throw duplicate entity exception"
        );
    }

    @Test
    public void tournamentServiceAttachesPlayerToTournament()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        tournamentService.attachPlayer(
                tournament,
                player,
                deck
        );
        assertPlayerIsAttached(
                tournament,
                player,
                deck
        );
    }

    @Test
    public void tournamentServiceDoesNotAttachSamePlayerToTournamentTwiceWithSameDeck()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        tournamentService.attachPlayer(
                tournament,
                player,
                deck
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> tournamentService.attachPlayer(
                        tournament,
                        player,
                        deck
                ),
                "Tournament service must prevent attaching same player twice"
        );
    }

    @Test
    public void tournamentServiceDoesNotAttachSamePlayerToTournamentTwiceWithDifferentDeck()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        Decklist d = deckService.createDecklist(
                player,
                "Dredge 2",
                ""
        );
        tournamentService.attachPlayer(
                tournament,
                player,
                d
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> tournamentService.attachPlayer(
                        tournament,
                        player,
                        deck
                ),
                "Tournament service must prevent attaching same player twice"
        );
    }

    @Test
    public void tournamentServiceDetachesPlayerFromTournament()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        tournamentService.attachPlayer(
                tournament,
                player,
                deck
        );
        tournamentService.detachPlayer(
                tournament,
                player
        );
        assertEquals(
                0,
                tournament
                        .getTournamentPlayers()
                        .size(),
                "The tournament players collection should be empty"
        );
    }

    @Test
    public void tournamentServiceFindsTournamentByName()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        Tournament found = tournamentService.findTournamentByName(NAME);
        assertCreatedWithFoundTournament(
                tournament,
                found
        );
    }

    @Test
    public void tournamentServiceFindsTournamentById()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        Tournament found = tournamentService.findTournamentById(tournament.getId());
        assertCreatedWithFoundTournament(
                tournament,
                found
        );
    }

    private void assertCreatedWithFoundTournament(Tournament tournament, Tournament found)
    {
        assertNotNull(
                found,
                "Tournament from database should not be null"
        );
        assertEquals(
                tournament.getId(),
                found.getId(),
                "Found and created ids should match"
        );
        assertEquals(
                tournament.getName(),
                found.getName(),
                "Found and created names should match"
        );
    }

    @Test
    public void tournamentServiceThrowsWhenTryingToFindNonExistingTournamentByName()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> tournamentService.findTournamentByName(NAME),
                "Should throw when it does not exist"
        );
    }

    @Test
    public void tournamentServiceThrowsWhenTryingToFindNonExistingTournamentById()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> tournamentService.findTournamentById(-1),
                "Should throw when it does not exist"
        );
    }

    @Test
    public void tournamentServiceRemovesTournaments()
    {
        Tournament tournament = tournamentService.createTournament(
                NAME,
                LocalDateTime
                        .now()
                        .plusDays(2),
                LocalDateTime
                        .now()
                        .plusDays(1)
        );
        tournamentService.removeTournament(tournament);
        session.flush();
        assertThrows(
                NoSuchEntityException.class,
                () -> tournamentService.findTournamentByName(NAME),
                "Tournament service must remove the provided tournament"
        );
    }

    private void assertPlayerIsAttached(Tournament tournament, Player player, Decklist deck)
    {
        TournamentPlayer tournamentPlayer = tournament
                .getTournamentPlayers()
                .stream()
                .findFirst()
                .get();
        assertEquals(
                player.getId(),
                tournamentPlayer
                        .getPlayer()
                        .getId(),
                "Tournament player id should match"
        );
        assertEquals(
                deck.getId(),
                tournamentPlayer
                        .getDecklist()
                        .getId(),
                "deck ids should match"
        );
    }

    @AfterEach
    public void tearDown()
    {
        session.getTransaction();
        Query q = session.createQuery("Delete Tournament");
        q.executeUpdate();
        session
                .getTransaction()
                .commit();
    }
}
