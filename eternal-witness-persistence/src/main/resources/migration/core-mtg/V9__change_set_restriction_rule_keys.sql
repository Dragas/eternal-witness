update rules set key = 'format.set.before' where key = 'format.before.set';
update rules set key = 'format.set.after' where key = 'format.after.set';
insert into rules(name, key, description, requirement) values
(
 'Banned set', 'format.set.banned', 'Defines which sets are not permitted in that format.', 'varchar set_shortname'
);
