insert into
    formats
(
    name
)
values
(
    '701580259845341226-Modern'
);
insert into
    format_rule
(
    rule_id,
    format_id,
    value
)
values
(
        (select id from rules where rules.key = 'format.mainboard.min'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        '60'
),
(
        (select id from rules where rules.key = 'format.sideboard.max'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        '15'
),
(
        (select id from rules where rules.key = 'format.repetition'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        '4'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Forest'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Swamp'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Island'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Plains'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Mountain'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Wastes'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Snow-Covered Forest'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Snow-Covered Swamp'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Snow-Covered Island'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Snow-Covered Plains'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Snow-Covered Mountain'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Persistent Petitioners'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Rat Colony'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Relentless Rats'
),
(
        (select id from rules where rules.key = 'format.repetitionex'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Shadowborn Apostle'
),

(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Ancient Den'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Birthing Pod'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Blazing Shoal'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Bridge from Below'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Chrome Mox'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Cloudpost'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Dark Depths'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Deathrite Shaman'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Dig Through Time'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Dread Return'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Eye of Ugin'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Faithless Looting'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Gitaxian Probe'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Glimpse of Nature'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Golgari Grave-Troll'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Great Furnace'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Green Sun''s Zenith'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Hogaak, Arisen Necropolis'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Hypergenesis'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Krark-Clan Ironworks'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Mental Misstep'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Mox Opal'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Mycosynth Lattice'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Oko, Thief of Crowns'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Once Upon a Time'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Ponder'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Preordain'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Punishing Fire'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Rite of Flame'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Seat of the Synod'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Second Sunrise'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Seething Song'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Sensei''s Divining Top'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Skullclamp'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Splinter Twin'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Summer Bloom'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Treasure Cruise'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Tree of Tales'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Umezawa''s Jitte'
),
(
        (select id from rules where rules.key = 'format.card.banned'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        'Vault of Whispers'
),
(
        (select id from rules where rules.key = 'format.set.after'),
        (select id from formats where formats.name = '701580259845341226-Modern'),
        '2003-07-29'
);

insert into
    format_rule(
    rule_id,
    format_id,
    value
)
select
    (
        select
            id
        from
            rules
        where
            rules.key = 'format.set.banned'
    ),
    (
        select
            id
        from
            formats
        where
            formats.name = '701580259845341226-Modern'
    ),
    (sets.short_name)
from
    sets
where
      release >= '2003-07-29'
  and sets.full_name not in (
                             'Eighth Edition',
                             'Mirrodin',
                             'Darksteel',
                             'Fifth Dawn',
                             'Champions of Kamigawa',
                             'Betrayers of Kamigawa',
                             'Saviors of Kamigawa',
                             'Ninth Edition',
                             'Ravnica: City of Guilds',
                             'Guildpact',
                             'Dissension',
                             'Coldsnap',
                             'Time Spiral',
                             'Planar Chaos',
                             'Future Sight',
                             'Tenth Edition',
                             'Lorwyn',
                             'Morningtide',
                             'Shadowmoor',
                             'Eventide',
                             'Shards of Alara',
                             'Conflux',
                             'Alara Reborn',
                             'Magic 2010',
                             'Zendikar',
                             'Worldwake',
                             'Rise of the Eldrazi',
                             'Magic 2011',
                             'Scars of Mirrodin',
                             'Mirrodin Besieged',
                             'New Phyrexia',
                             'Magic 2012',
                             'Innistrad',
                             'Dark Ascension',
                             'Avacyn Restored',
                             'Magic 2013',
                             'Return to Ravnica',
                             'Gatecrash',
                             'Dragon''s Maze',
                             'Magic 2014',
                             'Theros',
                             'Born of the Gods',
                             'Journey into Nyx',
                             'Magic 2015',
                             'Khans of Tarkir',
                             'Fate Reforged',
                             'Dragons of Tarkir',
                             'Magic Origins',
                             'Battle for Zendikar',
                             'Oath of the Gatewatch',
                             'Welcome Deck 2016',
                             'Shadows over Innistrad',
                             'Eldritch Moon',
                             'Kaladesh',
                             'Aether Revolt',
                             'Welcome Deck 2017',
                             'Amonkhet',
                             'Hour of Devastation',
                             'Ixalan',
                             'Rivals of Ixalan',
                             'Dominaria',
                             'Core Set 2019',
                             'Guilds of Ravnica',
                             'Ravnica Allegiance',
                             'War of the Spark',
                             'Core Set 2020',
                             'Modern Horizons',
                             'Throne of Eldraine',
                             'Theros: Beyond Death',
                             'Ikoria: Lair of Behemoths',
                             'Core Set 2021',
                             'Modern Masters 2015',
                             'Modern Masters 2017',
                             'Modern Masters'
    );

/*

"format.mainboard.min"
"format.sideboard.max"

*/
