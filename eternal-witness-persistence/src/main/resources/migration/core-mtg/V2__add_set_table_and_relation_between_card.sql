create table sets
(
	id serial not null,
	name varchar(5) not null
);

create unique index sets_id_uindex
	on sets (id);

create unique index sets_name_uindex
	on sets (name);

alter table sets
	add constraint sets_pk
		primary key (id);

alter table public.mtgcard drop column mana_cost;
alter table public.mtgcard drop column power;
alter table public.mtgcard drop column toughness;
alter table public.mtgcard drop column type;

create table mtgcard_set(
    id serial not null,
    set_id int not null,
    card_id int not null,
    card_number int not null,
    rarity varchar(20) not null
);

create unique index mtgcard_set_card_set_ids on mtgcard_set(set_id, card_id);
create unique index mtgcard_set_card_id on mtgcard_set(id);
alter table mtgcard_set add constraint mtgcard_set_pk primary key (id);

