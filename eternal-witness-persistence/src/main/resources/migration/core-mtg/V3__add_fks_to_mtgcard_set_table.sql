alter table mtgcard_set
    add constraint mtgcard_set_mtgcard_id_fk
        foreign key (card_id) references mtgcard (id)
            on delete cascade;

alter table mtgcard_set
    add constraint mtgcard_set_sets_id_fk
        foreign key (set_id) references sets
            on delete cascade;

