create table formats
(
    id   serial  not null
        constraint formats_pk
            primary key,
    name varchar not null
);

create unique index formats_id_uindex on formats (id);

create table types
(
    id   serial  not null
        constraint types_pk primary key,
    name varchar not null
);

create unique index types_id_uindex on types (id);
create unique index types_name_uindex on types (name);

create table mtgcard_type
(
    id      serial not null
        constraint mtgcard_type_pk primary key,
    card_id int    not null
        constraint mtgcard_type_fk_mtgcard references mtgcard on delete cascade,
    type_id int    not null
        constraint mtgcard_type_fk_types references types on delete cascade
);

create unique index mtgcard_type_id_uindex on mtgcard_type (id);
create unique index mtgcard_type_card_id_type_id_uindex on mtgcard_type (card_id, type_id);

create table rules
(
    id          serial  not null
        constraint rules_pk primary key,
    name        varchar not null,
    key         varchar not null,
    description varchar not null,
    requirement varchar not null
);

create unique index rules_name_uindex on rules (name);
create unique index rules_key_uindex on rules (key);

create table format_rule
(
    id        serial  not null
        constraint format_rule_pk primary key,
    value     varchar not null,
    format_id int     not null
        constraint format_rule_fk_formats references formats on delete cascade,
    rule_id   int     not null
        constraint format_rule_fk_rules references rules on delete cascade
);

insert into rules(name, key, requirement, description)
values ('Banned card', 'format.card.banned', 'card_id', 'Denotes the card that cannot be played in that format'),
       ('Restricted card', 'format.card.restricted', 'card_id', 'Denotes the card that can only played as one of'),
       ('Legal sets after (set)', 'format.after.set', 'set_id',
        'Denotes all the sets that are valid in the format after particular set (inclusive)'),
       ('Legal sets before (set)', 'format.before.set', 'set_id',
        'Denotes all the sets that are valid in the format before particular set (inclusive)'),
       ('Decklist mainboard size min', 'format.mainboard.min', 'int',
        'Denotes how many cards at least are required to be present in the deck mainboard for it to be valid'),
       ('Decklist mainboard size max', 'format.mainboard.max', 'int',
        'Denotes how many cards at most are required to be present in the deck mainboard for it to be valid'),
       ('Decklist mainboard card max repetition count', 'format.mainboard.repetition', 'int',
        'Denotes how many times the same card can repeat in the deck mainboard'),
       ('Decklist mainboard card repetition exception', 'format.mainboard.repetitionex', 'card_id',
        'Denotes exceptions to repetition count rule for mainoard'),
       ('Decklist sideboard size min', 'format.sideboard.min', 'int',
        'Denotes how many cards at least are required to be present in the deck sideboard for it to be valid'),
       ('Decklist sideboard size max', 'format.sideboard.max', 'int',
        'Denotes how many cards at most are required to be present in the deck sideboard for it to be valid'),
       ('Decklist sideboard card max repetition count', 'format.sideboard.repetition', 'int',
        'Denotes how many the same card can repeat in the sideboard'),
       ('Decklist sideboard card repetition exception', 'format.sideboard.repetitionex', 'card_id',
        'Denotes exceptions to repetition count rule for sb'),
       ('Banned type', 'format.type.banned', 'type_id', 'Denotes which card type is banned in that format'),
       ('Banned rarity', 'format.rarity.banned', 'rarity_varchar', 'Denotes which rarity is banned in that format');





