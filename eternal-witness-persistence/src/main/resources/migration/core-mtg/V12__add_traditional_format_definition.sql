insert into
    formats(
    name
)
values
(
    '701580259845341226-Traditional'
);

insert into
    format_rule
    (
    rule_id,
    format_id,
    value
)
select
    rule_id,
    (select id from formats where name = '701580259845341226-Traditional') as format_id,
    value
from
    format_rule
where
        format_id = (select id from formats where name = '701580259845341226-Modern');

insert into
    format_rule
(
    rule_id,
    format_id,
    value
)
values
(
        (select id from rules where key = 'format.rarity.banned'),
        (select id from formats where name = '701580259845341226-Traditional'),
        'mythic'
),
(
    (select id from rules where key = 'format.type.banned'),
    (select id from formats where name = '701580259845341226-Traditional'),
    'Planeswalker'
);
