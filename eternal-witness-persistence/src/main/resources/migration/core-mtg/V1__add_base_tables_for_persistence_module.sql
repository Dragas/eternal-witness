create table mtgcard
(
	id bigserial not null
		constraint mtgcard_pk
			primary key,
	name varchar(256) not null,
	cmc double precision not null,
	type varchar(100) not null,
	mana_cost varchar(100) null default null,
	power varchar(10) null default null,
	toughness varchar(10) null default null
);

create unique index mtgcard_id_uindex
	on mtgcard (id);

create unique index mtgcard_name_uindex
	on mtgcard (name);

create table players
(
	id serial not null
		constraint player_pk
			primary key,
	username varchar(32) not null,
	discord_name varchar(40) default ''::character varying not null
);

create unique index player_id_uindex
	on players (id);

create unique index player_username_uindex
	on players (username);

create table decklists
(
	id serial not null
		constraint decklists_pk
			primary key,
	name varchar(50) not null,
	player_id integer not null
		constraint decklists_players_id_fk
			references players
				on update cascade on delete cascade,
	deck_url varchar(100) default ''::character varying not null
);

create unique index decklists_id_uindex
	on decklists (id);

create unique index decklists_player_id_name_uindex
	on decklists (player_id, name);

create table decklist_mtgcard
(
	card_id integer not null
		constraint decklist_mtgcard_mtgcard_id_fk
			references mtgcard
				on update cascade on delete cascade,
	deck_id integer not null
		constraint decklist_mtgcard_decklists_id_fk
			references decklists
				on update cascade on delete cascade,
	count integer default 0 not null,
	id serial not null
		constraint decklist_mtgcard_pk_2
			primary key,
	sideboard boolean default false not null,
	constraint decklist_mtgcard_pk
		unique (card_id, deck_id, sideboard)
);

create index decklist_mtgcard_card_id_index
	on decklist_mtgcard (card_id);

create index decklist_mtgcard_deck_id_index
	on decklist_mtgcard (deck_id);

create unique index decklist_mtgcard_id_uindex
	on decklist_mtgcard (id);

create table tournaments
(
	id serial not null
		constraint tournaments_pk
			primary key,
	name varchar(100) not null,
	deadline timestamp not null,
	start timestamp not null
);

create unique index tournaments_id_uindex
	on tournaments (id);

create unique index tournaments_name_uindex
	on tournaments (name);

create table player_tournament
(
	player_id integer not null
		constraint player_tournament_players_id_fk
			references players
				on update cascade on delete cascade,
	deck_id integer not null
		constraint player_tournament_decklists_id_fk
			references decklists
				on update cascade on delete cascade,
	tournament_id integer not null
		constraint player_tournament_tournaments_id_fk
			references tournaments
				on update cascade on delete cascade,
	id serial not null
		constraint player_tournament_pk_2
			primary key,
	constraint player_tournament_pk
		unique (player_id, tournament_id)
);

create index player_tournament_deck_id_index
	on player_tournament (deck_id);

create index player_tournament_player_id_index
	on player_tournament (player_id);

create index player_tournament_tournament_id_index
	on player_tournament (tournament_id);

create unique index player_tournament_id_uindex
	on player_tournament (id);

