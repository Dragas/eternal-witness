alter table sets add column full_name varchar not null;
alter table sets add column release date not null;
alter table sets rename column name to short_name;
alter table sets alter column short_name type varchar(20);

alter index sets_name_uindex rename to sets_short_name_uindex;
create unique index sets_long_name_uindex on sets(full_name);
