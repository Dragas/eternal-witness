drop index mtgcard_set_card_set_ids;

alter table mtgcard_set alter column card_number type varchar using card_number::varchar;

create unique index mtgcard_set_ids_uindex on mtgcard_set(set_id, card_id, card_number);
