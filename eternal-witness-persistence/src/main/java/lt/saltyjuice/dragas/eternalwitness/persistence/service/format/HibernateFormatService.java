package lt.saltyjuice.dragas.eternalwitness.persistence.service.format;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Format;
import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.api.model.Rule;
import lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import java.util.Collection;
import java.util.stream.Collectors;

public class HibernateFormatService extends AbstractHibernateService implements FormatService
{

    public HibernateFormatService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public Format createFormat(String name)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                Format format = new Format();
                format.setName(name);
                s.save(format);
                return format;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Format %s already exists",
                            name
                    ),
                    e
            );
        }
    }

    @Override
    public void deleteFormat(Format format)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(format);
        });
    }

    @Override
    public Format fetchFormatByName(String name)
    {
        return performReadingRunnable((s) ->
        {
            Query<Format> formatQuery = s.createQuery(
                    "select f from Format f where f.name = ?1",
                    Format.class
            );
            formatQuery.setParameter(
                    1,
                    name
            );
            return getFirstResult(
                    formatQuery.getResultList(),
                    String.format(
                            "Format with name %s does not exist",
                            name
                    )
            );
        });
    }

    @Override
    public Rule getRule(String key)
    {
        return performReadingRunnable((s) ->
        {
            Query<Rule> ruleQuery = s.createQuery(
                    "select r from Rule r where r.key = ?1",
                    Rule.class
            );
            ruleQuery.setParameter(
                    1,
                    key
            );
            return getFirstResult(
                    ruleQuery.getResultList(),
                    String.format(
                            "Rule with key %s does not exist",
                            key
                    )
            );
        });
    }

    @Override
    public void attachRule(Format format, Rule rule, String value)
    {
        performSimpleRunnable((s) ->
        {
            FormatRule formatRule = new FormatRule();
            formatRule.setRule(rule);
            formatRule.setFormat(format);
            formatRule.setValue(value);
            format
                    .getRules()
                    .add(formatRule);
            s.saveOrUpdate(formatRule);
            s.saveOrUpdate(format);
        });
    }

    @Override
    public void detachRule(Format format, Rule rule)
    {
        performSimpleRunnable((s) ->
        {
            Collection<FormatRule> rules = format
                    .getRules()
                    .stream()
                    .filter(it -> it.getRule().getKey().equals(rule.getKey()))
                    .collect(Collectors.toList());
            format.getRules().removeAll(rules);
            rules.forEach(s::delete);
        });
    }
}
