package lt.saltyjuice.dragas.eternalwitness.persistence.service.format;

import lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateFormatServiceProvider
{
    public static FormatService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateFormatService(service.getSessionFactory());
    }
}
