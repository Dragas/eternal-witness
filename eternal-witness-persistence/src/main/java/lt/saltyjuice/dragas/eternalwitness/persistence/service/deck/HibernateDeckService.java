package lt.saltyjuice.dragas.eternalwitness.persistence.service.deck;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HibernateDeckService extends AbstractHibernateService implements DeckService
{
    private static final String STARTS_WITH_NAME_AND_DOUBLE_SLASH = "%s // %%";

    public HibernateDeckService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public Decklist createDecklist(Player player, String name, String deckUrl)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                Decklist decklist = new Decklist();
                decklist.setPlayer(player);
                decklist.setName(name);
                decklist.setDeckUrl(deckUrl);
                s.save(decklist);
                return decklist;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Player with id %d already has a deck named %s",
                            player.getId(),
                            name
                    ),
                    e
            );
        }
    }

    @Override
    public void removeDecklist(Decklist decklist)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(decklist);
        });
    }

    @Override
    public void attachCard(Decklist decklist, MtgCard card, int count, boolean sideboard)
    {
        performSimpleRunnable((s) ->
        {

            MtgDeckCard mtgDeckCardFromDeck = decklist
                    .getCards()
                    .stream()
                    .filter((it) -> it
                            .getCard()
                            .getId() == card.getId())
                    .filter((it) -> it.isSideboard() == sideboard)
                    .findFirst()
                    .orElseGet(() ->
                    {
                        MtgDeckCard mtgDeckCard = new MtgDeckCard();
                        mtgDeckCard.setDecklist(decklist);
                        mtgDeckCard.setCard(card);
                        mtgDeckCard.setSideboard(sideboard);
                        return mtgDeckCard;
                    });
            mtgDeckCardFromDeck.setCount(
                    mtgDeckCardFromDeck.getCount() + count
            );
            if (mtgDeckCardFromDeck.getId() == 0)
            {
                decklist
                        .getCards()
                        .add(mtgDeckCardFromDeck);
            }
            s.saveOrUpdate(mtgDeckCardFromDeck);
            s.saveOrUpdate(decklist);
        });
    }

    @Override
    public void detachCard(Decklist decklist, MtgCard card, boolean sideboard)
    {
        performSimpleRunnable((s) ->
        {
            MtgDeckCard mtgDeckCard = decklist
                    .getCards()
                    .stream()
                    .filter(it -> it.isSideboard() == sideboard)
                    .filter(it -> it
                            .getCard()
                            .getId() == card.getId())
                    .findFirst()
                    .orElse(null);
            if (mtgDeckCard != null)
            {
                decklist
                        .getCards()
                        .remove(mtgDeckCard);
                s.delete(mtgDeckCard);
            }
        });
    }

    @Override
    public Decklist getDeckList(Player player, String name)
    {
        return performReadingRunnable((s) ->
        {
            Query<Decklist> query = s.createQuery(
                    "select dt from Decklist dt join dt.player p where dt.name = ?1 and p.name = ?2",
                    Decklist.class
            );
            query.setParameter(
                    1,
                    name
            );
            query.setParameter(
                    2,
                    player.getName()
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format(
                            "Failed to find %s for player %s",
                            player.getName(),
                            name
                    )
            );
        });
    }

    @Override
    public Collection<Decklist> getPlayerDecklists(Player player)
    {
        return performReadingRunnable((s) ->
        {
            Query<Decklist> decklists = s.createQuery(
                    "select dt from Decklist dt join dt.player p where p.id = ?1",
                    Decklist.class
            );
            decklists.setParameter(
                    1,
                    player.getId()
            );
            try (Stream<Decklist> decklistStream = decklists.stream())
            {
                return decklistStream.collect(Collectors.toList());
            }
        });
    }

    @Override
    public MtgCard fetchCard(String cardName)
    {
        return performReadingRunnable((s) ->
        {
            String fusedLikenessQueryArgument = String.format(STARTS_WITH_NAME_AND_DOUBLE_SLASH, cardName);
            Query<MtgCard> mtgCardQuery = s.createQuery(
                    "select mc from MtgCard mc where mc.name = ?1 or mc.name like ?2 order by mc.name asc",
                    MtgCard.class
            );
            mtgCardQuery.setParameter(
                    1,
                    cardName
            );
            mtgCardQuery.setParameter(
                    2,
                    fusedLikenessQueryArgument
            );
            return getFirstResult(
                    mtgCardQuery.getResultList(),
                    String.format(
                            "Failed to find card with name %s",
                            cardName
                    )
            );
        });
    }
}
