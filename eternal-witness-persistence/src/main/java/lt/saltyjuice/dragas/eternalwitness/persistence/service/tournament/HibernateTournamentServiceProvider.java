package lt.saltyjuice.dragas.eternalwitness.persistence.service.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateTournamentServiceProvider
{
    public static TournamentService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateTournamentService(service.getSessionFactory());
    }
}
