package lt.saltyjuice.dragas.eternalwitness.persistence.service.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.PastDeadlineException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.api.model.TournamentPlayer;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import java.time.LocalDateTime;

public class HibernateTournamentService extends AbstractHibernateService implements TournamentService
{
    public HibernateTournamentService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public Tournament createTournament(String name, LocalDateTime startTime, LocalDateTime deadline)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                Tournament tournament = new Tournament();
                tournament.setName(name);
                tournament.setDeadline(deadline);
                tournament.setStart(startTime);
                s.save(tournament);
                return tournament;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Tournament with name %s already exists",
                            name
                    ),
                    e
            );
        }
    }

    @Override
    public void removeTournament(Tournament tournament)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(tournament);
        });
    }

    @Override
    public void attachPlayer(Tournament tournament, Player player, Decklist decklist)
    {
        if (tournament
                .getDeadline()
                .isBefore(LocalDateTime.now())
        )
        {
            throw new PastDeadlineException(
                    String.format(
                            "Trying to attach player %s to tournament %s past its deadline",
                            player.getName(),
                            tournament.getName()
                    )
            );
        }
        try
        {
            performSimpleRunnable((s) ->
            {
                TournamentPlayer tournamentPlayer = new TournamentPlayer();
                tournamentPlayer.setPlayer(player);
                tournamentPlayer.setDecklist(decklist);
                tournamentPlayer.setTournament(tournament);
                tournament
                        .getTournamentPlayers()
                        .add(tournamentPlayer);
                s.saveOrUpdate(tournamentPlayer);
                s.saveOrUpdate(tournament);
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Provided player %s is already in tournament %s",
                            player.getName(),
                            tournament.getName()
                    ),
                    e
            );
        }
    }

    @Override
    public void detachPlayer(Tournament tournament, Player player)
    {
        performSimpleRunnable((s) ->
        {
            s.refresh(tournament);
            TournamentPlayer tournamentPlayer = tournament
                    .getTournamentPlayers()
                    .stream()
                    .filter((it) -> it
                            .getPlayer()
                            .getId() == player.getId())
                    .findFirst()
                    .get();
            tournament
                    .getTournamentPlayers()
                    .remove(tournamentPlayer);
            s.delete(tournamentPlayer);
        });
    }

    @Override
    public Tournament findTournamentByName(String name)
    {
        return performReadingRunnable((s) ->
        {
            Query<Tournament> tournamentQuery = s.createQuery("select t from Tournament t where name = ?1");
            tournamentQuery.setParameter(
                    1,
                    name
            );
            return getFirstResult(
                    tournamentQuery.getResultList(),
                    String.format(
                            "Failed to find tournament with name %s",
                            name
                    )
            );
        });
    }

    @Override
    public Tournament findTournamentById(long id)
    {
        return performReadingRunnable((s) ->
        {
            Query<Tournament> tournamentQuery = s.createQuery("select t from Tournament t where id = ?1");
            tournamentQuery.setParameter(
                    1,
                    id
            );
            return getFirstResult(
                    tournamentQuery.getResultList(),
                    String.format(
                            "Failed to find tournament with id %d",
                            id
                    )
            );
        });
    }
}
