package lt.saltyjuice.dragas.eternalwitness.persistence.service.player;

import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernatePlayerServiceProvider
{
    public static PlayerService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernatePlayerService(service.getSessionFactory());
    }
}
