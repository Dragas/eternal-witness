package lt.saltyjuice.dragas.eternalwitness.persistence.service.player;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

public class HibernatePlayerService extends AbstractHibernateService implements PlayerService
{
    public HibernatePlayerService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public Player createPlayer(String name, String discordName) throws DuplicateEntityException
    {
        Player p = new Player();
        p.setName(name);
        p.setDiscordName(discordName);
        try
        {
            performSimpleRunnable(s -> s.saveOrUpdate(p));
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Player with name %s already exists",
                            name
                    ),
                    e
            );
        }
        return p;
    }

    @Override
    public void removePlayer(Player player)
    {
        performSimpleRunnable((s) ->
        {
            s.remove(player);
        });
    }

    @Override
    public Player fetchPlayerByName(String name) throws NoSuchEntityException
    {
        return performReadingRunnable((s) ->
        {
            Query<Player> q = s.createQuery(
                    "from Player where name = ?1",
                    Player.class
            );
            q.setParameter(
                    1,
                    name
            );
            return getFirstResult(
                    q.getResultList(),
                    String.format(
                            "Player with name %s does not exist",
                            name
                    )
            );
        });
    }

    @Override
    public Player fetchPlayerById(long id) throws NoSuchEntityException
    {
        final Player player = performReadingRunnable((s) -> s.get(
                Player.class,
                id
        ));
        if (player == null)
        {
            throw new NoSuchEntityException(String.format(
                    "Player with id %d does not exist",
                    id
            ));
        }
        return player;
    }
}
