package lt.saltyjuice.dragas.eternalwitness.persistence.service.deck;

import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateDeckServiceProvider
{
    public static DeckService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateDeckService(service.getSessionFactory());
    }
}
