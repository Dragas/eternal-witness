import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import lt.saltyjuice.dragas.eternalwitness.persistence.service.deck.HibernateDeckServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.persistence.service.format.HibernateFormatServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.persistence.service.player.HibernatePlayerServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.persistence.service.tournament.HibernateTournamentServiceProvider;

module eternal.witness.persistence {
    requires transitive eternal.witness.api;
    requires transitive eternal.witness.persistence.common;
    requires slf4j.api;
    exports lt.saltyjuice.dragas.eternalwitness.persistence.service.tournament;
    exports lt.saltyjuice.dragas.eternalwitness.persistence.service.player;
    exports lt.saltyjuice.dragas.eternalwitness.persistence.service.deck;
    exports lt.saltyjuice.dragas.eternalwitness.persistence.service.format;
    uses SessionFactoryService;
    uses PlayerService;
    uses DeckService;
    uses TournamentService;
    uses FormatService;
    provides PlayerService with HibernatePlayerServiceProvider;
    provides DeckService with HibernateDeckServiceProvider;
    provides TournamentService with HibernateTournamentServiceProvider;
    provides FormatService with HibernateFormatServiceProvider;
}
