open module eternal.witness.api {
    exports lt.saltyjuice.dragas.eternalwitness.api.service;
    exports lt.saltyjuice.dragas.eternalwitness.api.model;
    exports lt.saltyjuice.dragas.eternalwitness.api.exception;
    requires java.base;
    requires static org.hibernate.orm.core;
}
