package lt.saltyjuice.dragas.eternalwitness.api.service;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;

/**
 * Interface for managing players.
 * <p>
 * Player is such an entity in the system, that he can be taken as a singular unit.
 * Implementations should not add functionality
 * <p>
 * if a player does not exist, it is expected that the operation throws NoSuchPlayerException
 */
public interface PlayerService {

    Player createPlayer(String name, String discordName) throws DuplicateEntityException;

    void removePlayer(Player player);

    Player fetchPlayerByName(String name) throws NoSuchEntityException;

    Player fetchPlayerById(long id) throws NoSuchEntityException;
}
