package lt.saltyjuice.dragas.eternalwitness.api.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

public class Format
{
    private long id;
    private String name;
    private Collection<FormatRule> rules = new TreeSet<>();

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<FormatRule> getRules()
    {
        return rules;
    }

    public void setRules(Collection<FormatRule> rules)
    {
        this.rules = rules;
    }
}
