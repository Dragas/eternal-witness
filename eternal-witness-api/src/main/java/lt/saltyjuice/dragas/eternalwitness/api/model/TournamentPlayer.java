package lt.saltyjuice.dragas.eternalwitness.api.model;

public class TournamentPlayer implements Comparable<TournamentPlayer>
{

    private long id;
    private Player player;
    private Tournament tournament;
    private Decklist decklist;

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)
    {
        this.player = player;
    }

    public Tournament getTournament()
    {
        return tournament;
    }

    public void setTournament(Tournament tournament)
    {
        this.tournament = tournament;
    }

    public Decklist getDecklist()
    {
        return decklist;
    }

    public void setDecklist(Decklist decklist)
    {
        this.decklist = decklist;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    @Override
    public int compareTo(TournamentPlayer o)
    {
        return (int) (this.getId() - o.getId());
    }
}
