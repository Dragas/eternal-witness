package lt.saltyjuice.dragas.eternalwitness.api.model;

import java.time.LocalDate;

public class MtgSet
{
    private long id;
    private String shortName;
    private String name;
    private LocalDate release;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getShortName()
    {
        return shortName;
    }

    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public LocalDate getRelease()
    {
        return release;
    }

    public void setRelease(LocalDate release)
    {
        this.release = release;
    }
}
