package lt.saltyjuice.dragas.eternalwitness.api.service;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;

import java.time.LocalDateTime;

/**
 * Tournament service is used to manage tournaments in the system.
 * <br>
 * Tournaments are such unit in the system, where each player can only be
 * attached once to the tournament, with a deck that exists in the system.
 * <br>
 * Should the player want to update their deck, they must drop from tournament
 * and reenter it with different one instead.
 */
public interface TournamentService
{

    Tournament createTournament(String name, LocalDateTime startTime, LocalDateTime deadline);

    void removeTournament(Tournament tournament);

    void attachPlayer(Tournament tournament, Player player, Decklist decklist);

    void detachPlayer(Tournament tournament, Player player);

    Tournament findTournamentByName(String name);

    Tournament findTournamentById(long id);

}
