package lt.saltyjuice.dragas.eternalwitness.api.exception;

public class PastDeadlineException extends EternalWitnessExcepion
{
    public PastDeadlineException()
    {
        super();
    }

    public PastDeadlineException(String message)
    {
        super(message);
    }

    public PastDeadlineException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PastDeadlineException(Throwable cause)
    {
        super(cause);
    }

    protected PastDeadlineException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
