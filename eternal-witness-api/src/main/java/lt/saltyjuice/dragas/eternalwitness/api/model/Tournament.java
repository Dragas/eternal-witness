package lt.saltyjuice.dragas.eternalwitness.api.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.TreeSet;

public class Tournament
{
    private long id;
    private String name;
    private LocalDateTime deadline;
    private LocalDateTime start;
    private Collection<TournamentPlayer> tournamentPlayers = new TreeSet<>();

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public LocalDateTime getDeadline()
    {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline)
    {
        this.deadline = deadline;
    }

    public LocalDateTime getStart()
    {
        return start;
    }

    public void setStart(LocalDateTime start)
    {
        this.start = start;
    }

    public Collection<TournamentPlayer> getTournamentPlayers()
    {
        return tournamentPlayers;
    }

    public void setTournamentPlayers(Collection<TournamentPlayer> tournamentPlayers)
    {
        this.tournamentPlayers = tournamentPlayers;
    }
}
