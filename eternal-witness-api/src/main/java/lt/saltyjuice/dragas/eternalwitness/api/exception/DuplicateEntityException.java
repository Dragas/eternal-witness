package lt.saltyjuice.dragas.eternalwitness.api.exception;

public class DuplicateEntityException extends EternalWitnessExcepion
{
    public DuplicateEntityException()
    {
        super();
    }

    public DuplicateEntityException(String message)
    {
        super(message);
    }

    public DuplicateEntityException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DuplicateEntityException(Throwable cause)
    {
        super(cause);
    }

    protected DuplicateEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
