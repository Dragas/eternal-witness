package lt.saltyjuice.dragas.eternalwitness.api.model;

public class MtgType implements Comparable<MtgType>
{
    private long id;
    private String name;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int compareTo(MtgType o)
    {
        return getName().compareTo(o.getName());
    }
}
