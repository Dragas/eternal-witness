package lt.saltyjuice.dragas.eternalwitness.api.model;

public class MtgCardType implements Comparable<MtgCardType>
{
    private long id;
    private MtgCard card;
    private MtgType type;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public MtgCard getCard()
    {
        return card;
    }

    public void setCard(MtgCard card)
    {
        this.card = card;
    }

    public MtgType getType()
    {
        return type;
    }

    public void setType(MtgType type)
    {
        this.type = type;
    }

    @Override
    public int compareTo(MtgCardType o)
    {
        return (int) (getId() - o.getId());
    }
}
