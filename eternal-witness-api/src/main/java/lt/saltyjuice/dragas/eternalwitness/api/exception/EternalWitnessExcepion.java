package lt.saltyjuice.dragas.eternalwitness.api.exception;

public class EternalWitnessExcepion extends RuntimeException {
    public EternalWitnessExcepion() {
        super();
    }

    public EternalWitnessExcepion(String message) {
        super(message);
    }

    public EternalWitnessExcepion(String message, Throwable cause) {
        super(message, cause);
    }

    public EternalWitnessExcepion(Throwable cause) {
        super(cause);
    }

    protected EternalWitnessExcepion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
