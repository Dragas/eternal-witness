package lt.saltyjuice.dragas.eternalwitness.api.model;

import java.util.Collection;
import java.util.TreeSet;

public class Decklist {
    private long id;
    private Player player; // via decklists.player_id->player.id
    private Collection<MtgDeckCard> cards = new TreeSet<>();
    private String name;
    private String deckUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Collection<MtgDeckCard> getCards() {
        return cards;
    }

    public void setCards(Collection<MtgDeckCard> cards) {
        this.cards = cards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeckUrl() {
        return deckUrl;
    }

    public void setDeckUrl(String deckUrl) {
        this.deckUrl = deckUrl;
    }
}
