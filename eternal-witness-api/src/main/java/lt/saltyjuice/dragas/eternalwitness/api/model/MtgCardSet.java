package lt.saltyjuice.dragas.eternalwitness.api.model;

public class MtgCardSet implements Comparable<MtgCardSet>
{
    private long id;
    private MtgCard card;
    private MtgSet set;
    private String cardNumber;
    private String rarity;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public MtgCard getCard()
    {
        return card;
    }

    public void setCard(MtgCard card)
    {
        this.card = card;
    }

    public MtgSet getSet()
    {
        return set;
    }

    public void setSet(MtgSet set)
    {
        this.set = set;
    }

    public String getCardNumber()
    {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber)
    {
        this.cardNumber = cardNumber;
    }

    public String getRarity()
    {
        return rarity;
    }

    public void setRarity(String rarity)
    {
        this.rarity = rarity;
    }

    @Override
    public int compareTo(MtgCardSet o)
    {
        return (int) (getId() - o.getId());
    }
}
