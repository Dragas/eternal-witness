package lt.saltyjuice.dragas.eternalwitness.api.service;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;

import java.util.Collection;

/**
 * Decklist management service.
 * <p>
 * Decklist is a unit that depends on presence of players in the system. The
 * implied relationship between the two is that a player may have a lot of decks,
 * but a deck can only belong to single player. Implementations may opt to choose
 * that decklists are immutable and create a new version every time a modification
 * is saved.
 */
public interface DeckService
{

    Decklist createDecklist(Player player, String name, String deckUrl);

    void removeDecklist(Decklist decklist);

    void attachCard(Decklist decklist, MtgCard card, int count, boolean sideboard);

    void detachCard(Decklist decklist, MtgCard card, boolean sideboard);

    Decklist getDeckList(Player player, String name);

    MtgCard fetchCard(String cardName);

    Collection<Decklist> getPlayerDecklists(Player player);
}
