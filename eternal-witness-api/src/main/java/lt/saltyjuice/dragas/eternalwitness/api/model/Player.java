package lt.saltyjuice.dragas.eternalwitness.api.model;

public class Player {

    private long id;
    private String name;
    private String discordName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDiscordName()
    {
        return discordName;
    }

    public void setDiscordName(String discordName)
    {
        this.discordName = discordName;
    }
}
