package lt.saltyjuice.dragas.eternalwitness.api.model;

/**
 * wrapper for many to many relationship between {@link MtgCard} and {@link Decklist}
 */
public class MtgDeckCard implements Comparable<MtgDeckCard>
{
    private boolean sideboard;
    private MtgCard card; // via decklist_mtgcard.card_id->mtgcard.id
    private Decklist decklist; // via decklist_mtgcard.decklist_id->decklists.id
    private int count;
    private long id;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public boolean isSideboard()
    {
        return sideboard;
    }

    public void setSideboard(boolean sideboard)
    {
        this.sideboard = sideboard;
    }

    public MtgCard getCard()
    {
        return card;
    }

    public void setCard(MtgCard card) {
        this.card = card;
    }

    public Decklist getDecklist() {
        return decklist;
    }

    public void setDecklist(Decklist decklist) {
        this.decklist = decklist;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    @Override
    public int compareTo(MtgDeckCard o)
    {
        return (int) (getId() - o.getId());
    }
}
