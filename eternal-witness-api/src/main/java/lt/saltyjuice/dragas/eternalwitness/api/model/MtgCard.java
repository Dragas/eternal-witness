package lt.saltyjuice.dragas.eternalwitness.api.model;

import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

public class MtgCard implements Comparable<MtgCard> {

    private long id;
    private String name;
    private Collection<MtgCardType> types;
    private Collection<MtgCardSet> sets;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<MtgCardType> getTypes()
    {
        if(types == null) {
            types = new TreeSet<>();
        }
        return types;
    }

    public void setTypes(Collection<MtgCardType> types)
    {
        this.types = types;
    }

    @Override
    public int compareTo(MtgCard o)
    {
        return getName().compareTo(o.getName());
    }

    public Collection<MtgCardSet> getSets()
    {
        if(sets == null) {
            sets = new TreeSet<>();
        }
        return sets;
    }

    public void setSets(Collection<MtgCardSet> sets)
    {
        this.sets = sets;
    }
}
