package lt.saltyjuice.dragas.eternalwitness.api.service;

import lt.saltyjuice.dragas.eternalwitness.api.model.Format;
import lt.saltyjuice.dragas.eternalwitness.api.model.Rule;

/**
 * Format is such unit in the system where it
 * groups any amount of rules (either repeating or not)
 * into a single group and provides the set of
 * rules to validate decks against.
 * <br>
 * It is up to implementations to choose how
 * to use those rules for validation.
 */
public interface FormatService
{
    Format createFormat(String name);

    void deleteFormat(Format format);

    Format fetchFormatByName(String name);

    Rule getRule(String key);

    void attachRule(Format format, Rule rule, String value);

    void detachRule(Format format, Rule rule);
}
