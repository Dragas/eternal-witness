package lt.saltyjuice.dragas.eternalwitness.api.model;

public class FormatRule implements Comparable<FormatRule>
{
    private long id;
    private Rule rule;
    private Format format;
    private String value;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Rule getRule()
    {
        return rule;
    }

    public void setRule(Rule rule)
    {
        this.rule = rule;
    }

    public Format getFormat()
    {
        return format;
    }

    public void setFormat(Format format)
    {
        this.format = format;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public int compareTo(FormatRule o)
    {
        return (int) (getId() - o.getId());
    }
}
