import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.banned.BannedModule;
import lt.saltyjuice.dragas.eternalwitness.validator.banned.BannedValidator;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.banned {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.banned;
    uses ValidatorModule;
    provides ValidatorModule with BannedModule;
    provides ConstraintValidator with BannedValidator;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
