import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.banned.BannedModule;
import lt.saltyjuice.dragas.eternalwitness.validator.banned.BannedValidator;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.banned {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.banned;
    opens lt.saltyjuice.dragas.eternalwitness.validator.banned to org.hibernate.validator;
    provides ValidatorModule with BannedModule;
    provides ConstraintValidator with BannedValidator;
}
