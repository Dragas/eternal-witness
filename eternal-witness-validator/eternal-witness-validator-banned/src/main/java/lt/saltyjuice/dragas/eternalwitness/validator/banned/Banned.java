package lt.saltyjuice.dragas.eternalwitness.validator.banned;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Restricts the appearance of cards in a deck.
 * The validation happens by name, as a workaround to the fact
 * that decks do not contain some sort of multiverse id, but
 * rather a name. As a result, it is out of scope for this
 * validator to resolve translations of cards.
 * <br>
 * This annotation must be put on collection of {@link MtgDeckCard} elements.
 *
 * <p>
 * Supported types are:
 *     <ul>
 *         <li>
 *             {@link MtgDeckCard}
 *         </li>
 *     </ul>
 * </p>
 * <p>
 * {@code null} elements are considered valid.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface Banned
{
    String message() default "{format.card.banned}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value() default {};
}
