package lt.saltyjuice.dragas.eternalwitness.validator.banned;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BannedModule implements ValidatorModule
{
    public static final String FORMAT_CARD_BANNED = "format.card.banned";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Collections.singletonList(FORMAT_CARD_BANNED);
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        BannedConstraintDef def = new BannedConstraintDef();
        String[] values = formatRulesList.stream().map(FormatRule::getValue).toArray(String[]::new);
        def.value(values);
        return def;
    }
}
