package lt.saltyjuice.dragas.eternalwitness.validator.banned;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

public class BannedValidator implements ConstraintValidator<Banned, Collection<MtgDeckCard>>
{
    private Collection<String> bannedCards;

    @Override
    public void initialize(Banned constraintAnnotation)
    {
        this.bannedCards = new TreeSet<>(Arrays.asList(constraintAnnotation.value()));
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        return value.stream()
                .map(MtgDeckCard::getCard)
                .map(MtgCard::getName)
                .noneMatch(bannedCards::contains);
    }
}
