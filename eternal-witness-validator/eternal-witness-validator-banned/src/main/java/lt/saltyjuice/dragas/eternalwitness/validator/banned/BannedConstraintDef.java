package lt.saltyjuice.dragas.eternalwitness.validator.banned;

import org.hibernate.validator.cfg.ConstraintDef;

public class BannedConstraintDef extends ConstraintDef<BannedConstraintDef, Banned>
{
    public BannedConstraintDef()
    {
        super(Banned.class);
    }

    public BannedConstraintDef value(String... values) {
        addParameter("value", values);
        return this;
    }
}
