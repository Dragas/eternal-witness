# EW-Validator BOM

Contains the modules related to deck validation.

## Implementing your own rules

EW-Validator is intended to load rules definitions at runtime. This
imposes additional limitations to how rules should be implemented.
For now, I have no clue how to do validation messages, but the
general guideline to implement your own rules is the following:

1. Create rule's annotation (required by Java Bean Validation);
2. Implement `javax.validation.ConstraintValidator` for that annotation;
3. Extend `org.hibernate.validator.cfg.ConstraintDef` for that annotation;
4. Create `META-INF/services/javax.validation.ConstraintValidator` file
and provide your implementation class via it (see
 [Service Loader](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/ServiceLoader.html));
5. Same as 4, but for `lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule`;
6. Include in your `module-info.java` that you provide an implementation
for `javax.validation.ConstraintValidator`;
7. Same as 6, but for `lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule`
8. Write tests for it:
   1. Always include a sanity test so that you'd know you properly
   defined the CDI semantics;
9. Create a readme md explaining the following:
   1. How the rules are consumed;
   2. Which rules are repeatable;
   3. What happens when non-repeating rules are encountered;
   4. Which rules are supported.
   
In cases where your validator implementation can support multiple instances of samey rules, but under different keys, consider creating a ValidatorModule per configuration and filtering out for particular set of samey rules. See countsum validator

See examples validator-repeating and validator-count-sum.
   
