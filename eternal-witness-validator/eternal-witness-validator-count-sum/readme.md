# EW-Validator-CountSum

Rule to validate whether card counts in mainboard and sideboard
match the desired integer ranges.

## Specification

This module implements the following rules:
* `format.mainboard.min`
* `format.mainboard.max`
* `format.sideboard.min`
* `format.sideboard.max`

To configure rules for the particular zone, use
`CardCountSumSideboardModule` and `CardCountSumMainboardModule`
respectively.

Implementation note: multiple instances of those rules
in configuration medium have no affect. Modules
take the first occurrence of rules above. Depending on
the data structure you used when providing an instance
of `Format` to create a validator for it, the order of
first instance of particular rule taken, and the first
instance of rule appearing in your configuration medium
might differ.

## Annotation Definition

Annotation for `CardCountSum` is defined as the following (w.o. required JBV arguments)

```java
@CardCountSum(
 min = 0,
 max = Integer.MAX_VALUE,
 sideboard = false
)
```

Where:
 * sideboard controls which zone is validated
 * min controls how many cards there should be at least in that zone
 * max controls how many cards there should be at most in that zone
 
The annotation is repeatable and should only be applied to
`Collection<MtgDeckCard>` fields, arguments, methods. As noted in
implementation note above, only the first two occurrences matter
in programmatic API. Meanwhile, in XML or Annotation based configurations,
hibernate will validate against each occurrence of the repeated
annotation, but it is not recommended doing it more than twice, once
for each value of `sideboard`.  

