import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumMainboardModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumSideboardModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumValidator;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.countsum {
    requires transitive eternal.witness.validator.api;
    provides ConstraintValidator with CardCountSumValidator;
    exports lt.saltyjuice.dragas.eternalwitness.validator.countsum;
    opens lt.saltyjuice.dragas.eternalwitness.validator.countsum to org.hibernate.validator;
    provides ValidatorModule with CardCountSumMainboardModule, CardCountSumSideboardModule;
}
