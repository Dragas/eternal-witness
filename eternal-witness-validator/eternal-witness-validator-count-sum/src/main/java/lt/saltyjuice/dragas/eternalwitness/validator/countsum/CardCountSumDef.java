package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

import org.hibernate.validator.cfg.ConstraintDef;

public class CardCountSumDef extends ConstraintDef<CardCountSumDef, CardCountSum>
{
    public CardCountSumDef()
    {
        super(CardCountSum.class);
    }

    public CardCountSumDef min(int min) {
        addParameter("min", min);
        return this;
    }

    public CardCountSumDef max(int max) {
        addParameter("max", max);
        return this;
    }

    public CardCountSumDef sideboard(boolean sideboard) {
        addParameter("sideboard", sideboard);
        return this;
    }
}
