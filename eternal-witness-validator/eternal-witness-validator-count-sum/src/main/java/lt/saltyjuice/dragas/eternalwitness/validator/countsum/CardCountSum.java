package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Groups {@link MtgDeckCard} by {@link MtgDeckCard#isSideboard()}, sums
 * their {@link MtgDeckCard#getCount()} values and returns true when
 * the value is between {@link #min()} and {@link #max()}. Must be
 * applied to collection of {@link MtgDeckCard}.
 *
 * <p>
 * Supported classes:
 *     <ul>
 *         <li>{@link MtgDeckCard}</li>
 *     </ul>
 * </p>
 * {@code null} values are considered valid.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(CardCountSum.List.class)
@Documented
@Constraint(validatedBy = {})
public @interface CardCountSum
{
    String message() default "{format.mainboard.min}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return the least amount of cards in the deck's zone for it to be considered valid, inclusive
     */
    int min() default 0;

    /**
     * @return the most amount of cards in the deck's zone for it to be considered valid, inclusive
     */
    int max() default Integer.MAX_VALUE;

    /**
     * @return whether or not this rule applies to sideboard
     */
    boolean sideboard() default false;

    /**
     * Defines several {@link CardCountSum} annotations on the same element.
     *
     * @see CardCountSum
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        CardCountSum[] value();
    }
}
