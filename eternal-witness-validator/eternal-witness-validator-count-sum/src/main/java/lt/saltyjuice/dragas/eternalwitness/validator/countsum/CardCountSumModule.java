package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class CardCountSumModule implements ValidatorModule
{
    public static final String FORMAT_MAINBOARD_MAX = "format.mainboard.max";
    public static final String FORMAT_MAINBOARD_MIN = "format.mainboard.min";
    public static final String FORMAT_SIDEBOARD_MAX = "format.sideboard.max";
    public static final String FORMAT_SIDEBOARD_MIN = "format.sideboard.min";
    public static final String FORMAT_SIDEBOARD_MESSAGE = "{format.sideboard}";
    public static final String FORMAT_MAINBOARD_MESSAGE = "{format.mainboard}";


    @Override
    public Collection<String> getSupportedKeys()
    {
        return Arrays.asList(
                getMaxRuleKey(),
                getMinRuleKey()
        );
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        CardCountSumDef def = new CardCountSumDef();
        def.sideboard(isSideboard());
        formatRulesList
                .stream()
                .filter(this::extractFirstInstanceOfMaxRule)
                .findFirst()
                .map(FormatRule::getValue)
                .map(Integer::parseInt)
                .ifPresent(def::max);
        formatRulesList
                .stream()
                .filter(this::extractFirstInstanceOfMinRule)
                .findFirst()
                .map(FormatRule::getValue)
                .map(Integer::parseInt)
                .ifPresent(def::min);
        def.message(getMessageKey());
        return def;
    }

    protected abstract boolean isSideboard();

    protected abstract String getMaxRuleKey();

    protected abstract String getMinRuleKey();

    protected abstract String getMessageKey();

    private boolean extractFirstInstanceOfMaxRule(FormatRule formatRule)
    {
        return isRule(
                formatRule,
                getMaxRuleKey()
        );
    }

    private boolean extractFirstInstanceOfMinRule(FormatRule formatRule)
    {
        return isRule(
                formatRule,
                getMinRuleKey()
        );
    }
}
