package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

public class CardCountSumMainboardModule extends CardCountSumModule
{
    @Override
    protected boolean isSideboard()
    {
        return false;
    }

    @Override
    protected String getMaxRuleKey()
    {
        return FORMAT_MAINBOARD_MAX;
    }

    @Override
    protected String getMinRuleKey()
    {
        return FORMAT_MAINBOARD_MIN;
    }

    @Override
    protected String getMessageKey()
    {
        return FORMAT_MAINBOARD_MESSAGE;
    }
}
