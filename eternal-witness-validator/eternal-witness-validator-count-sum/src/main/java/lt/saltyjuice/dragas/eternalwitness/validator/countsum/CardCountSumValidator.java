package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

import lt.saltyjuice.dragas.eternalwitness.api.exception.EternalWitnessExcepion;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class CardCountSumValidator implements ConstraintValidator<CardCountSum, Collection<MtgDeckCard>>
{
    private boolean sideboard;
    private int max;
    private int min;

    @Override
    public void initialize(CardCountSum constraintAnnotation)
    {
        this.min = constraintAnnotation.min();
        this.max = constraintAnnotation.max();
        this.sideboard = constraintAnnotation.sideboard();
        assertParameters();
    }

    private void assertParameters()
    {
        if (max < min)
        {
            throw new EternalWitnessExcepion(String.format(
                    "Expected maximum value (%d) to be greater than minimum value (%d)",
                    max,
                    min
            ));
        }
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        if (value == null)
        {
            return true;
        }
        final int sum = value
                .stream()
                .filter(this::matchesSideboardRequirement)
                .mapToInt(this::unwrapArgument)
                .sum();
        return isInRange(sum);
    }

    private boolean isInRange(int sum)
    {
        return sum >= min && sum <= max;
    }

    private int unwrapArgument(MtgDeckCard mtgDeckCard)
    {
        return mtgDeckCard.getCount();
    }

    private boolean matchesSideboardRequirement(MtgDeckCard mtgDeckCard)
    {
        return sideboard == mtgDeckCard.isSideboard();
    }
}
