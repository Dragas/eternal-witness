package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

public class CardCountSumSideboardModule extends CardCountSumModule
{

    @Override
    protected boolean isSideboard()
    {
        return true;
    }

    @Override
    protected String getMaxRuleKey()
    {
        return FORMAT_SIDEBOARD_MAX;
    }

    @Override
    protected String getMinRuleKey()
    {
        return FORMAT_SIDEBOARD_MIN;
    }

    @Override
    protected String getMessageKey()
    {
        return FORMAT_SIDEBOARD_MESSAGE;
    }
}
