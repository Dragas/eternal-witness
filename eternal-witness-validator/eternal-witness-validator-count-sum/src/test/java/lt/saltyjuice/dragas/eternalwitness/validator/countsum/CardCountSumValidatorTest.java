package lt.saltyjuice.dragas.eternalwitness.validator.countsum;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModuleFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class CardCountSumValidatorTest
{
    private static final int REQUIRED_MINIMUM_MAINBOARD = 60;
    private static final int REQUIRED_MAXIMUM_SB = 15;
    private static final AtomicInteger counter = new AtomicInteger();
    private static final ValidatorModuleFactory factory = new ValidatorModuleFactory();
    private static Validator validator;
    private static Format format;

    @BeforeAll
    public static void beforeAll()
    {
        ServiceLoader
                .load(ValidatorModule.class)
                .forEach(factory::install);
        format = new Format();
        format
                .getRules()
                .add(createRule(
                        CardCountSumModule.FORMAT_MAINBOARD_MIN,
                        REQUIRED_MINIMUM_MAINBOARD
                ));
        format
                .getRules()
                .add(createRule(
                        CardCountSumModule.FORMAT_SIDEBOARD_MAX,
                        REQUIRED_MAXIMUM_SB
                ));
        format.setName("Modern");
        validator = factory.getValidator(format);
    }

    private static FormatRule createRule(String key, int value)
    {
        FormatRule formatRule = new FormatRule();
        Rule rule = new Rule();
        formatRule.setId(counter.getAndIncrement());
        rule.setId(counter.getAndIncrement());
        rule.setKey(key);
        formatRule.setRule(rule);
        formatRule.setValue(Integer.toString(value));
        return formatRule;
    }

    @Test
    public void sanityTest()
    {
        Decklist d = createDecklist();
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertTrue(
                result.isEmpty(),
                "Should have no violations"
        );
    }

    @Test
    public void validator_sideboardAboveMaximum_fails()
    {
        Decklist d = createDecklist(createCard(
                REQUIRED_MAXIMUM_SB + 1,
                true
        ));
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertEquals(
                2,
                result.size(),
                "Should have 2 violations"
        );
    }

    @Test
    public void validator_sideboardAboveMaxAndMainBoardConforming_fails()
    {
        Decklist d = createDecklist(
                createCard(
                        REQUIRED_MINIMUM_MAINBOARD,
                        false
                ),
                createCard(
                        REQUIRED_MAXIMUM_SB + 1,
                        true
                )
        );
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertEquals(
                1,
                result.size(),
                "Should have 1 violation"
        );
    }

    @Test
    public void validator_sideboardConformingAndMainboardBelowMin_fails()
    {
        Decklist d = createDecklist(
                createCard(
                        REQUIRED_MINIMUM_MAINBOARD - 1,
                        false
                ),
                createCard(
                        REQUIRED_MAXIMUM_SB,
                        true
                )
        );
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertEquals(
                1,
                result.size(),
                "Should have 1 violation"
        );
    }

    @Test
    public void validator_sideboardConformingAndMainboardConforming_succeeds()
    {
        Decklist d = createDecklist(
                createCard(
                        REQUIRED_MINIMUM_MAINBOARD,
                        false
                ),
                createCard(
                        REQUIRED_MAXIMUM_SB,
                        true
                )
        );
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertEquals(
                0,
                result.size(),
                "Should have 0 violations"
        );
    }

    @Test
    public void validator_emptyDeck_fails()
    {
        Decklist d = new Decklist();
        Set<ConstraintViolation<Decklist>> result = validator.validate(d);
        Assertions.assertEquals(
                1,
                result.size(),
                "Should have 1 violation"
        );
    }

    private Decklist createDecklist()
    {
        return createDecklist(createCard(
                REQUIRED_MINIMUM_MAINBOARD,
                false
        ));
    }

    private Decklist createDecklist(MtgDeckCard... cards)
    {
        Decklist d = new Decklist();
        d
                .getCards()
                .addAll(Arrays.asList(cards));
        return d;
    }

    private MtgDeckCard createCard(int count, boolean sideboard)
    {
        MtgDeckCard card = new MtgDeckCard();
        card.setCount(count);
        card.setSideboard(sideboard);
        card.setId(getNextId());
        return card;
    }

    public long getNextId()
    {
        return counter.getAndIncrement();
    }
}
