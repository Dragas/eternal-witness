import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumMainboardModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumSideboardModule;
import lt.saltyjuice.dragas.eternalwitness.validator.countsum.CardCountSumValidator;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.countsum {
    requires eternal.witness.validator.api;
    provides ConstraintValidator with CardCountSumValidator;
    exports lt.saltyjuice.dragas.eternalwitness.validator.countsum;
    uses ValidatorModule;
    provides ValidatorModule with CardCountSumMainboardModule, CardCountSumSideboardModule;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
