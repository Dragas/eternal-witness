# EW-Validator-Repeating

Rule to validate whether card repeats up to predefined
amount in the deck.

## Specification

This module implements the following rules:
* `format.repetition`
* `format.repetitionex` (repeatable)

To configure the rule, use `RepetitionCountModule`

Implementation note: module takes only the first
occurrence of `format.repetition` rule. Meanwhile
`format.repetitionex` can be repeated multiple times.
The repeated values are merged into an array of strings
and assigned as `exceptions` field on the annotation.
The validator resolves repeating instances of `MtgCard` wrappers,
since they might appear in both sideboard and mainboard.
It is up to user to resolve the multiple language issue.

## Annotation Definition

Annotation for `RepetitionCount` is defined as the following (w.o. required JBV arguments)

```java
@RepetitionCount(
    int max(),
    String[] exceptions() default {};
)
```

Where:
 * max controls the maximum possible value of `MtgDeckCard#getCount()`
 * exceptions control a string array of `MtgCard#getName()` that would
 be exempt from the check
 
 
The annotation is not repeatable and should only be applied to
`Collection<MtgDeckCard>` fields, arguments, methods.

