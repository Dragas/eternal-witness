package lt.saltyjuice.dragas.eternalwitness.validator.repeating;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModuleFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class RepeatingValidatorTest
{
    private static final int PREDEFINED_MAXIMUM = 4;
    private static final String SWAMP = "Swamp";
    private static final String FOREST = "Forest";
    private static final String EXCEPTION = SWAMP;
    private static final AtomicInteger counter = new AtomicInteger();
    private static final ValidatorModuleFactory factory = new ValidatorModuleFactory();
    private static Validator validator;
    private static Format format;

    @BeforeAll
    public static void beforeAll()
    {
        ServiceLoader
                .load(ValidatorModule.class)
                .forEach(factory::install);
        format = new Format();
        format
                .getRules()
                .add(createRule(
                        RepetitionCountModule.FORMAT_REPETITION,
                        PREDEFINED_MAXIMUM
                ));
        format
                .getRules()
                .add(createRule(
                        RepetitionCountModule.FORMAT_REPETITION_EXCEPTION,
                        EXCEPTION
                ));
        format.setName("Modern");
        validator = factory.getValidator(format);
    }

    private static FormatRule createRule(String key, String value)
    {
        FormatRule formatRule = new FormatRule();
        Rule rule = new Rule();
        formatRule.setId(counter.getAndIncrement());
        rule.setId(counter.getAndIncrement());
        rule.setKey(key);
        formatRule.setRule(rule);
        formatRule.setValue(value);
        return formatRule;
    }

    private static FormatRule createRule(String key, int value)
    {
        return createRule(
                key,
                Integer.toString(value)
        );
    }

    @Test
    public void sanityTest()
    {
        Decklist d = createDecklist();
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertTrue(
                violations.isEmpty(),
                "There should be no violations"
        );
    }

    @Test
    public void validator_withMoreForestsThanRequired_fails()
    {
        Decklist d = createDecklist(
                FOREST,
                PREDEFINED_MAXIMUM + 1
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertFalse(
                violations.isEmpty(),
                "There should violations"
        );
    }

    @Test
    public void validator_withMoreSwampsThanRequired_succeeds()
    {
        Decklist d = createDecklist(
                EXCEPTION,
                PREDEFINED_MAXIMUM * 15
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertTrue(
                violations.isEmpty(),
                "There should be no violations"
        );
    }

    @Test
    public void validator_withMultipleConformingCards_succeeds()
    {
        Decklist d = createDecklist(
                createMtgCard(
                        FOREST,
                        1
                ),
                createMtgCard(
                        FOREST,
                        2
                )
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertTrue(
                violations.isEmpty(),
                "There should be no violations"
        );
    }

    @Test
    public void validator_withConformingAndNonConformingCard_fails()
    {
        Decklist d = createDecklist(
                createMtgCard(
                        FOREST,
                        PREDEFINED_MAXIMUM * 15
                ),
                createMtgCard(
                        FOREST,
                        2
                )
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertEquals(
                1,
                violations.size(),
                "There should be 1 violation"
        );
    }

    @Test
    public void validator_withConformingAndException_succeeds()
    {
        Decklist d = createDecklist(
                createMtgCard(
                        EXCEPTION,
                        PREDEFINED_MAXIMUM * 15
                ),
                createMtgCard(
                        FOREST,
                        2
                )
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertTrue(
                violations.isEmpty(),
                "There should be no violations"
        );
    }

    @Test
    public void validator_withNonConformingAndException_fails()
    {
        Decklist d = createDecklist(
                createMtgCard(
                        FOREST,
                        PREDEFINED_MAXIMUM * 15
                ),
                createMtgCard(
                        EXCEPTION,
                        PREDEFINED_MAXIMUM * 15
                )
        );
        Set<ConstraintViolation<Decklist>> violations = validator.validate(d);
        Assertions.assertEquals(
                1,
                violations.size(),
                "There should be 1 violation"
        );
    }

    private Decklist createDecklist()
    {
        return createDecklist(
                FOREST,
                PREDEFINED_MAXIMUM
        );
    }

    private Decklist createDecklist(String cardName, int count)
    {
        return createDecklist(createMtgCard(
                cardName,
                count
        ));
    }

    private Decklist createDecklist(MtgDeckCard... cards)
    {
        Decklist d = new Decklist();
        d
                .getCards()
                .addAll(Arrays.asList(cards));
        return d;
    }

    private MtgDeckCard createMtgCard(String cardName, int count)
    {
        MtgDeckCard card = new MtgDeckCard();
        MtgCard mtgCard = new MtgCard();
        mtgCard.setName(cardName);
        card.setCard(mtgCard);
        card.setCount(count);
        card.setId(getNextId());
        return card;
    }

    private long getNextId()
    {
        return counter.getAndIncrement();
    }
}
