import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.repeating.RepetitionCountModule;
import lt.saltyjuice.dragas.eternalwitness.validator.repeating.RepetitionCountValidator;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.repeating {
    requires eternal.witness.validator.api;
    provides ConstraintValidator with RepetitionCountValidator;
    exports lt.saltyjuice.dragas.eternalwitness.validator.repeating;
    uses ValidatorModule;
    provides ValidatorModule with RepetitionCountModule;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
