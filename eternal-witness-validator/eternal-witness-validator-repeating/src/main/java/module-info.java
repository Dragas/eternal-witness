import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.repeating.RepetitionCountModule;
import lt.saltyjuice.dragas.eternalwitness.validator.repeating.RepetitionCountValidator;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.repeating {
    requires transitive eternal.witness.validator.api;
    provides ConstraintValidator with RepetitionCountValidator;
    exports lt.saltyjuice.dragas.eternalwitness.validator.repeating;
    opens lt.saltyjuice.dragas.eternalwitness.validator.repeating to org.hibernate.validator;
    provides ValidatorModule with RepetitionCountModule;
}
