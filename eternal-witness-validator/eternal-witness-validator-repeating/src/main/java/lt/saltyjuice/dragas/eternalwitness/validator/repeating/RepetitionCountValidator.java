package lt.saltyjuice.dragas.eternalwitness.validator.repeating;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class RepetitionCountValidator implements ConstraintValidator<RepetitionCount, Collection<MtgDeckCard>>
{
    private int min;
    private Collection<String> exceptions;

    @Override
    public void initialize(RepetitionCount constraintAnnotation)
    {
        this.min = constraintAnnotation.max();
        this.exceptions = Arrays.asList(constraintAnnotation.exceptions());
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        if (value == null)
        {
            return true;
        }
        final List<MtgDeckCard> conflictingCards = value
                .stream()
                .collect(Collectors.groupingBy(it -> it
                        .getCard()
                        .getName()))
                .values()
                .stream()
                .map(it -> it
                        .stream()
                        .reduce(this::accumulateMtgDeckCard)
                        .get())
                .filter(this::excludeCard)
                .filter(this::countIsGreaterThanConfigured)
                .collect(Collectors.toList());
        return conflictingCards.isEmpty();
    }

    private boolean countIsGreaterThanConfigured(MtgDeckCard mtgDeckCard)
    {
        return mtgDeckCard.getCount() > min;
    }

    private String getCardName(MtgDeckCard card)
    {
        return card
                .getCard()
                .getName();
    }

    private boolean excludeCard(MtgDeckCard card)
    {
        return !exceptions.contains(getCardName(card));
    }



    private MtgDeckCard accumulateMtgDeckCard(MtgDeckCard mtgDeckCard, MtgDeckCard mtgDeckCard1)
    {
        MtgDeckCard card = new MtgDeckCard();
        card.setCount(mtgDeckCard.getCount() + mtgDeckCard1.getCount());
        card.setCard(mtgDeckCard.getCard());
        return card;
    }
}
