package lt.saltyjuice.dragas.eternalwitness.validator.repeating;

import org.hibernate.validator.cfg.ConstraintDef;

public class RepetitionCountDef extends ConstraintDef<RepetitionCountDef, RepetitionCount>
{
    public RepetitionCountDef()
    {
        super(RepetitionCount.class);
    }

    public RepetitionCountDef exceptions(String... exceptions) {
        addParameter("exceptions", exceptions);
        return this;
    }

    public RepetitionCountDef max(int max) {
        addParameter("max", max);
        return this;
    }
}
