package lt.saltyjuice.dragas.eternalwitness.validator.repeating;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validates if particular {@link MtgDeckCard} has a {@link MtgDeckCard#getCount()}
 * value lower than or equal to {@link #max()} value. It is possible to define a
 * collection of exceptions via {@link #exceptions()} to this rule using card string
 * names. This is to work around an issue where decks do not contain their card ids,
 * but rather the names, and it is out of scope to define a translator or something.
 * <br>
 * This annotation must be put on collection of {@link MtgDeckCard} elements.
 *
 * <p>
 * Supported types are:
 *     <ul>
 *         <li>{@link MtgDeckCard}</li>
 *     </ul>
 * </p>
 * <p>
 * {@code null} elements are considered valid.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface RepetitionCount
{
    String message() default "{format.repetition}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return Defines how many times a particular card can repeat in a deck at most
     */
    int max();

    /**
     * @return Defines which cards are exception to the rule in your deck's language
     */
    String[] exceptions() default {};
}
