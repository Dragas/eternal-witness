package lt.saltyjuice.dragas.eternalwitness.validator.repeating;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class RepetitionCountModule implements ValidatorModule
{

    public static final String FORMAT_REPETITION = "format.repetition";
    public static final String FORMAT_REPETITION_EXCEPTION = "format.repetitionex";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Arrays.asList(
                FORMAT_REPETITION,
                FORMAT_REPETITION_EXCEPTION
        );
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        String[] exceptions = formatRulesList
                .stream()
                .filter(this::isExceptionRule)
                .map(FormatRule::getValue)
                .toArray(String[]::new);
        Integer max = formatRulesList
                .stream()
                .filter(this::isRepetitionCountRule)
                .findFirst()
                .map(FormatRule::getValue)
                .map(Integer::valueOf)
                .get();
        return new RepetitionCountDef()
                .exceptions(exceptions)
                .max(max);
    }

    private boolean isExceptionRule(FormatRule formatRule)
    {
        return isRule(
                formatRule,
                FORMAT_REPETITION_EXCEPTION
        );
    }

    private boolean isRepetitionCountRule(FormatRule formatRule)
    {
        return isRule(
                formatRule,
                FORMAT_REPETITION
        );
    }
}
