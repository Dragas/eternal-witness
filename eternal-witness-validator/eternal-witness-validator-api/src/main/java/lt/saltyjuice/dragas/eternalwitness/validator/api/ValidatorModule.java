package lt.saltyjuice.dragas.eternalwitness.validator.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.api.model.Rule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Collection;
import java.util.List;

/**
 * Validator modules bridge eternal witness rule implementations
 * with hibernate validator's programmatic configuration API, by
 * providing an interface to convert runtime EW configuration into
 * hibernate runtime configuration. It is expected (while not enforced)
 * that each module should return only a single type of {@link ConstraintDef}
 * implementation.
 */
public interface ValidatorModule
{
    /**
     * @return Returns a list of keys that are supported by this factory
     * @see Rule#getKey()
     */
    Collection<String> getSupportedKeys();

    /**
     * Helper method for all validator modules to determine if particular rule matches the key
     * @param formatRule rule to test
     * @param key the key to test against
     * @return true, if the keys match
     */
    default boolean isRule(FormatRule formatRule, String key)
    {
        return key.equals(formatRule
                .getRule()
                .getKey());
    }

    /**
     * Creates a single constraint def implementation which can
     * be used by hibernate configure validators and validate
     * {@link lt.saltyjuice.dragas.eternalwitness.api.model.Decklist}
     * pojos against them.
     * @param formatRulesList filtered list of rules in accordance to {@link #getSupportedKeys()}
     * @return Configured Constraint Definition
     */
    ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList);
}
