package lt.saltyjuice.dragas.eternalwitness.validator.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.Decklist;
import lt.saltyjuice.dragas.eternalwitness.api.model.Format;
import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.cfg.ConstraintDef;
import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.context.Constrainable;
import org.hibernate.validator.cfg.context.PropertyConstraintMappingContext;
import org.hibernate.validator.cfg.context.TypeConstraintMappingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

/**
 * Factory implementation for validator modules, which
 * consumes {@link Format} configurations, installs
 * {@link ValidatorModule} implementations and provides
 * configured {@link Validator} implementations per format.
 * <br>
 * Validators are cached per {@link Format#getName()},
 * but can be refreshed by calling {@link #refresh(String)}
 * to clear the prebuilt validator for that format.
 * <br>
 * The module is {@link lt.saltyjuice.dragas.eternalwitness.api.service.FormatService} agnostic,
 * so that it could be used standalone, without the database or
 * any particular implementation of that service. As a result,
 * it is up to users to figure out how to configure and store
 * the rules.
 * <br>
 * You must not retain reference to validators provided
 * by {@link #getValidator(Format)}, because
 */
public class ValidatorModuleFactory implements AutoCloseable
{
    private static final Logger LOG = LoggerFactory.getLogger(ValidatorModuleFactory.class);
    private final Map<String, ValidatorWrapper> validatorMap;
    private final Map<String, ValidatorModule> moduleMap;

    /**
     * Creates a validator factory with an empty backing map.
     */
    public ValidatorModuleFactory()
    {
        this(
                new HashMap<>(),
                new HashMap<>()
        );
    }

    /**
     * Creates validator factory with provided backing map and wraps that into
     * {@link Collections#synchronizedMap(Map)} to make this implementation
     * a bit more thread safe.
     *
     * @param validatorMap backing map either an empty one or provided by
     *                     other ValidatorModuleFactory
     * @param moduleMap    backing map either an empty one or provided by
     *                     other ValidatorModuleFactory
     */
    public ValidatorModuleFactory(Map<String, ValidatorWrapper> validatorMap, Map<String, ValidatorModule> moduleMap)
    {
        TreeMap<String, ValidatorWrapper> treeMap = new TreeMap<>(validatorMap);
        TreeMap<String, ValidatorModule> moduleTreeMap = new TreeMap<>(moduleMap);
        this.validatorMap = Collections.synchronizedSortedMap(treeMap);
        this.moduleMap = Collections.synchronizedSortedMap(moduleTreeMap);
    }

    /**
     * Returns a copy of backing map for modules of this factory.
     * Changes done to this are not reflected in actual factory's
     * module map.
     *
     * @return map of currently configured modules
     */
    public Map<String, ValidatorModule> getBackingModuleMap()
    {
        return new HashMap<>(this.moduleMap);
    }

    /**
     * Returns a copy of the backing map of this factory. Changes done
     * to this are not reflected in the actual factory's validator
     * map.
     * <br>
     * Closing this factory will render all of validators in this map useless.
     *
     * @return map of currently configured validators
     */
    public Map<String, ValidatorWrapper> getBackingValidatorCache()
    {
        LOG.trace(
                "Requesting backing map with {} elements",
                validatorMap.size()
        );
        return new HashMap<>(validatorMap);
    }

    /**
     * Removes preconfigured validator from the backing map. Useful
     * when you need to reconfigure validator for that particular name.
     * After this call, the validator that was provided for that name
     * will be rendered useless because it will be considered closed
     * as well.
     *
     * @param formatName name of the validator
     */
    public void refresh(String formatName)
    {
        LOG.trace(
                "Trying to remove validator for format named '{}' from the map",
                formatName
        );
        final ValidatorWrapper remove = validatorMap.remove(formatName);
        if (remove == null)
        {
            LOG.warn(
                    "Trying to remove non existent validator for format '{}'",
                    formatName
            );
        }
        else
        {
            remove.close();
            LOG.info(
                    "Removed validator for format '{}'",
                    formatName
            );
        }
    }

    /**
     * Installs the provided validator module so that it could provide
     * the means how to convert {@link FormatRule} into {@link org.hibernate.validator.cfg.ConstraintDef}
     * for validators. In case two modules clash in supported keys,
     * they don't get overwritten. Instead you need to explicitly
     * call {@link #uninstall(ValidatorModule)} on the module you
     * want to remove.
     *
     * @param module module to install
     */
    public void install(ValidatorModule module)
    {
        Collection<String> supportedKeys = module.getSupportedKeys();
        LOG.trace(
                "Trying to install module {} for keys '{}'",
                module,
                supportedKeys
        );
        for (String key : supportedKeys)
        {
            if (!moduleMap.containsKey(key))
            {
                moduleMap.put(
                        key,
                        module
                );
                LOG.info(
                        "Installed module {} for key {}",
                        module,
                        key
                );
            }
            else
            {
                LOG.warn(
                        "There's already a module installed for key {}",
                        key
                );
            }
        }
    }

    /**
     * Uninstalls particular module so that it could no longer produce
     * constraint definitions for future validators.
     *
     * @param module to remove. Tested by reference rather than keys.
     */
    public void uninstall(ValidatorModule module)
    {
        Collection<String> supportedKeys = module.getSupportedKeys();
        LOG.trace(
                "Trying to uninstall module {} for keys '{}'",
                module,
                supportedKeys
        );
        for (String key : supportedKeys)
        {
            ValidatorModule installedModule = moduleMap.get(key);
            if (module == installedModule)
            {
                moduleMap.remove(key);
                LOG.info(
                        "Uninstalled module {} for key {}",
                        module,
                        key
                );
            }
            else
            {
                LOG.warn(
                        "Attempted to uninstall a non-equivalent instance of module for key {}",
                        key
                );
            }
        }
    }

    /**
     * Returns validator for format. In case that format's rules had
     * changed since last time this method was called, it is up to
     * users to call {@link #refresh(String)} to invalidate the
     * previously returned validator.
     *
     * @param format the format to create validator for
     * @return new validator or the one from cache
     */
    public Validator getValidator(Format format)
    {
        String formatName = format.getName();
        LOG.trace(
                "Trying to fetch validator for '{}' from cache",
                formatName
        );
        if (validatorMap.containsKey(formatName))
        {
            LOG.info(
                    "Fetched validator for '{}' from cache",
                    formatName
            );
            return validatorMap
                    .get(formatName)
                    .getValidator();
        }
        LOG.trace(
                "Validator for '{}' isn't present in cache, so need to fire it up",
                formatName
        );
        Collection<FormatRule> copy = new ArrayList<>(format.getRules());
        Map<ValidatorModule, List<FormatRule>> groupsPerModule = new HashMap<>();
        for (FormatRule rule : copy)
        {
            String key = rule
                    .getRule()
                    .getKey();
            LOG.trace(
                    "Attempting to get rule implementation for key '{}'",
                    key
            );
            ValidatorModule module = moduleMap.get(key);
            if (module != null)
            {
                LOG.trace(
                        "Implementation for '{}' is present, adding it to group '{}'",
                        key,
                        module
                );
                Collection<FormatRule> groupForModule = groupsPerModule.computeIfAbsent(
                        module,
                        (it) -> new ArrayList<>()
                );
                groupForModule.add(rule);
            }
            else
            {
                LOG.warn(
                        "No such rule implementation for key '{}'",
                        key
                );
            }
        }
        if (!groupsPerModule.isEmpty())
        {
            LOG.info(
                    "Creating a validator with {} modules for {} rules",
                    groupsPerModule.size(),
                    copy.size()
            );
        }
        else
        {
            LOG.warn("No implementations found for provided rules. In the future, this will throw instead of returning null");
            return null;
        }
        ValidatorWrapper wrapper = createValidatorWrapper(groupsPerModule);
        validatorMap.put(
                formatName,
                wrapper
        );
        return wrapper.getValidator();
    }

    protected ValidatorWrapper createValidatorWrapper(Map<ValidatorModule, List<FormatRule>> groupsPerModule)
    {
        HibernateValidatorConfiguration configuration = Validation
                .byProvider(HibernateValidator.class)
                .configure();
        final ConstraintMapping constraintMapping = configuration.createConstraintMapping();
        TypeConstraintMappingContext<Decklist> mapping = constraintMapping
                .type(Decklist.class);
        PropertyConstraintMappingContext constraintDefs = groupsPerModule
                .entrySet()
                .stream()
                .map(this::entryToConstraintDef)
                .reduce(
                        mapping.getter("cards"),
                        (Constrainable::constraint),
                        (it1, it2) -> it1
                );
        ValidatorFactory factory = configuration
                .addMapping(constraintMapping)
                .buildValidatorFactory();
        Validator validator = factory.getValidator();
        return new ValidatorWrapper(
                configuration,
                factory,
                validator
        );
    }

    private ConstraintDef entryToConstraintDef(Map.Entry<ValidatorModule, List<FormatRule>> validatorModuleListEntry)
    {
        ValidatorModule module = validatorModuleListEntry.getKey();
        List<FormatRule> rules = validatorModuleListEntry.getValue();
        return module.getConstraintImpl(rules);
    }

    @Override
    public void close() throws Exception
    {
        validatorMap
                .values()
                .forEach(ValidatorWrapper::close);
    }

    /**
     * In hibernate-validator, apparently it's expensive to instantiate configuration,
     * and {@link ValidatorFactory} are closeable. If you were to close the factory
     * before getting rid off validator, it will stop working. As a result, it's
     * necessary to keep everything about the source of that validator.
     */
    public static class ValidatorWrapper implements AutoCloseable
    {
        private final HibernateValidatorConfiguration configuration;
        private final Validator validator;
        private final ValidatorFactory factory;

        private ValidatorWrapper(HibernateValidatorConfiguration configuration, ValidatorFactory factory, Validator validator)
        {
            this.configuration = configuration;
            this.factory = factory;
            this.validator = validator;
        }

        public Validator getValidator()
        {
            return validator;
        }

        @Override
        public void close()
        {
            factory.close();
        }
    }
}
