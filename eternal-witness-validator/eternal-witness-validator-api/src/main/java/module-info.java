module eternal.witness.validator.api {
    requires transitive org.hibernate.validator;
    requires transitive eternal.witness.api;
    requires transitive java.validation;
    requires slf4j.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.api;
}
