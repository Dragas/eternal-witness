package lt.saltyjuice.dragas.eternalwitness.validator.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class MockConstraintValidator implements ConstraintValidator<MockConstraint, Collection<MtgDeckCard>>
{
    private int min;

    @Override
    public void initialize(MockConstraint constraintAnnotation)
    {
        this.min = constraintAnnotation.min();
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        return value == null || value.size() >= min;
    }
}
