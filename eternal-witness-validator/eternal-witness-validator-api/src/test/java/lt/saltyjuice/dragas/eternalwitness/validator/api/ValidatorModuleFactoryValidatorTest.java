package lt.saltyjuice.dragas.eternalwitness.validator.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatorModuleFactoryValidatorTest
{

    private static Decklist DEFAULT_DECKLIST;
    private static Format format;
    private ValidatorModuleFactory factory;
    private MockModule module;

    @BeforeAll
    public static void setUpAll()
    {
        format = new Format();
        format.setName("Mockern");
        Rule rule = new Rule();
        rule.setKey(MockModule.DEFAULT_KEY);
        FormatRule formatRule = new FormatRule();
        formatRule.setFormat(format);
        formatRule.setRule(rule);
        formatRule.setValue("1");
        format
                .getRules()
                .add(formatRule);
        DEFAULT_DECKLIST = new Decklist();
        DEFAULT_DECKLIST
                .getCards()
                .add(new MtgDeckCard());
    }

    @BeforeEach
    void setUp()
    {
        factory = new ValidatorModuleFactory();
        module = new MockModule();
        factory.install(module);
    }

    @Test
    public void factory_forMockFormat_createsValidator()
    {
        Validator v = factory.getValidator(format);
        assertValidatorWorks(v);
    }

    @Test
    public void factory_forMockFormatRefresh_removesValidator() {
        Validator v = factory.getValidator(format);
        factory.refresh(format.getName());
        assertTrue(factory.getBackingValidatorCache().isEmpty(), "Should be empty");
    }

    @Test
    public void factory_ForMockFormatAndUninstalledModule_doesNotCreateValidator() {
        factory.uninstall(module);
        Validator v = factory.getValidator(format);
        assertNull(v, "Should not be able to produce it");
    }

    @Test
    public void factory_forMockFormatWithoutRules_doesNotCreateValidator() {
        Format format = new Format();
        format.setName("asdf");
        Validator v = factory.getValidator(format);
        assertNull(v, "Should not be able to produce it");
    }

    @Test
    public void factory_forMockFormatWithoutSupportedRules_doesNotCreateValidator() {
        Format format = new Format();
        format.setName("asdf");
        final FormatRule formatRule = new FormatRule();
        final Rule rule = new Rule();
        rule.setKey("bar");
        formatRule.setRule(rule);
        format.getRules().add(formatRule);
        Validator v = factory.getValidator(format);
        assertNull(v, "Should not be able to produce it");
    }

    private void assertValidatorWorks(Validator v)
    {
        assertNotNull(
                v,
                "Validator should not be null"
        );
        Map<String, ValidatorModuleFactory.ValidatorWrapper> validatorMap = factory.getBackingValidatorCache();
        assertEquals(
                v,
                validatorMap
                        .get(format.getName())
                        .getValidator(),
                "This validator should always be returned for this validator"
        );
        Set<ConstraintViolation<Decklist>> validationfailures = v.validate(DEFAULT_DECKLIST);
        assertTrue(validationfailures.isEmpty());
        assertFalse(
                v
                        .validate(new Decklist())
                        .isEmpty(),
                "Should not be empty for empty decklist"
        );
    }


    @AfterEach
    void tearDown() throws Exception
    {
        factory.close();
    }
}
