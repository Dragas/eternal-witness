package lt.saltyjuice.dragas.eternalwitness.validator.api;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MockModule implements ValidatorModule
{
    public static final String DEFAULT_KEY = "foo";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Collections.singleton(DEFAULT_KEY);
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        FormatRule rule = formatRulesList.get(0);
        MockConstraintDef def = new MockConstraintDef();
        def.min(Integer.parseInt(rule.getValue()));
        return def;
    }
}
