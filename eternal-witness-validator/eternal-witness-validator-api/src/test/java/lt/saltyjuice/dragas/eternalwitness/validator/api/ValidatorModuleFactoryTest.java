package lt.saltyjuice.dragas.eternalwitness.validator.api;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorModuleFactoryTest
{

    private ValidatorModuleFactory factory;

    @BeforeEach
    void setUp()
    {
        factory = new ValidatorModuleFactory();
    }

    @Test
    public void factory_installsMockModule()
    {
        factory.install(new MockModule());
        Map<String, ValidatorModule> moduleMap = factory.getBackingModuleMap();
        assertNotNull(
                moduleMap.get(MockModule.DEFAULT_KEY),
                "module should be installed"
        );
    }

    @Test
    public void factory_doesNotOverwriteInstalledModule()
    {
        final MockModule module = new MockModule();
        factory.install(module);
        factory.install(new MockModule());
        Map<String, ValidatorModule> moduleMap = factory.getBackingModuleMap();
        assertEquals(
                module,
                moduleMap.get(MockModule.DEFAULT_KEY),
                "module should not be overwritten"
        );
    }

    @Test
    public void factory_uninstallsProvidedModule()
    {
        final MockModule module = new MockModule();
        factory.install(module);
        factory.uninstall(module);
        Map<String, ValidatorModule> moduleMap = factory.getBackingModuleMap();
        assertTrue(
                moduleMap.isEmpty(),
                "module should be uninstalled"
        );
    }

    @Test
    public void factory_doesNotUninstallAnotherInstanceOfSameModule()
    {
        final MockModule module = new MockModule();
        factory.install(module);
        factory.uninstall(new MockModule());
        Map<String, ValidatorModule> moduleMap = factory.getBackingModuleMap();
        assertFalse(
                moduleMap.isEmpty(),
                "module should not be uninstalled"
        );
    }

    @AfterEach
    void tearDown() throws Exception
    {
        factory.close();
    }
}
