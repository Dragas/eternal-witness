package lt.saltyjuice.dragas.eternalwitness.validator.api;


import org.hibernate.validator.cfg.ConstraintDef;

public class MockConstraintDef extends ConstraintDef<MockConstraintDef, MockConstraint>
{
    public MockConstraintDef()
    {
        super(MockConstraint.class);
    }

    public MockConstraintDef min(int min) {
        addParameter("min", min);
        return this;
    }
}
