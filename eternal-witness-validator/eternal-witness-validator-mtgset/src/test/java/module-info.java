import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.mtgset.PermittedMtgSetModule;
import lt.saltyjuice.dragas.eternalwitness.validator.mtgset.PermittedMtgSetValidator;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.mtgset {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.mtgset;
    provides ConstraintValidator with PermittedMtgSetValidator;
    provides ValidatorModule with PermittedMtgSetModule;
    uses ValidatorModule;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
