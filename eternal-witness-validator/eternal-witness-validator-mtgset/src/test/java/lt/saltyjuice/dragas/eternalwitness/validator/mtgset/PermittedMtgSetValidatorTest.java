package lt.saltyjuice.dragas.eternalwitness.validator.mtgset;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModuleFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PermittedMtgSetValidatorTest
{
    private static final String BANNED_SET_NAME = "MSN";
    private static final String MODERN = "MODERN";
    private static final String MAXIMUM_DATE = "2020-12-31";
    private static final String MINIMUM_DATE = "1990-01-01";
    private static final String DATE_PROPER = "2001-09-11";
    private static final String DATE_AFTER_MAX = "2021-12-31";
    private static final String DATE_BEFORE_MIN = "1984-04-20";
    private static final AtomicLong COUNTER = new AtomicLong();
    private static Format format;
    private static Decklist BANNED_SET_DECK;
    private static Decklist PROPER_TIME_SET_DECK;
    private static Decklist AFTER_MAX_SET_DECK;
    private static Decklist BEFORE_MIN_SET_DECK;
    private static MtgSet BEFORE_MIN_SET;
    private static MtgSet AFTER_MAX_SET;
    private static MtgSet PROPER_TIME_SET;
    private static MtgSet BANNED_SET;
    private Validator validator;
    private ValidatorModuleFactory factory;

    @BeforeAll
    public static void setUpSuite()
    {
        format = new Format();
        format.setName(MODERN);
        format
                .getRules()
                .add(
                        createFormatRule(
                                PermittedMtgSetModule.FORMAT_SET_BANNED,
                                BANNED_SET_NAME
                        )
                );
        format
                .getRules()
                .add(
                        createFormatRule(
                                PermittedMtgSetModule.FORMAT_SET_AFTER,
                                MINIMUM_DATE
                        )
                );
        format
                .getRules()
                .add(
                        createFormatRule(
                                PermittedMtgSetModule.FORMAT_SET_BEFORE,
                                MAXIMUM_DATE
                        )
                );
        BEFORE_MIN_SET = createSet(DATE_BEFORE_MIN);
        AFTER_MAX_SET = createSet(DATE_AFTER_MAX);
        PROPER_TIME_SET = createSet(DATE_PROPER);
        BANNED_SET = createSet(
                BANNED_SET_NAME,
                DATE_PROPER
        );
        BANNED_SET_DECK = createDeckList(BANNED_SET);
        BEFORE_MIN_SET_DECK = createDeckList(BEFORE_MIN_SET);
        AFTER_MAX_SET_DECK = createDeckList(AFTER_MAX_SET);
        PROPER_TIME_SET_DECK = createDeckList(PROPER_TIME_SET);
    }

    private static Decklist createDeckList(MtgSet... sets)
    {
        Decklist decklist = new Decklist();
        for (MtgSet set : sets)
        {
            MtgDeckCard deckCard = createMtgDeckCard(set);
            decklist
                    .getCards()
                    .add(deckCard);
        }
        return decklist;
    }

    private static MtgDeckCard createMtgDeckCard(MtgSet set)
    {
        MtgDeckCard deckCard = new MtgDeckCard();
        MtgCard card = createMtgCard(set);
        deckCard.setCard(card);
        deckCard.setId(COUNTER.getAndIncrement());
        return deckCard;
    }

    private static MtgCard createMtgCard(MtgSet set)
    {
        MtgCard card = new MtgCard();
        MtgCardSet cardSet = createMtgCardSet(
                set
        );
        cardSet.setCard(card);
        card
                .getSets()
                .add(cardSet);
        return card;
    }

    private static MtgCardSet createMtgCardSet(MtgSet set)
    {
        MtgCardSet cardSet = new MtgCardSet();
        cardSet.setSet(set);
        return cardSet;
    }

    private static MtgSet createSet(String date)
    {
        return createSet(
                "",
                date
        );
    }

    private static MtgSet createSet(String shorthand, String date)
    {
        MtgSet set = new MtgSet();
        set.setShortName(shorthand);
        set.setRelease(LocalDate.parse(date));
        return set;
    }

    private static FormatRule createFormatRule(String key, String value)
    {
        Rule rule = createRule(key);
        FormatRule formatRule = new FormatRule();
        formatRule.setRule(rule);
        formatRule.setValue(value);
        formatRule.setId(COUNTER.getAndIncrement());
        rule.setId(COUNTER.getAndIncrement());
        return formatRule;
    }

    private static Rule createRule(String ruleKey)
    {
        Rule rule = new Rule();
        rule.setKey(ruleKey);
        return rule;
    }

    @BeforeEach
    public void setUp()
    {
        factory = new ValidatorModuleFactory();
        ServiceLoader<ValidatorModule> loader = ServiceLoader.load(ValidatorModule.class);
        for (ValidatorModule module : loader)
        {
            factory.install(module);
        }
        validator = factory.getValidator(format);
    }

    @AfterEach
    public void tearDown() throws Exception
    {
        validator = null;
        factory.close();
    }

    @Test
    public void validator_forBannedSetCard_fails() {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(BANNED_SET_DECK);
        assertFalse(violations.isEmpty());
    }
    @Test
    public void validator_forBeforeMinCard_fails() {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(BEFORE_MIN_SET_DECK);
        assertFalse(violations.isEmpty());
    }
    @Test
    public void validator_forAfterMaxCard_fails() {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(AFTER_MAX_SET_DECK);
        assertFalse(violations.isEmpty());
    }
    @Test
    public void validator_forProperCard_succeeds() {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(PROPER_TIME_SET_DECK);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void validator_forMixedBannedAndProper_fails() {
        Decklist mixed = createDeckList(PROPER_TIME_SET, BANNED_SET);
        Set<ConstraintViolation<Decklist>> violations = validator.validate(mixed);
        assertFalse(violations.isEmpty());
    }
    @Test
    public void validator_forMixedBeforeAndProper_fails() {
        Decklist mixed = createDeckList(PROPER_TIME_SET, BEFORE_MIN_SET);
        Set<ConstraintViolation<Decklist>> violations = validator.validate(mixed);
        assertFalse(violations.isEmpty());
    }
    @Test
    public void validator_forMixedAfterAndProper_fails() {
        Decklist mixed = createDeckList(PROPER_TIME_SET, AFTER_MAX_SET);
        Set<ConstraintViolation<Decklist>> violations = validator.validate(mixed);
        assertFalse(violations.isEmpty());
    }
}
