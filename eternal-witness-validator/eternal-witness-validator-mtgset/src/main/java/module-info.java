import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.mtgset.PermittedMtgSetModule;
import lt.saltyjuice.dragas.eternalwitness.validator.mtgset.PermittedMtgSetValidator;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.mtgset {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.mtgset;
    opens lt.saltyjuice.dragas.eternalwitness.validator.mtgset to org.hibernate.validator;
    provides ConstraintValidator with PermittedMtgSetValidator;
    provides ValidatorModule with PermittedMtgSetModule;
}
