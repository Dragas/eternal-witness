package lt.saltyjuice.dragas.eternalwitness.validator.mtgset;

import org.hibernate.validator.cfg.ConstraintDef;

public class PermittedMtgSetDef extends ConstraintDef<PermittedMtgSetDef, PermittedMtgSet>
{
    public PermittedMtgSetDef()
    {
        super(PermittedMtgSet.class);
    }

    public PermittedMtgSetDef before(String before) {
        addParameter("before", before);
        return this;
    }
    public PermittedMtgSetDef after(String after) {
        addParameter("after", after);
        return this;
    }
    public PermittedMtgSetDef banned(String... banned) {
        addParameter("banned", banned);
        return this;
    }
}
