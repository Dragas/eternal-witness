package lt.saltyjuice.dragas.eternalwitness.validator.mtgset;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Reduces {@link MtgDeckCard} to {@link MtgCard#getSets()}
 * collections and then filters those out by asserting the
 * provided arguments for {@link #before()}, {@link #after()},
 * and {@link #banned()}. Must be applied to collection of
 * {@link MtgDeckCard}.
 *
 * <p>
 * Supported classes:
 *     <ul>
 *         <li>
 *              {@link MtgDeckCard}
 *         </li>
 *     </ul>
 * </p>
 * {@code null} values are considered valid.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface PermittedMtgSet
{
    String message() default "{format.set.banned}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The maximum date value that the sets should be released before
     *
     * @return string representation in ISO8601 YYYY-MM-DD format
     */
    String before() default "9999-12-31";

    /**
     * The minimum date value that sets should be released after
     *
     * @return string representation in ISO8601 YYYY-MM-DD format
     */
    String after() default "1990-01-01";

    /**
     * The sets that should not appear as card's single printing
     *
     * @return string array containing short names of sets that
     * are not permitted in that format
     */
    String[] banned() default {};
}
