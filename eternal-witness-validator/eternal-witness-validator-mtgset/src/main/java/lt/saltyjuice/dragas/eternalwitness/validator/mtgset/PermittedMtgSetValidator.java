package lt.saltyjuice.dragas.eternalwitness.validator.mtgset;

import lt.saltyjuice.dragas.eternalwitness.api.exception.EternalWitnessExcepion;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCardSet;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgSet;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PermittedMtgSetValidator implements ConstraintValidator<PermittedMtgSet, Collection<MtgDeckCard>>
{
    private LocalDate before;
    private LocalDate after;
    private Set<String> banned;

    @Override
    public void initialize(PermittedMtgSet constraintAnnotation)
    {
        this.before = LocalDate.parse(constraintAnnotation.before());
        this.after = LocalDate.parse(constraintAnnotation.after());
        this.banned = new TreeSet<>(Arrays.asList(constraintAnnotation.banned()));
        this.banned = Collections.unmodifiableSet(banned);
        assertParameters();
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        if (value == null)
        {
            return true;
        }
        final List<MtgCard> filteredDecklist = value
                .stream()
                .map(MtgDeckCard::getCard)
                .filter(this::cardsWithAfterPrinting)
                .filter(this::cardsWithBeforePrinting)
                .filter(this::cardsWithUnbannedSetPrintings)
                .collect(Collectors.toList());
        return value.size() == filteredDecklist.size();
    }

    private boolean cardsWithUnbannedSetPrintings(MtgCard mtgCard)
    {
        return mtgCard.getSets().stream()
                .map(MtgCardSet::getSet)
                .map(MtgSet::getShortName)
                .anyMatch(this::isNotInBannedList);
    }

    private boolean isNotInBannedList(String s)
    {
        return !banned.contains(s);
    }

    private boolean cardsWithAfterPrinting(MtgCard card)
    {
        return cardsWithPrintingPredicate(card, this::isDateAfter);
    }

    private boolean cardsWithBeforePrinting(MtgCard card) {
        return cardsWithPrintingPredicate(card, this::isDateBefore);
    }

    private boolean cardsWithPrintingPredicate(MtgCard card, Predicate<LocalDate> isDateBefore)
    {
        return card
                .getSets()
                .stream()
                .map(MtgCardSet::getSet)
                .map(MtgSet::getRelease)
                .anyMatch(isDateBefore);
    }

    private boolean isDateAfter(LocalDate date)
    {
        return date.isAfter(after);
    }

    private boolean isDateBefore(LocalDate date) {
        return date.isBefore(before);
    }

    private void assertParameters()
    {
        if (this.after.isAfter(this.before))
        {
            throw new EternalWitnessExcepion(String.format(
                    "Expected minimum date value (%s) to be less than maximum date value (%s)",
                    after,
                    before
            ));
        }
    }
}
