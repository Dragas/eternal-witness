package lt.saltyjuice.dragas.eternalwitness.validator.mtgset;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PermittedMtgSetModule implements ValidatorModule
{
    public static final String FORMAT_SET_BEFORE = "format.set.before";
    public static final String FORMAT_SET_AFTER = "format.set.after";
    public static final String FORMAT_SET_BANNED = "format.set.banned";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Arrays.asList(
                FORMAT_SET_BEFORE,
                FORMAT_SET_AFTER,
                FORMAT_SET_BANNED
        );
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        PermittedMtgSetDef def = new PermittedMtgSetDef();
        String[] banned = formatRulesList
                .stream()
                .filter(this::isBannedRule)
                .map(FormatRule::getValue)
                .toArray(String[]::new);
        formatRulesList
                .stream()
                .filter(this::isBeforeRule)
                .findFirst()
                .map(FormatRule::getValue)
                .ifPresent(def::before);
        formatRulesList
                .stream()
                .filter(this::isAfterRule)
                .findFirst()
                .map(FormatRule::getValue)
                .ifPresent(def::after);
        return def.banned(banned);
    }

    private boolean isAfterRule(FormatRule formatRule)
    {
        return isRule(formatRule, FORMAT_SET_AFTER);
    }

    private boolean isBeforeRule(FormatRule formatRule)
    {
        return isRule(formatRule, FORMAT_SET_BEFORE);
    }

    private boolean isBannedRule(FormatRule formatRule)
    {
        return isRule(formatRule, FORMAT_SET_BANNED);
    }
}
