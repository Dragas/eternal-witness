# EW-Validator-MtgSet

Rule to validate whether card has valid printings for that format. 

## Specification

This module implements the following rules:
* `format.set.before`
* `format.set.after`  
* `format.set.banned` (repeatable)

To configure the rule, use `PermittedMtgSetModule`

Implementation note: module takes only the first
occurrence of `format.set.before` and `format.set.after` 
rules. Meanwhile `format.set.banned` can be repeated multiple times.
The repeated values are merged into an array of strings
and assigned as `banned` field on the annotation.
It is up to user to resolve the multiple language issue.

## Annotation Definition

Annotation for `PermittedMtgSet` is defined as the following (w.o. required JBV arguments)

```java
@PermittedMtgSet(
    String before() default "9999-12-31",
    String after() default "1990-01-01",
    String[] banned() default {};
)
```

Where:
 * `before` controls the maximum possible value of `MtgSet#getRelease()` as ISO8601 date string
 * `after` controls the minimum possible value of `MtgSet#getRelease()` as ISO8601 date string
 * `banned` controls a string array of `MtgSet#getShortName()` that not be permitted as valid printings
 
 
The annotation is not repeatable and should only be applied to
`Collection<MtgDeckCard>` fields, arguments, methods.

