import lt.saltyjuice.dragas.eternalwitness.validator.rarity.RarityModule;
import lt.saltyjuice.dragas.eternalwitness.validator.rarity.RarityValidator;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.rarity {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.rarity;
    provides ConstraintValidator with RarityValidator;
    provides ValidatorModule with RarityModule;
    uses ValidatorModule;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
