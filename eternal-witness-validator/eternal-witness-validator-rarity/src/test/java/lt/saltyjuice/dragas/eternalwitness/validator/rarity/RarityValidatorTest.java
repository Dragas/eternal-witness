package lt.saltyjuice.dragas.eternalwitness.validator.rarity;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModuleFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RarityValidatorTest
{
    private static final String BANNED_CARD = "Swamp";
    private static final String REGULAR_CARD = "Forest";
    private static final String MODERN = "MODERN";
    private static final AtomicLong COUNTER = new AtomicLong();
    private static Format format;
    private static Decklist PROPER_DECKLIST;
    private static Decklist BANNED_DECKLIST;
    private static Decklist MIXED_DECKLIST;
    private Validator validator;
    private ValidatorModuleFactory factory;

    @BeforeAll
    public static void setUpSuite()
    {
        format = createFormat(
                MODERN,
                createFormatRule(
                        RarityModule.FORMAT_RARITY_BANNED,
                        BANNED_CARD
                )
        );
        PROPER_DECKLIST = createDecklist(REGULAR_CARD);
        BANNED_DECKLIST = createDecklist(BANNED_CARD);
        MIXED_DECKLIST = createDecklist(
                REGULAR_CARD,
                BANNED_CARD
        );
    }

    private static Decklist createDecklist(String... cardNames)
    {
        Decklist decklist = new Decklist();
        for (String name : cardNames)
        {
            MtgDeckCard card = createDeckCard(name);
            decklist
                    .getCards()
                    .add(card);
        }
        return decklist;
    }

    private static MtgDeckCard createDeckCard(String name)
    {
        MtgDeckCard deckCard = new MtgDeckCard();
        MtgCard card = createCard(name);
        deckCard.setCard(card);
        deckCard.setId(COUNTER.getAndIncrement());
        return deckCard;
    }

    private static MtgCard createCard(String name)
    {
        MtgCard card = new MtgCard();
        card.setName(name + name);
        card
                .getTypes()
                .add(createCardType(name));
        card
                .getSets()
                .add(createCardSet(name));
        return card;
    }

    private static MtgCardSet createCardSet(String name)
    {
        MtgCardSet cardSet = new MtgCardSet();
        cardSet.setRarity(name);
        cardSet.setId(COUNTER.getAndIncrement());
        return cardSet;
    }

    private static MtgCardType createCardType(String name)
    {
        MtgCardType mtgCardType = new MtgCardType();
        mtgCardType.setType(createType(name));
        return mtgCardType;
    }

    private static MtgType createType(String name)
    {
        MtgType type = new MtgType();
        type.setName(name);
        return type;
    }

    private static Format createFormat(String name, FormatRule... formatRules)
    {
        Format format = new Format();
        format.setName(name);
        format
                .getRules()
                .addAll(Arrays.asList(formatRules));
        return format;
    }

    private static FormatRule createFormatRule(String key, String value)
    {
        Rule rule = createRule(key);
        FormatRule formatRule = new FormatRule();
        formatRule.setRule(rule);
        formatRule.setValue(value);
        return formatRule;
    }

    private static Rule createRule(String key)
    {
        Rule rule = new Rule();
        rule.setKey(key);
        return rule;
    }

    @BeforeEach
    public void setUp()
    {
        factory = new ValidatorModuleFactory();
        ServiceLoader<ValidatorModule> loader = ServiceLoader.load(ValidatorModule.class);
        for (ValidatorModule module : loader)
        {
            factory.install(module);
        }
        validator = factory.getValidator(format);
    }

    @AfterEach
    public void tearDown() throws Exception
    {
        validator = null;
        factory.close();
    }

    @Test
    public void validator_forBannedRarityDeck_fails()
    {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(BANNED_DECKLIST);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void validator_forMixedRarityDeck_fails()
    {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(MIXED_DECKLIST);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void validator_forProperRarityDeck_succeeds()
    {
        Set<ConstraintViolation<Decklist>> violations = validator.validate(PROPER_DECKLIST);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void validator_forMultiplePrintings_succeeds()
    {
        Decklist decklist = createDecklist(BANNED_CARD);
        Collection<MtgCardSet> cardSet = decklist
                .getCards()
                .stream()
                .findFirst()
                .get()
                .getCard()
                .getSets();
        cardSet.add(createCardSet(REGULAR_CARD));
        Set<ConstraintViolation<Decklist>> violations = validator.validate(PROPER_DECKLIST);
        assertTrue(violations.isEmpty());
    }
}
