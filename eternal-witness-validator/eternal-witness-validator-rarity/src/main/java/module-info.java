import lt.saltyjuice.dragas.eternalwitness.validator.rarity.RarityModule;
import lt.saltyjuice.dragas.eternalwitness.validator.rarity.RarityValidator;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.rarity {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.rarity;
    opens lt.saltyjuice.dragas.eternalwitness.validator.rarity to org.hibernate.validator;
    provides ConstraintValidator with RarityValidator;
    provides ValidatorModule with RarityModule;
}
