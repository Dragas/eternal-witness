package lt.saltyjuice.dragas.eternalwitness.validator.rarity;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Restricts the appearance of cards in a deck.
 * The validation happens by rarity name in an exclusive fashion.
 * <br>
 * This annotation must be put on collection of {@link MtgDeckCard} elements.
 *
 * <p>
 * Supported types are:
 *     <ul>
 *         <li>
 *             {@link MtgDeckCard}
 *         </li>
 *     </ul>
 * </p>
 * <p>
 * {@code null} elements are considered valid.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface Rarity
{
    String message() default "{format.rarity.banned}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value() default {};
}
