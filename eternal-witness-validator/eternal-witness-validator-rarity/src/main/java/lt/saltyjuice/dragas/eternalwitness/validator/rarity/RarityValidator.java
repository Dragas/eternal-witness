package lt.saltyjuice.dragas.eternalwitness.validator.rarity;

import lt.saltyjuice.dragas.eternalwitness.api.model.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

public class RarityValidator implements ConstraintValidator<Rarity, Collection<MtgDeckCard>>
{
    private Collection<String> bannedRarities;

    @Override
    public void initialize(Rarity constraintAnnotation)
    {
        this.bannedRarities = new TreeSet<>(Arrays.asList(constraintAnnotation.value()));
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        return value.stream()
                .map(MtgDeckCard::getCard)
                .allMatch(this::hasPrintingWithoutBannedRarities);
    }

    private boolean hasPrintingWithoutBannedRarities(MtgCard mtgCard)
    {
        return mtgCard
                .getSets()
                .stream()
                .map(MtgCardSet::getRarity)
                .anyMatch(this::shouldNotExclude);
    }

    private boolean shouldNotExclude(String s)
    {
        return !bannedRarities.contains(s);
    }
}
