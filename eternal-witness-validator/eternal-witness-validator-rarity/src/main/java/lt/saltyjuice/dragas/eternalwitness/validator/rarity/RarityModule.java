package lt.saltyjuice.dragas.eternalwitness.validator.rarity;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RarityModule implements ValidatorModule
{
    public static final String FORMAT_RARITY_BANNED = "format.rarity.banned";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Collections.singletonList(FORMAT_RARITY_BANNED);
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        RarityConstraintDef def = new RarityConstraintDef();
        String[] values = formatRulesList.stream().map(FormatRule::getValue).toArray(String[]::new);
        def.value(values);
        return def;
    }
}
