package lt.saltyjuice.dragas.eternalwitness.validator.rarity;

import org.hibernate.validator.cfg.ConstraintDef;

public class RarityConstraintDef extends ConstraintDef<RarityConstraintDef, Rarity>
{
    public RarityConstraintDef()
    {
        super(Rarity.class);
    }

    public RarityConstraintDef value(String... values) {
        addParameter("value", values);
        return this;
    }
}
