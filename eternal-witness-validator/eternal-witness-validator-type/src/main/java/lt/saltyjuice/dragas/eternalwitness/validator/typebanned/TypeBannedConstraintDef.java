package lt.saltyjuice.dragas.eternalwitness.validator.typebanned;

import org.hibernate.validator.cfg.ConstraintDef;

public class TypeBannedConstraintDef extends ConstraintDef<TypeBannedConstraintDef, TypeBanned>
{
    public TypeBannedConstraintDef()
    {
        super(TypeBanned.class);
    }

    public TypeBannedConstraintDef value(String... values) {
        addParameter("value", values);
        return this;
    }
}
