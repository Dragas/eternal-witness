package lt.saltyjuice.dragas.eternalwitness.validator.typebanned;

import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgCardType;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgDeckCard;
import lt.saltyjuice.dragas.eternalwitness.api.model.MtgType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

public class TypeBannedValidator implements ConstraintValidator<TypeBanned, Collection<MtgDeckCard>>
{
    private Collection<String> bannedTypes;

    @Override
    public void initialize(TypeBanned constraintAnnotation)
    {
        this.bannedTypes = new TreeSet<>(Arrays.asList(constraintAnnotation.value()));
    }

    @Override
    public boolean isValid(Collection<MtgDeckCard> value, ConstraintValidatorContext context)
    {
        return value.stream()
                .map(MtgDeckCard::getCard)
                .map(MtgCard::getTypes)
                .flatMap(Collection::stream)
                .map(MtgCardType::getType)
                .map(MtgType::getName)
                .noneMatch(bannedTypes::contains);
    }
}
