package lt.saltyjuice.dragas.eternalwitness.validator.typebanned;

import lt.saltyjuice.dragas.eternalwitness.api.model.FormatRule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import org.hibernate.validator.cfg.ConstraintDef;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TypeBannedModule implements ValidatorModule
{
    public static final String FORMAT_TYPE_BANNED = "format.type.banned";

    @Override
    public Collection<String> getSupportedKeys()
    {
        return Collections.singletonList(FORMAT_TYPE_BANNED);
    }

    @Override
    public ConstraintDef getConstraintImpl(List<FormatRule> formatRulesList)
    {
        TypeBannedConstraintDef def = new TypeBannedConstraintDef();
        String[] values = formatRulesList.stream().map(FormatRule::getValue).toArray(String[]::new);
        def.value(values);
        return def;
    }
}
