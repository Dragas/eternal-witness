import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.typebanned.TypeBannedModule;
import lt.saltyjuice.dragas.eternalwitness.validator.typebanned.TypeBannedValidator;

import javax.validation.ConstraintValidator;

module eternal.witness.validator.typebanned {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.typebanned;
    opens lt.saltyjuice.dragas.eternalwitness.validator.typebanned to org.hibernate.validator;
    provides ValidatorModule with TypeBannedModule;
    provides ConstraintValidator with TypeBannedValidator;
}
