import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.typebanned.TypeBannedModule;
import lt.saltyjuice.dragas.eternalwitness.validator.typebanned.TypeBannedValidator;

import javax.validation.ConstraintValidator;

open module eternal.witness.validator.typebanned {
    requires transitive eternal.witness.validator.api;
    exports lt.saltyjuice.dragas.eternalwitness.validator.typebanned;
    uses ValidatorModule;
    provides ValidatorModule with TypeBannedModule;
    provides ConstraintValidator with TypeBannedValidator;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
}
