create table discord.guilds
(
	id serial not null
		constraint guilds_pk
			primary key,
	guild_id varchar(50) not null
		constraint guilds_pk_2
			unique,
	back_office_channel_id varchar(50) not null
		constraint guilds_pk_3
			unique,
	announcement_channel_id varchar(50) not null
		constraint guilds_pk_4
			unique
);

create table discord.discord_tournament_extension
(
	id serial not null
		constraint discord_tournament_extension_pk
			primary key,
	tournament_id integer not null
		constraint discord_tournament_extension_tournaments_id_fk
			references tournaments
				on update cascade on delete cascade,
	guild_snowflake varchar(50) not null
		constraint discord_tournament_extension_guilds_guild_id_fk
			references discord.guilds (guild_id)
				on update cascade on delete cascade,
	emoji_snowflake varchar(50) not null,
	role_snowflake varchar(50) not null,
	message varchar(2000) not null,
	message_snowflake varchar(50) not null
);

alter table discord.discord_tournament_extension owner to mtg_shit_tester;

create index discord_tournament_extension_guild_snowflake_index
	on discord.discord_tournament_extension (guild_snowflake);

create index discord_tournament_extension_tournament_id_index
	on discord.discord_tournament_extension (tournament_id);

create unique index discord_tournament_extension_tournament_id_uindex
	on discord.discord_tournament_extension (tournament_id);

create unique index discord_tournament_extension_message_snowflake_uindex
	on discord.discord_tournament_extension (message_snowflake);

create table discord.discord_players_queue
(
	id serial not null
		constraint discord_players_queue_pk
			primary key,
	player_snowflake varchar(50) not null,
	tournament_id integer not null
		constraint players_queue_tournament_extension_tournament_id_fk
			references discord.discord_tournament_extension
				on update cascade on delete cascade,
	constraint discord_players_queue_pk_2
		unique (player_snowflake, tournament_id)
);

create unique index discord_players_queue_id_uindex
	on discord.discord_players_queue (id);

create unique index guilds_guild_id_uindex
	on discord.guilds (guild_id);

create unique index guilds_id_uindex
	on discord.guilds (id);

create unique index guilds_back_office_channel_id_uindex
	on discord.guilds (back_office_channel_id);

create unique index guilds_announcement_channel_id_uindex
	on discord.guilds (announcement_channel_id);

