import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.persistence.guild.HibernateDiscordGuildServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.discord.persistence.player.HibernateDiscordPlayerQueueServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.discord.persistence.tournament.HibernateDiscordTournamentServiceProvider;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

module eternal.witness.discord.persistence {
    requires transitive eternal.witness.discord.core;
    requires transitive eternal.witness.persistence;
    uses SessionFactoryService;
    uses DiscordGuildService;
    uses DiscordTournamentService;
    uses DiscordPlayerQueueService;
    uses TournamentService;
    provides DiscordGuildService with HibernateDiscordGuildServiceProvider;
    provides DiscordTournamentService with HibernateDiscordTournamentServiceProvider;
    provides DiscordPlayerQueueService with HibernateDiscordPlayerQueueServiceProvider;
}
