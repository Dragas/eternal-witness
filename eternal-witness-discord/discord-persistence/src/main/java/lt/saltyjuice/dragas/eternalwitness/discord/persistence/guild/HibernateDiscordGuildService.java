package lt.saltyjuice.dragas.eternalwitness.discord.persistence.guild;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

public class HibernateDiscordGuildService extends AbstractHibernateService implements DiscordGuildService
{
    public HibernateDiscordGuildService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public DiscordGuild createDiscordGuild(String guildId, String backOfficeChannelId, String announcementChannelId)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                DiscordGuild discordGuild = new DiscordGuild();
                discordGuild.setBackOfficeChannelId(backOfficeChannelId);
                discordGuild.setGuildId(guildId);
                discordGuild.setAnnouncementChannelId(announcementChannelId);
                s.save(discordGuild);
                return discordGuild;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(String.format(
                    "Guild with either guild id %s or channel id %s already exists",
                    guildId,
                    backOfficeChannelId
            ));
        }
    }

    @Override
    public DiscordGuild fetchById(long id)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordGuild> query = s.createQuery(
                    "select dg from DiscordGuild dg where id = ?1",
                    DiscordGuild.class
            );
            query.setParameter(
                    1,
                    id
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format(
                            "Guild with id %s does not exist",
                            id
                    )
            );
        });
    }

    @Override
    public DiscordGuild fetchByGuildId(String guildId)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordGuild> query = s.createQuery(
                    "select dg from DiscordGuild dg where guildId = ?1",
                    DiscordGuild.class
            );
            query.setParameter(
                    1,
                    guildId
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format(
                            "Guild with guild id %s does not exist",
                            guildId
                    )
            );
        });
    }

    @Override
    public DiscordGuild fetchByBackOfficeChannelId(String channelId)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordGuild> query = s.createQuery(
                    "select dg from DiscordGuild dg where backOfficeChannelId = ?1",
                    DiscordGuild.class
            );
            query.setParameter(
                    1,
                    channelId
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format(
                            "Guild with back channel id %s does not exist",
                            channelId
                    )
            );
        });
    }

    @Override
    public DiscordGuild fetchByAnnouncementChannelId(String channelId)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordGuild> query = s.createQuery(
                    "select dg from DiscordGuild dg where announcementChannelId = ?1",
                    DiscordGuild.class
            );
            query.setParameter(
                    1,
                    channelId
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format(
                            "Guild with channel id %s does not exist",
                            channelId
                    )
            );
        });
    }

    @Override
    public void removeGuild(DiscordGuild guild)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(guild);
        });
    }
}
