package lt.saltyjuice.dragas.eternalwitness.discord.persistence.player;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueItem;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

public class HibernateDiscordPlayerQueueService extends AbstractHibernateService implements DiscordPlayerQueueService
{
    public HibernateDiscordPlayerQueueService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public DiscordPlayerQueueItem addToQueue(DiscordTournament tournament, String playerSnowflake)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                DiscordPlayerQueueItem discordPlayerQueueItem = new DiscordPlayerQueueItem();
                discordPlayerQueueItem.setPlayerSnowflake(playerSnowflake);
                discordPlayerQueueItem.setTournament(tournament);
                s.save(discordPlayerQueueItem);
                return discordPlayerQueueItem;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Player %s is already part of queue for tournament %s",
                            playerSnowflake,
                            tournament.getId()
                    ),
                    e
            );
        }
    }

    @Override
    public DiscordPlayerQueueItem fetchFromQueue(String guildSnowflake, String tournamentName, String playerSnowflake)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordPlayerQueueItem> query = s.createQuery(
                    "select dpqi from DiscordPlayerQueueItem dpqi join dpqi.tournament dt join dt.tournament t join dt.discordGuild dg where dpqi.playerSnowflake = ?1 and t.name = ?2 and dg.guildId = ?3",
                    DiscordPlayerQueueItem.class
            );
            query.setParameter(
                    1,
                    playerSnowflake
            );
            query.setParameter(
                    2,
                    tournamentName
            );
            query.setParameter(
                    3,
                    guildSnowflake
            );
            return getFirstResult(
                    query.getResultList(),
                    String.format("Failed to fetch player %s from queue %s for guild %s",
                            playerSnowflake,
                            tournamentName,
                            guildSnowflake
                    )
            );
        });
    }

    @Override
    public void removePlayerQueueItem(DiscordPlayerQueueItem discordPlayerQueueItem)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(discordPlayerQueueItem);
        });
    }
}
