package lt.saltyjuice.dragas.eternalwitness.discord.persistence.tournament;

import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateDiscordTournamentServiceProvider
{
    public static DiscordTournamentService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateDiscordTournamentService(service.getSessionFactory());
    }
}
