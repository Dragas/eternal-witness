package lt.saltyjuice.dragas.eternalwitness.discord.persistence.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.AbstractHibernateService;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

public class HibernateDiscordTournamentService extends AbstractHibernateService implements DiscordTournamentService
{
    public HibernateDiscordTournamentService(SessionFactory sessionFactory)
    {
        super(sessionFactory);
    }

    @Override
    public DiscordTournament createDiscordTournament(Tournament tournament, DiscordGuild guild, String emoji, String role, String message, String messageSnowflake)
    {
        try
        {
            return performReadingRunnable((s) ->
            {
                DiscordTournament discordTournament = new DiscordTournament();
                discordTournament.setTournament(tournament);
                discordTournament.setDiscordGuild(guild);
                discordTournament.setEmojiSnowflake(emoji);
                discordTournament.setRoleSnowflake(role);
                discordTournament.setMessage(message);
                discordTournament.setMessageSnowflake(messageSnowflake);
                s.save(discordTournament);
                return discordTournament;
            });
        }
        catch (ConstraintViolationException e)
        {
            throw new DuplicateEntityException(
                    String.format(
                            "Tournament %s already has discord extension",
                            tournament.getName()
                    ),
                    e
            );
        }
    }

    @Override
    public DiscordTournament findByMessageSnowflake(String snowflake)
    {
        return performReadingRunnable((s) ->
        {
            Query<DiscordTournament> tournamentQuery = s.createQuery(
                    "select dt from DiscordTournament dt where dt.messageSnowflake = ?1",
                    DiscordTournament.class
            );
            tournamentQuery.setParameter(
                    1,
                    snowflake
            );
            return getFirstResult(
                    tournamentQuery.getResultList(),
                    String.format(
                            "Failed to find tournament for message %s",
                            snowflake
                    )
            );
        });
    }

    @Override
    public void removeDiscordTournament(DiscordTournament tournament)
    {
        performSimpleRunnable((s) ->
        {
            s.delete(tournament);
        });
    }
}
