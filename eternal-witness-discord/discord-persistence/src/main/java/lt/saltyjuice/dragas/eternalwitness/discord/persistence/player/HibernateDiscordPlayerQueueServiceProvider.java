package lt.saltyjuice.dragas.eternalwitness.discord.persistence.player;

import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateDiscordPlayerQueueServiceProvider
{
    public static DiscordPlayerQueueService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateDiscordPlayerQueueService(service.getSessionFactory());
    }
}
