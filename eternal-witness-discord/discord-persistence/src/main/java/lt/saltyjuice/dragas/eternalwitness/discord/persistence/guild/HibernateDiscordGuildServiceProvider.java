package lt.saltyjuice.dragas.eternalwitness.discord.persistence.guild;

import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;

import java.util.ServiceLoader;

public class HibernateDiscordGuildServiceProvider
{
    public static DiscordGuildService provider()
    {
        ServiceLoader<SessionFactoryService> load = ServiceLoader.load(SessionFactoryService.class);
        SessionFactoryService service = load
                .findFirst()
                .get();
        return new HibernateDiscordGuildService(service.getSessionFactory());
    }
}
