package lt.saltyjuice.dragas.eternalwitness.discord.persistence;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class HibernateDiscordTournamentServiceTest
{
    private static final String GUILD_ID = "1234";
    private static final String EMOJI_ID = "56789";
    private static final String ROLE_ID = "12346";
    private static final String MESSAGE_ID = "1235454";
    private static final String MESSAGE = "FUCK";
    private static final String TITLE = "SHIT";
    private static TournamentService tournamentService;
    private static DiscordTournamentService discordTournamentService;
    private static DiscordGuildService discordGuildService;
    private static SessionFactory sessionFactory;
    private static Tournament tournament;
    private static DiscordGuild guild;

    @BeforeAll
    public static void setUpSuite()
    {
        discordTournamentService = ServiceLoader
                .load(DiscordTournamentService.class)
                .findFirst()
                .get();
        discordGuildService = ServiceLoader
                .load(DiscordGuildService.class)
                .findFirst()
                .get();
        sessionFactory = ServiceLoader
                .load(SessionFactoryService.class)
                .findFirst()
                .get()
                .getSessionFactory();
        tournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
        DatabaseTestUtility.getSession(sessionFactory).beginTransaction();
        guild = discordGuildService.createDiscordGuild(
                GUILD_ID,
                GUILD_ID,
                GUILD_ID
        );
        tournament = tournamentService.createTournament(
                TITLE,
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        sessionFactory.getCurrentSession().getTransaction().commit();
    }

    @AfterAll
    public static void tearDownSuite()
    {
        sessionFactory.getCurrentSession().beginTransaction();
        sessionFactory.getCurrentSession().update(tournament);
        sessionFactory.getCurrentSession().update(guild);
        tournamentService.removeTournament(tournament);
        discordGuildService.removeGuild(guild);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().getTransaction().commit();
        DatabaseTestUtility.removeSession(sessionFactory.getCurrentSession());
    }

    @BeforeEach
    public void setUp() {
        sessionFactory.getCurrentSession().beginTransaction();
    }

    @Test
    public void serviceCreatesTournamentExtension()
    {
        DiscordTournament discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                EMOJI_ID,
                ROLE_ID,
                MESSAGE,
                MESSAGE_ID
        );
        assertNotEquals(
                0,
                discordTournament.getId(),
                "Should get id assigned"
        );
    }

    @Test
    public void serviceThrowsWhenTryingToCreateExtensionForSameTournament()
    {
        DiscordTournament discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                EMOJI_ID,
                ROLE_ID,
                MESSAGE,
                MESSAGE_ID
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> discordTournamentService.createDiscordTournament(
                        tournament,
                        guild,
                        EMOJI_ID,
                        ROLE_ID,
                        MESSAGE,
                        MESSAGE_ID
                ),
                "Should throw when try to create same extension"
        );
    }

    @Test
    public void serviceGetsTournamentBySnowflake()
    {
        DiscordTournament discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                EMOJI_ID,
                ROLE_ID,
                MESSAGE,
                MESSAGE_ID
        );
        DiscordTournament found = discordTournamentService.findByMessageSnowflake(MESSAGE_ID);
        assertEquals(
                discordTournament.getId(),
                found.getId(),
                "Ids should match"
        );
    }

    @Test
    public void serviceRemovesTournament()
    {
        DiscordTournament discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                EMOJI_ID,
                ROLE_ID,
                MESSAGE,
                MESSAGE_ID
        );
        discordTournamentService.removeDiscordTournament(discordTournament);
        sessionFactory.getCurrentSession().flush();
        assertThrows(
                NoSuchEntityException.class,
                () -> discordTournamentService.findByMessageSnowflake(MESSAGE_ID),
                "Should throw when trying to find non existing tournament"
        );
    }

    @AfterEach
    public void tearDown()
    {
        Session s = sessionFactory.getCurrentSession();
        s.getTransaction();
        Query q = s.createQuery("Delete DiscordTournament");
        q.executeUpdate();
        s
                .getTransaction()
                .commit();
    }
}
