package lt.saltyjuice.dragas.eternalwitness.discord.persistence;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class HibernateDiscordGuildServiceTest
{
    private static final String BACK_OFFICE_ID = "BACK_OFFICE_ID";
    private static final String GUILD_ID = "GUILD_ID";
    private static DiscordGuildService guildService;
    private static SessionFactory sessionFactory;

    @BeforeAll
    public static void beforeSuite()
    {
        guildService = ServiceLoader
                .load(DiscordGuildService.class)
                .findFirst()
                .get();
        sessionFactory = ServiceLoader
                .load(SessionFactoryService.class)
                .findFirst()
                .get()
                .getSessionFactory();
        DatabaseTestUtility.getSession(sessionFactory);
    }

    @AfterAll
    public static void tearDownAll()
    {
       DatabaseTestUtility.removeSession(sessionFactory.getCurrentSession());
    }

    @BeforeEach
    public void setUp() {
        sessionFactory.getCurrentSession().beginTransaction();
    }

    @Test
    public void guildServiceCreatesGuild()
    {
        DiscordGuild guild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                ""
        );
        assertNotEquals(
                0,
                guild.getId(),
                "guild should be saved"
        );
    }

    @Test
    public void guildServiceDeletesGuild()
    {
        DiscordGuild guild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                ""
        );
        guildService.removeGuild(guild);
        sessionFactory.getCurrentSession().flush();
        assertThrows(
                NoSuchEntityException.class,
                () -> guildService.fetchByGuildId(GUILD_ID),
                "Guild service should throw when there's no such guild"
        );
    }

    @Test
    public void guildServiceThrowsWhenFindingNonExistentGuildById()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> guildService.fetchById(-1),
                "Service should throw when there's no such guild"
        );
    }

    @Test
    public void guildServiceThrowsWhenFindingNonExistentGuildByGuildId()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> guildService.fetchByGuildId(GUILD_ID),
                "Service should throw when there's no such guild"
        );
    }

    @Test
    public void guildServiceThrowsWhenFindingNonExistentGuildByChannelId()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> guildService.fetchByBackOfficeChannelId(BACK_OFFICE_ID),
                "Service should throw when there's no such guild"
        );
    }

    @Test
    public void guildServiceThrowsWhenTryingToCreateDuplicateGuild()
    {
        guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                ""
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> guildService.createDiscordGuild(
                        GUILD_ID,
                        GUILD_ID,
                        ""
                ),
                "Should throw when trying to create same guild"
        );
    }

    @Test
    public void guildServiceThrowsWhenTryingToCreateDuplicateChannel()
    {
        guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                ""
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> guildService.createDiscordGuild(
                        BACK_OFFICE_ID,
                        BACK_OFFICE_ID,
                        ""
                ),
                "Should throw when trying to create same guild"
        );
    }

    @Test
    public void guildServiceFetchesGuildById()
    {
        DiscordGuild discordGuild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                BACK_OFFICE_ID
        );
        DiscordGuild fetched = guildService.fetchById(discordGuild.getId());
        assertEquals(
                discordGuild.getId(),
                fetched.getId()
        );
    }

    @Test
    public void guildServiceFetchesGuildByBackOfficeId()
    {
        DiscordGuild discordGuild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                BACK_OFFICE_ID
        );
        DiscordGuild fetched = guildService.fetchByBackOfficeChannelId(BACK_OFFICE_ID);
        assertEquals(
                discordGuild.getId(),
                fetched.getId()
        );
    }

    @Test
    public void guildServiceFetchesGuildByAnnouncementChannelId()
    {
        DiscordGuild discordGuild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                BACK_OFFICE_ID
        );
        DiscordGuild fetched = guildService.fetchByAnnouncementChannelId(BACK_OFFICE_ID);
        assertEquals(
                discordGuild.getId(),
                fetched.getId()
        );
    }

    @Test
    public void guildServiceFetchesGuildByGuildId()
    {
        DiscordGuild discordGuild = guildService.createDiscordGuild(
                GUILD_ID,
                BACK_OFFICE_ID,
                BACK_OFFICE_ID
        );
        DiscordGuild fetched = guildService.fetchByGuildId(GUILD_ID);
        assertEquals(
                discordGuild.getId(),
                fetched.getId()
        );
    }

    @AfterEach
    public void tearDown()
    {
        Session s = sessionFactory.getCurrentSession();
        s.getTransaction();
        Query q = s.createQuery("Delete DiscordGuild");
        q.executeUpdate();
        s
                .getTransaction()
                .commit();
    }
}
