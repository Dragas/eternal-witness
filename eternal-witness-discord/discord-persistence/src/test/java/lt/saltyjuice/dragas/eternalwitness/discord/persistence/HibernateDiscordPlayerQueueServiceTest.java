package lt.saltyjuice.dragas.eternalwitness.discord.persistence;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueItem;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class HibernateDiscordPlayerQueueServiceTest
{
    private static final String SNOWFLAKE = "snowflake";
    private static final String GENERIC_ID = "1";
    private static DiscordPlayerQueueService discordPlayerQueueService;
    private static DiscordTournamentService discordTournamentService;
    private static DiscordGuildService discordGuildService;
    private static TournamentService tournamentService;
    private static DiscordGuild guild;
    private static Tournament tournament;
    private static SessionFactory sessionFactory;
    private static DiscordTournament discordTournament;

    @BeforeAll
    public static void setUpSuite()
    {
        discordPlayerQueueService = ServiceLoader
                .load(DiscordPlayerQueueService.class)
                .findFirst()
                .get();
        discordTournamentService = ServiceLoader
                .load(DiscordTournamentService.class)
                .findFirst()
                .get();
        discordGuildService = ServiceLoader
                .load(DiscordGuildService.class)
                .findFirst()
                .get();
        sessionFactory = ServiceLoader
                .load(SessionFactoryService.class)
                .findFirst()
                .get()
                .getSessionFactory();
        tournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
        Session session = DatabaseTestUtility.getSession(sessionFactory);
        session.beginTransaction();
        guild = discordGuildService.createDiscordGuild(
                GENERIC_ID,
                GENERIC_ID,
                GENERIC_ID
        );
        tournament = tournamentService.createTournament(
                GENERIC_ID,
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                GENERIC_ID,
                GENERIC_ID,
                GENERIC_ID,
                GENERIC_ID
        );
        session.getTransaction().commit();
    }

    @BeforeEach
    public void setUp() {
        sessionFactory.getCurrentSession().beginTransaction();
    }

    @AfterAll
    public static void tearDownSuite()
    {
        sessionFactory.getCurrentSession().beginTransaction();
        sessionFactory.getCurrentSession().update(tournament);
        tournamentService.removeTournament(tournament);
        discordGuildService.removeGuild(guild);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().getTransaction().commit();
        DatabaseTestUtility.removeSession(sessionFactory.getCurrentSession());
    }

    @Test
    public void playerQueueServicePutsPlayerInQueue()
    {
        DiscordPlayerQueueItem item = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE
        );
        assertNotEquals(
                0,
                item.getId(),
                "Item should have an id assigned to it"
        );
    }

    @Test
    public void playerQueuePermitsAttachingMultiplePeople()
    {
        DiscordPlayerQueueItem first = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE
        );
        DiscordPlayerQueueItem second = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE + SNOWFLAKE
        );
        assertNotEquals(
                first.getId(),
                second.getId(),
                "Two players ids should not match"
        );
    }

    @Test
    public void playerQueueDoesNotPermitSamePersonTwice()
    {
        DiscordPlayerQueueItem first = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE
        );
        assertThrows(
                DuplicateEntityException.class,
                () -> discordPlayerQueueService.addToQueue(
                        discordTournament,
                        SNOWFLAKE
                ),
                "Same player should not get entered twice"
        );

    }

    @Test
    public void playerQueueFetchesItemFromQueue()
    {
        DiscordPlayerQueueItem item = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE
        );
        DiscordPlayerQueueItem fetched = discordPlayerQueueService.fetchFromQueue(
                guild.getGuildId(),
                GENERIC_ID,
                SNOWFLAKE
        );
        assertEquals(
                item.getId(),
                fetched.getId(),
                "Should fetch same item"
        );
    }

    @Test
    public void playerQueueFailsToFetchPlayerWhichHasNotEntered()
    {
        assertThrows(
                NoSuchEntityException.class,
                () -> discordPlayerQueueService.fetchFromQueue(
                        guild.getGuildId(),
                        GENERIC_ID,
                        SNOWFLAKE
                ),
                "Should throw when trying to fetch"
        );
    }

    @Test
    public void playerQueueRemovesPlayerFromQueue()
    {
        DiscordPlayerQueueItem item = discordPlayerQueueService.addToQueue(
                discordTournament,
                SNOWFLAKE
        );
        discordPlayerQueueService.removePlayerQueueItem(item);
        sessionFactory.getCurrentSession().flush();
        assertThrows(
                NoSuchEntityException.class,
                () -> discordPlayerQueueService.fetchFromQueue(
                        guild.getGuildId(),
                        GENERIC_ID,
                        SNOWFLAKE
                ),
                "Should throw when trying to fetch non existing item"
        );
    }

    @AfterEach
    public void tearDown()
    {
        Session s = sessionFactory.getCurrentSession();
        s.getTransaction();
        Query q = s.createQuery("Delete DiscordPlayerQueueItem");
        q.executeUpdate();
        s
                .getTransaction()
                .commit();
    }

}
