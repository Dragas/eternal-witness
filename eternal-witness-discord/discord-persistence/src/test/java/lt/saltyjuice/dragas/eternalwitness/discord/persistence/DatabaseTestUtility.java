package lt.saltyjuice.dragas.eternalwitness.discord.persistence;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

public class DatabaseTestUtility
{
    public static final Session getSession(SessionFactory factory) {
        Session s = factory.openSession();
        ManagedSessionContext.bind(s);
        s.setHibernateFlushMode(FlushMode.MANUAL);
        return s;
    }

    public static final void removeSession(Session s) {
        ManagedSessionContext.unbind(s.getSessionFactory());
        Transaction t = s.getTransaction();
        if(t.isActive()) {
            t.rollback();
        }
        s.close();
    }
}
