package lt.saltyjuice.dragas.eternalwitness.discord.reaction;

import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class TournamentGuildMessageReactionListener extends ListenerAdapter implements EternalWitnessDiscordListener
{
    private static final Logger LOG = LoggerFactory.getLogger(TournamentGuildMessageReactionListener.class);

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event)
    {
        MDC.put("discord.serverId", event
                .getGuild()
                .getId());
        LOG.info("Reaction received!");
        Guild guild = event.getGuild();
        Member member = event.getMember();
        Role role = guild.getRoleById(442421920265273355L);
        guild
                .addRoleToMember(member, role)
                .queue();
    }

    @Override
    public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event)
    {
        MDC.put("discord.serverId", event
                .getGuild()
                .getId());
        LOG.info("Reaction removed :(");
        Guild guild = event.getGuild();
        Member member = guild.getMemberById(event.getUserId());
        Role role = guild.getRoleById(442421920265273355L);
        guild
                .removeRoleFromMember(member, role)
                .queue();
    }
}
