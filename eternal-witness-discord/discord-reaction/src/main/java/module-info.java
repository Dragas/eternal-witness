import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.reaction.TournamentGuildMessageReactionListener;

module eternal.witness.discord.reaction {
    requires slf4j.api;
    requires eternal.witness.discord.core;
    provides EternalWitnessDiscordListener with TournamentGuildMessageReactionListener;
}
