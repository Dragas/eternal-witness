import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.reaction.tournament.TournamentReactionListener;

module eternal.witness.discord.tournament.reaction {
    requires eternal.witness.discord.core;
    requires slf4j.api;
    uses DiscordPlayerQueueService;
    uses DiscordTournamentService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
    provides EternalWitnessDiscordListener with TournamentReactionListener;
}
