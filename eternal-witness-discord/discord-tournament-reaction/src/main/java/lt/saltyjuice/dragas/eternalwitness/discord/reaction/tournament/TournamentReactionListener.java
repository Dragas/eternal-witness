package lt.saltyjuice.dragas.eternalwitness.discord.reaction.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.exception.DuplicateEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.Player;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueItem;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.guild.react.GenericGuildMessageReactionEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ServiceLoader;

public class TournamentReactionListener extends ListenerAdapter implements EternalWitnessDiscordListener
{
    private static final Logger LOG = LoggerFactory.getLogger(TournamentReactionListener.class);
    private final DiscordTournamentService discordTournamentService;
    private final DiscordPlayerQueueService playerQueueService;
    private final PlayerService playerService;
    private final TournamentService tournamentService;
    private final String template;

    public TournamentReactionListener() throws IOException
    {
        discordTournamentService = ServiceLoader
                .load(DiscordTournamentService.class)
                .findFirst()
                .get();
        playerQueueService = ServiceLoader
                .load(DiscordPlayerQueueService.class)
                .findFirst()
                .get();
        tournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
        playerService = ServiceLoader
                .load(PlayerService.class)
                .findFirst()
                .get();
        InputStream inputStream = getClass()
                .getResourceAsStream("/private-message.template");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        inputStream.transferTo(outputStream);
        inputStream.close();
        template = outputStream.toString(Charset.defaultCharset());
    }


    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event)
    {
        if (
                event
                        .getMember()
                        .getUser()
                        .isBot()
        )
        {
            // prevents self reactions
            return;
        }
        try
        {
            DiscordTournament tournament = discordTournamentService.findByMessageSnowflake(event.getMessageId());
            String emoji = eventToEmoji(event);
            if (
                    !tournament
                            .getEmojiSnowflake()
                            .equals(emoji)
            )
            {
                // short circuiting operation
                return;
            }
            final String tournamentName = tournament
                    .getTournament()
                    .getName();
            playerQueueService.addToQueue(
                    tournament,
                    event.getUserId()
            );
            event
                    .getUser()
                    .openPrivateChannel()
                    .flatMap((it) ->
                    {

                        String message = String
                                .format(
                                        template,
                                        tournamentName,
                                        event
                                                .getGuild()
                                                .getName(),
                                        event.getMessageId(),
                                        event
                                                .getGuild()
                                                .getId()
                                );
                        return it.sendMessage(message);
                    })
                    .queue();
            String backOfficeChannelId = tournament.getDiscordGuild().getBackOfficeChannelId();
            event
                    .getJDA()
                    .getTextChannelById(backOfficeChannelId)
                    .sendMessage(String.format("<@%s> has entered the queue for %s", event.getUser().getId(), tournamentName))
                    .queue();
        }
        catch (NoSuchEntityException e)
        {
            // I would like not having it muted because false positives
            // but then again discord sends fucking everything to you
            // while pretending that you subscribe to things.
        }
        catch (DuplicateEntityException e)
        {
            LOG.error(
                    "Race condition detected",
                    e
            );
        }
    }

    @Override
    public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event)
    {
        if (
                event
                        .getMember()
                        .getUser()
                        .isBot()
        )
        {
            // prevents self reactions
            return;
        }
        DiscordTournament tournament = null;
        try
        {
            final DiscordTournament scopedTournament = tournament = discordTournamentService.findByMessageSnowflake(event.getMessageId());
            String emoji = eventToEmoji(event);
            if (
                    !scopedTournament
                            .getEmojiSnowflake()
                            .equals(emoji)
            )
            {
                // short circuiting operation
                return;
            }
            final String tournamentName = scopedTournament
                    .getTournament()
                    .getName();
            DiscordPlayerQueueItem queueItem = playerQueueService
                    .fetchFromQueue(
                            event
                                    .getGuild()
                                    .getId(),
                            tournamentName,
                            event.getUserId()
                    );
            // ^ will throw when there's no such element
            playerQueueService.removePlayerQueueItem(queueItem);
            event
                    .getUser()
                    .openPrivateChannel()
                    .flatMap((it) -> it.sendMessage(String.format(
                            "You've left the queue for %s at %s",
                            tournamentName,
                            event
                                    .getGuild()
                                    .getName()
                    )))
                    .queue();
            String backOfficeChannelId = tournament.getDiscordGuild().getBackOfficeChannelId();
            event
                    .getJDA()
                    .getTextChannelById(backOfficeChannelId)
                    .sendMessage(String.format("<@%s> has left the queue for %s", event.getUser().getId(), tournamentName))
                    .queue();
        }
        catch (NoSuchEntityException e)
        {
            LOG.info("Player is leaving the tournament");
            Player player = playerService.fetchPlayerByName(event.getUser().getAsTag());
            String roleSnowflake = tournament.getRoleSnowflake();
            Role role = event.getGuild().getRoleById(roleSnowflake);
            event.getGuild().removeRoleFromMember(event.getMember(), role).queue();
            tournamentService.detachPlayer(tournament.getTournament(), player);
            String backOfficeChannelId = tournament.getDiscordGuild().getBackOfficeChannelId();
            event
                    .getJDA()
                    .getTextChannelById(backOfficeChannelId)
                    .sendMessage(String.format("<@%s> has dropped from %s", event.getUser().getId(), tournament.getTournament().getName()))
                    .queue();
        }
    }

    private String eventToEmoji(GenericGuildMessageReactionEvent event)
    {
        MessageReaction.ReactionEmote emote = event.getReactionEmote();
        if (emote.isEmote())
        {
            return emote
                    .getEmote()
                    .getAsMention();
        }
        return emote.getEmoji();
    }
}
