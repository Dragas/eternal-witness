module eternal.discord.hibernate.event.manager {
    exports lt.saltyjuice.dragas.eternalwitness.discord.eventmanager;
    uses lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
    requires net.dv8tion.jda;
    requires eternal.witness.persistence.common;
}
