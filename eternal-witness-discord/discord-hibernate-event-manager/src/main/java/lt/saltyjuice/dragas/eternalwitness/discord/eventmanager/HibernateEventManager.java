package lt.saltyjuice.dragas.eternalwitness.discord.eventmanager;

import lt.saltyjuice.dragas.eternalwitness.persistence.common.SessionFactoryService;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.hooks.InterfacedEventManager;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.util.ServiceLoader;

public class HibernateEventManager extends InterfacedEventManager
{
    private final SessionFactory sessionFactory;

    public HibernateEventManager()
    {
        SessionFactoryService sessionFactoryService = ServiceLoader
                .load(SessionFactoryService.class)
                .findFirst()
                .get();
        sessionFactory = sessionFactoryService.getSessionFactory();
    }

    @Override
    public void handle(GenericEvent event)
    {
        Session session = sessionFactory.openSession();
        session.setHibernateFlushMode(FlushMode.MANUAL);
        ManagedSessionContext.bind(session);
        session.beginTransaction();
        super.handle(event);
        session = ManagedSessionContext.unbind(sessionFactory);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }
}
