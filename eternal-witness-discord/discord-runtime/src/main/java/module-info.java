module eternal.witness.discord.runtime {
    uses lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
    requires eternal.witness.discord.core;
    requires eternal.discord.hibernate.event.manager;
}
