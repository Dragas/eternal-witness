package lt.saltyjuice.dragas.eternalwitness.discord.runtime;

import lt.saltyjuice.dragas.eternalwitness.discord.eventmanager.HibernateEventManager;
import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.ServiceLoader;

public class Main
{
    public static void main(String[] args) throws IOException, LoginException
    {
        Properties applicationProperties = new Properties();
        try (
                InputStream propertiesStream = Thread
                        .currentThread()
                        .getContextClassLoader()
                        .getResourceAsStream("application.properties")
        )
        {
            applicationProperties.load(propertiesStream);
        }
        JDABuilder builder = JDABuilder.create(
                GatewayIntent.DIRECT_MESSAGE_REACTIONS,
                GatewayIntent.DIRECT_MESSAGES,
                GatewayIntent.GUILD_MESSAGES,
                GatewayIntent.GUILD_MESSAGE_REACTIONS,
                GatewayIntent.GUILD_EMOJIS,
                GatewayIntent.GUILD_MEMBERS
        );
        builder.setToken(applicationProperties.getProperty("eternalwitness.token"));
        builder.setActivity(Activity.watching("Tournaments"));
        builder.disableCache(CacheFlag.ACTIVITY);
        builder.disableCache(CacheFlag.VOICE_STATE);
        builder.disableCache(CacheFlag.CLIENT_STATUS);
        builder.setEventManager(new HibernateEventManager());
        ServiceLoader<EternalWitnessDiscordListener> loader = ServiceLoader.load(EternalWitnessDiscordListener.class);
        for (EternalWitnessDiscordListener eternalWitnessDiscordListener : loader)
        {
            builder.addEventListeners(eternalWitnessDiscordListener);
        }
        builder.build();
    }
}
