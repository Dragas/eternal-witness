package lt.saltyjuice.dragas.eternalwitness.discord.message.tournament.host;

import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.MentioningCommandMessageListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ServiceLoader;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Deprecated
public class DiscordTournamentHostCommandListener extends MentioningCommandMessageListener implements EternalWitnessDiscordListener
{
    private static final Logger LOG = LoggerFactory.getLogger(DiscordTournamentHostCommandListener.class);
    private final DiscordGuildService guildService;
    private final DiscordTournamentService discordTournamentService;
    private final TournamentService regularTournamentService;

    public DiscordTournamentHostCommandListener()
    {
        guildService = ServiceLoader
                .load(DiscordGuildService.class)
                .findFirst()
                .get();
        discordTournamentService = ServiceLoader
                .load(DiscordTournamentService.class)
                .findFirst()
                .get();
        regularTournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
    }

    @Override
    protected String getCommand()
    {
        return "host";
    }

    @Override
    public void onGuildCommandReceived(GuildMessageReceivedEvent event, String fixedMessage)
    {
        event.getChannel().sendMessage("`host` command is deprecated, since it has no way of specifying which format should be used and scheduled for removal in 2.0.0-LEB").queue();
        DiscordGuild guild = guildService.fetchByGuildId(
                event
                        .getGuild()
                        .getId()
        );
        final TextChannel backOfficeChannel = event.getChannel();
        if (!guild
                .getBackOfficeChannelId()
                .equals(
                        backOfficeChannel
                                .getId()))
        {
            return;
        }

        List<Message.Attachment> attachments = event
                .getMessage()
                .getAttachments();
        String[] splitMessage = fixedMessage.split(" ");
        if (attachments.size() != 1 && (splitMessage.length != 2 && splitMessage.length != 3))
        {
            backOfficeChannel
                    .sendMessage("Usage: host [deadline:ISO8601 datetime string] [start:ISO8601 datetime string] [emoji] and uploaded file with up to 2k characters in it. \nTournament title is inferred from filename.")
                    .queue();
            backOfficeChannel
                    .sendMessage("Example: `host 2020-01-01T00:00 2020-01-01T01:00:00 :joy:`")
                    .addFile(
                            getClass().getResourceAsStream("/example.txt"),
                            "New Year Open.txt"
                    )
                    .queue();
            return;
        }
        String from = splitMessage[0];
        String to = from;
        String emoji = splitMessage[1];
        if (splitMessage.length == 3)
        {
            to = splitMessage[1];
            emoji = splitMessage[2];
        }
        Message.Attachment attachment = attachments.get(0);
        LocalDateTime fromDate = LocalDateTime.parse(from);
        LocalDateTime toDate = LocalDateTime.parse(to);
        if (fromDate.isAfter(toDate))
        {
            backOfficeChannel
                    .sendMessage("Warning: start date is after deadline. Posting anyways.")
                    .queue();
        }
        final String finalEmoji = emoji;
        String tournamentName = attachment.getFileName();
        InputStream it = null;
        try
        {
            it = attachment
                    .retrieveInputStream()
                    .get();
        }
        catch (Throwable e)
        {
            LOG.error(String.format("Something happened %s", UUID.randomUUID()), e);
            throw new RuntimeException(e);
        }
        StringBuilder content = new StringBuilder();
        byte[] buffer = new byte[1024];
        try
        {
            int length = 0;
            while ((length = it.read(buffer)) != -1)
            {
                content.append(new String(
                        buffer,
                        0,
                        length
                ));
            }
        }
        catch (IOException e)
        {
            String message = String.format(
                    "Failed to download file. %s",
                    UUID.randomUUID()
            );
            LOG.error(
                    message,
                    e
            );
            event
                    .getChannel()
                    .sendMessage(message)
                    .queue();
            return;
        }
        Tournament tournament = regularTournamentService.createTournament(
                tournamentName,
                fromDate,
                toDate
        );
        Role role = event
                .getGuild()
                .createRole()
                .setName(tournamentName)
                .complete();
        TextChannel announcementChannel = event
                .getGuild()
                .getTextChannelById(guild.getAnnouncementChannelId());
        Message message = announcementChannel
                .sendMessage(content.toString())
                .complete();
        DiscordTournament discordTournament = discordTournamentService.createDiscordTournament(
                tournament,
                guild,
                finalEmoji,
                role.getId(),
                content.toString(),
                message.getId()
        );
        message
                .addReaction(finalEmoji)
                .queue();
    }

    @Override
    public void onPrivateCommandReceived(PrivateMessageReceivedEvent event, String fixedMessage)
    {

    }
}
