import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.discord.message.tournament.host.DiscordTournamentHostCommandListener;

module eternal.witness.discord.tournament.host {
    uses DiscordGuildService;
    uses DiscordTournamentService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
    requires eternal.witness.discord.core;
    requires slf4j.api;
    provides EternalWitnessDiscordListener with DiscordTournamentHostCommandListener;
}
