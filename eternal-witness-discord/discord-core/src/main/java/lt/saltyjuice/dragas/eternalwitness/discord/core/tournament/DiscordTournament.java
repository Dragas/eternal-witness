package lt.saltyjuice.dragas.eternalwitness.discord.core.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;

public class DiscordTournament
{
    private long id;
    private Tournament tournament;
    private DiscordGuild discordGuild;
    private String roleSnowflake;
    private String emojiSnowflake;
    private String message;
    private String messageSnowflake;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Tournament getTournament()
    {
        return tournament;
    }

    public void setTournament(Tournament tournament)
    {
        this.tournament = tournament;
    }

    public DiscordGuild getDiscordGuild()
    {
        return discordGuild;
    }

    public void setDiscordGuild(DiscordGuild discordGuild)
    {
        this.discordGuild = discordGuild;
    }

    public String getRoleSnowflake()
    {
        return roleSnowflake;
    }

    public void setRoleSnowflake(String roleSnowflake)
    {
        this.roleSnowflake = roleSnowflake;
    }

    public String getEmojiSnowflake()
    {
        return emojiSnowflake;
    }

    public void setEmojiSnowflake(String emojiSnowflake)
    {
        this.emojiSnowflake = emojiSnowflake;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessageSnowflake()
    {
        return messageSnowflake;
    }

    public void setMessageSnowflake(String messageSnowflake)
    {
        this.messageSnowflake = messageSnowflake;
    }
}
