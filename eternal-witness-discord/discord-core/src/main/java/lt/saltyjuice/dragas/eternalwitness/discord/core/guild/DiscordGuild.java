package lt.saltyjuice.dragas.eternalwitness.discord.core.guild;

public class DiscordGuild
{
    private long id;
    private String guildId;
    private String backOfficeChannelId;
    private String announcementChannelId;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getGuildId()
    {
        return guildId;
    }

    public void setGuildId(String guildId)
    {
        this.guildId = guildId;
    }

    public String getBackOfficeChannelId()
    {
        return backOfficeChannelId;
    }

    public void setBackOfficeChannelId(String backOfficeChannelId)
    {
        this.backOfficeChannelId = backOfficeChannelId;
    }

    public String getAnnouncementChannelId()
    {
        return announcementChannelId;
    }

    public void setAnnouncementChannelId(String announcementChannelId)
    {
        this.announcementChannelId = announcementChannelId;
    }
}
