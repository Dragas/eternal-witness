package lt.saltyjuice.dragas.eternalwitness.discord.core.tournament;

import lt.saltyjuice.dragas.eternalwitness.api.model.Tournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuild;

public interface DiscordTournamentService
{
    DiscordTournament createDiscordTournament(
            Tournament tournament,
            DiscordGuild guild,
            String emoji,
            String role,
            String message,
            String messageSnowflake
    );

    DiscordTournament findByMessageSnowflake(String snowflake);

    void removeDiscordTournament(DiscordTournament tournament);
}
