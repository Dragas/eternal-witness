package lt.saltyjuice.dragas.eternalwitness.discord.core.guild;

public interface DiscordGuildService
{
    DiscordGuild createDiscordGuild(String guildId, String backOfficeChannelId, String announcementChannelId);

    DiscordGuild fetchById(long id);

    DiscordGuild fetchByGuildId(String guildId);

    DiscordGuild fetchByBackOfficeChannelId(String channelId);

    DiscordGuild fetchByAnnouncementChannelId(String channelId);

    void removeGuild(DiscordGuild guild);
}
