package lt.saltyjuice.dragas.eternalwitness.discord.core.player;

import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;

public interface DiscordPlayerQueueService
{
    DiscordPlayerQueueItem addToQueue(DiscordTournament tournament, String playerSnowflake);

    DiscordPlayerQueueItem fetchFromQueue(String guildSnowflake, String tournamentName, String playerSnowflake);

    void removePlayerQueueItem(DiscordPlayerQueueItem discordPlayerQueueItem);
}
