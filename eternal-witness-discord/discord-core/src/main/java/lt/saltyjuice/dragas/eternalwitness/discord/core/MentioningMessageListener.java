package lt.saltyjuice.dragas.eternalwitness.discord.core;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.MDC;

public abstract class MentioningMessageListener extends ListenerAdapter
{

    private static final String DISCORD_MESSAGE_ID = "discord.messageId";
    private static final String DISCORD_CHANNEL_ID = "discord.channelId";
    private static final String DISCORD_GUILD_ID = "discord.guildId";

    @Override
    public final void onGuildMessageReceived(GuildMessageReceivedEvent event)
    {
        MDC.put(DISCORD_GUILD_ID, event
                .getMessage()
                .getGuild()
                .getId());
        MDC.put(DISCORD_CHANNEL_ID, event
                .getMessage()
                .getChannel()
                .getId());
        MDC.put(DISCORD_MESSAGE_ID, event
                .getMessage()
                .getId());
        String message = event
                .getMessage()
                .getContentRaw();
        String mention = event
                .getJDA()
                .getSelfUser()
                .getId();
        mention = String.format("^<@!?%s> ",mention);
        String replaced = message.replaceFirst(mention, "");
        if (replaced.length() != message.length())
        {
            onGuildMessageReceivedWhileMentioned(event, replaced);
        }
        MDC.remove(DISCORD_GUILD_ID);
        MDC.remove(DISCORD_CHANNEL_ID);
        MDC.remove(DISCORD_MESSAGE_ID);
    }

    @Override
    public final void onPrivateMessageReceived(PrivateMessageReceivedEvent event)
    {
        MDC.put(DISCORD_CHANNEL_ID, event
                .getMessage()
                .getChannel()
                .getId());
        MDC.put(DISCORD_MESSAGE_ID, event
                .getMessage()
                .getId());
        String message = event
                .getMessage()
                .getContentRaw();
        String mention = event
                .getJDA()
                .getSelfUser()
                .getAsMention();
        if (message.startsWith(mention))
        {
            message = message.substring(mention.length());
            onPrivateMessageReceivedWhileMentioned(event, message);
        }
        MDC.remove(DISCORD_CHANNEL_ID);
        MDC.remove(DISCORD_MESSAGE_ID);
    }

    public void onGuildMessageReceivedWhileMentioned(GuildMessageReceivedEvent event, String fixedMessage)
    {

    }

    public void onPrivateMessageReceivedWhileMentioned(PrivateMessageReceivedEvent event, String fixedMessage)
    {

    }
}
