package lt.saltyjuice.dragas.eternalwitness.discord.core.player;

import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;

public class DiscordPlayerQueueItem
{
    private long id;
    private String playerSnowflake;
    private DiscordTournament tournament;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getPlayerSnowflake()
    {
        return playerSnowflake;
    }

    public void setPlayerSnowflake(String playerSnowflake)
    {
        this.playerSnowflake = playerSnowflake;
    }

    public DiscordTournament getTournament()
    {
        return tournament;
    }

    public void setTournament(DiscordTournament tournament)
    {
        this.tournament = tournament;
    }
}
