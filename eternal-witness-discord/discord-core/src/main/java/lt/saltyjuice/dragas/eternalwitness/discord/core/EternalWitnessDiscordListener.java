package lt.saltyjuice.dragas.eternalwitness.discord.core;

import net.dv8tion.jda.api.hooks.EventListener;

/**
 * Decorator interface for eternal witness specific event listener implementations
 */
public interface EternalWitnessDiscordListener extends EventListener
{

}


