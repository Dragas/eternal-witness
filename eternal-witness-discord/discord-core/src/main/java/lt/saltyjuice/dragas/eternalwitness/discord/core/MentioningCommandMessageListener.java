package lt.saltyjuice.dragas.eternalwitness.discord.core;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;

public abstract class MentioningCommandMessageListener extends MentioningMessageListener
{
    protected abstract String getCommand();

    @Override
    public final void onGuildMessageReceivedWhileMentioned(GuildMessageReceivedEvent event, String fixedMessage)
    {
        if (fixedMessage.startsWith(getCommand()))
        {
            fixedMessage = fixedMessage.substring(getCommand().length()).trim();
            onGuildCommandReceived(event, fixedMessage);
        }
    }

    public abstract void onGuildCommandReceived(GuildMessageReceivedEvent event, String fixedMessage);

    @Override
    public final void onPrivateMessageReceivedWhileMentioned(PrivateMessageReceivedEvent event, String fixedMessage)
    {
        if (fixedMessage.startsWith(getCommand()))
        {
            fixedMessage = fixedMessage.substring(getCommand().length());
            onPrivateCommandReceived(event, fixedMessage);
        }
    }

    public abstract void onPrivateCommandReceived(PrivateMessageReceivedEvent event, String fixedMessage);
}
