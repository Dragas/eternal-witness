open module eternal.witness.discord.core {
    requires static transitive net.dv8tion.jda;
    requires static org.hibernate.orm.core;
    requires transitive eternal.witness.api;
    requires slf4j.api;
    exports lt.saltyjuice.dragas.eternalwitness.discord.core;
    exports lt.saltyjuice.dragas.eternalwitness.discord.core.tournament;
    exports lt.saltyjuice.dragas.eternalwitness.discord.core.guild;
    exports lt.saltyjuice.dragas.eternalwitness.discord.core.player;
}
