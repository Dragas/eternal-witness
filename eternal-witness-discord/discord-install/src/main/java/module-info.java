import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.guild.message.install.DiscordInstallListener;

module eternal.witness.discord.install {
    requires transitive eternal.witness.discord.core;
    requires slf4j.api;
    uses lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
    provides EternalWitnessDiscordListener with DiscordInstallListener;
}
