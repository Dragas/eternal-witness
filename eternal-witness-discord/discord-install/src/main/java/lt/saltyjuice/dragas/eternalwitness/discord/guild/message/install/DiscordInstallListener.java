package lt.saltyjuice.dragas.eternalwitness.discord.guild.message.install;

import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.MentioningCommandMessageListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.guild.DiscordGuildService;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ServiceLoader;
import java.util.UUID;

public class DiscordInstallListener extends MentioningCommandMessageListener implements EternalWitnessDiscordListener
{
    private static final Logger LOG = LoggerFactory.getLogger(DiscordInstallListener.class);
    private final DiscordGuildService guildService;

    public DiscordInstallListener()
    {
        guildService = ServiceLoader
                .load(DiscordGuildService.class)
                .findFirst()
                .get();
    }

    @Override
    protected String getCommand()
    {
        return "install";
    }

    @Override
    public void onGuildCommandReceived(GuildMessageReceivedEvent event, String fixedMessage)
    {
        try
        {
            final String guildId = event
                    .getGuild()
                    .getId();
            guildService.fetchByGuildId(guildId);
            //event.getChannel().sendMessage("This guild has already installed Eternal Witness. Ignoring").queue();
            return;
        }
        catch (NoSuchEntityException e)
        {
            LOG.info("New guild is installing the tool!");
        }
        String backOfficeId = event
                .getMessage()
                .getChannel()
                .getId();
        String announcementChannelId = fixedMessage.replaceAll("[<>#]", "");
        final TextChannel backOfficeChannel = event.getChannel();
        final TextChannel targetChannel = event
                .getJDA()
                .getTextChannelById(announcementChannelId);
        final Guild guild = event
                .getGuild();
        // test message write to that channel
        // test role creation
        // test role assignment
        try
        {
            targetChannel
                    .sendMessage("Message write test.")
                    .complete()
                    .delete()
                    .queue();
            backOfficeChannel
                    .sendMessage("Message write test complete.")
                    .queue();
            backOfficeChannel
                    .sendMessage("Creating role EWTEST.")
                    .queue();
            Role testRole = guild
                    .createRole()
                    .setName("EWTEST")
                    .complete();
            backOfficeChannel
                    .sendMessage("Created role EWTEST.")
                    .queue();
            guild
                    .addRoleToMember(event.getMember(), testRole)
                    .complete();
            backOfficeChannel
                    .sendMessage(String.format("Assigned role to installer: %s", event
                            .getMember()
                            .getAsMention()))
                    .queue();
            backOfficeChannel.sendMessage("Cleaning the role up.").queue();
            testRole
                    .delete()
                    .queue();
            backOfficeChannel.sendMessage("Done").queue();
            guildService.createDiscordGuild(guild.getId(), backOfficeId, announcementChannelId);
            backOfficeChannel
                    .sendMessage(
                            "Installation complete. Eternal witness is now ready to use."
                    )
                    .queue();
        }
        catch (Throwable e)
        {
            UUID uuid = UUID.randomUUID();
            LOG.error(String.format("Failed to do something with uuid %s", uuid), e);
            event
                    .getJDA()
                    .openPrivateChannelById(
                            event
                            .getMember()
                            .getUser()
                            .getId())
                    .flatMap((it) -> it.sendMessage(String.format("Action failed with reason: %s. Send this uuid to Dragas#1580 `%s`", e.getMessage(), uuid)))
                    .queue();
        }
    }

    @Override
    public void onPrivateCommandReceived(PrivateMessageReceivedEvent event, String fixedMessage)
    {

    }
}
