package lt.saltyjuice.dragas.eternalwitness.discord.message.tournament.accepter;

import lt.saltyjuice.dragas.eternalwitness.api.exception.EternalWitnessExcepion;
import lt.saltyjuice.dragas.eternalwitness.api.exception.NoSuchEntityException;
import lt.saltyjuice.dragas.eternalwitness.api.model.*;
import lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
import lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
import lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
import lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.api.GenericDeckDownloaderFactory;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.deckstats.DeckstatsDeckDownloader;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.goldfish.MtgGoldfishDownloader;
import lt.saltyjuice.dragas.eternalwitness.deckdownloader.tappedout.TappedoutDeckDownloader;
import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueItem;
import lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournament;
import lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
import lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModuleFactory;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Deprecated
public class TournamentAcceptingListener extends ListenerAdapter implements EternalWitnessDiscordListener
{
    private static final Logger LOG = LoggerFactory.getLogger(TournamentAcceptingListener.class);
    private static final String NON_NUMERIC_REGEX_PATTERN = "^\\D+$";
    private final DiscordPlayerQueueService discordPlayerQueueService;
    private final DiscordTournamentService discordTournamentService;
    private final PlayerService playerService;
    private final DeckService decklistService;
    private final TournamentService tournamentService;
    private final GenericDeckDownloaderFactory deckDownloaderFactory;
    private final ExecutorService executorService;
    private final OkHttpClient okhttpClient;
    private final ValidatorModuleFactory validatorModuleFactory;
    private final FormatService formatService;

    public TournamentAcceptingListener()
    {
        discordTournamentService = ServiceLoader
                .load(DiscordTournamentService.class)
                .findFirst()
                .get();
        discordPlayerQueueService = ServiceLoader
                .load(DiscordPlayerQueueService.class)
                .findFirst()
                .get();
        playerService = ServiceLoader
                .load(PlayerService.class)
                .findFirst()
                .get();
        tournamentService = ServiceLoader
                .load(TournamentService.class)
                .findFirst()
                .get();
        decklistService = ServiceLoader
                .load(DeckService.class)
                .findFirst()
                .get();
        formatService = ServiceLoader
                .load(FormatService.class)
                .findFirst()
                .get();
        deckDownloaderFactory = new GenericDeckDownloaderFactory();
        executorService = Executors.newCachedThreadPool();
        okhttpClient = new OkHttpClient.Builder().build();
        deckDownloaderFactory.install(new TappedoutDeckDownloader(
                okhttpClient,
                executorService
        ));
        deckDownloaderFactory.install(new MtgGoldfishDownloader(
                okhttpClient,
                executorService
        ));
        deckDownloaderFactory.install(new DeckstatsDeckDownloader(
                okhttpClient,
                executorService
        ));
        validatorModuleFactory = new ValidatorModuleFactory();
        for (ValidatorModule module : ServiceLoader.load(ValidatorModule.class))
        {
            validatorModuleFactory.install(module);
        }
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event)
    {
        String message = event
                .getMessage()
                .getContentRaw();
        final PrivateChannel channel = event.getChannel();
        String[] args = message.split(" ");
        if (args.length != 4)
        {
            return;
        }
        String messageId = args[0];
        String tournamentName = args[1];
        String guildId = args[2];
        String deckUrl = args[3];
        if (messageId.matches(NON_NUMERIC_REGEX_PATTERN) || guildId.matches(NON_NUMERIC_REGEX_PATTERN))
        {
            return;
        }
        if (!deckUrl.startsWith("http"))
        {
            event
                    .getMessage()
                    .getChannel()
                    .sendMessage("Invalid deck url. Does it start with `http`?")
                    .queue();
            return;
        }
        String userId = event
                .getAuthor()
                .getId();
        DiscordPlayerQueueItem item = null;
        try {
            item = discordPlayerQueueService.fetchFromQueue(
                    guildId,
                    tournamentName,
                    userId
            );
        } catch (NoSuchEntityException e) {
            channel.sendMessage(String.format("You're not in line for %s.", tournamentName)).queue();
            LOG.warn(String.format("Player %s attempted to join %s, but failed", userId, tournamentName));
            throw e;
        }
        Future<Decklist> downloadedDecklistFuture = deckDownloaderFactory.download(deckUrl);
        Player player = null;
        try
        {
            // try fetching first because then it wont have to restart tx
            player = playerService.fetchPlayerByName(event
                    .getAuthor()
                    .getAsTag());
        }
        catch (NoSuchEntityException e)
        {
            // doesnt exist, creating
            player = playerService.createPlayer(
                    event
                            .getAuthor()
                            .getAsTag(),
                    event
                            .getAuthor()
                            .getId()
            );
        }
        Format traditionalFormat = formatService.fetchFormatByName(String.format(
                "%s-Traditional",
                guildId
        ));
        Decklist downloadedDecklist = null;
        try
        {
            downloadedDecklist = downloadedDecklistFuture.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            UUID uuid = UUID.randomUUID();
            channel
                    .sendMessage(String.format(
                            "Failed to fetch your deck. Please send %s to Dragas#1580.",
                            uuid
                    ))
                    .queue();
            if(e instanceof InterruptedException)
                Thread.currentThread().interrupt();
            throw new EternalWitnessExcepion(
                    String.format(
                            "Error id %s. Failed to download deck from url %s.",
                            uuid,
                            deckUrl
                    ),
                    e
            );
        }
        Validator validator = validatorModuleFactory.getValidator(traditionalFormat);
        downloadedDecklist
                .getCards()
                .stream()
                .forEach(this::replaceMtgCardWithDatabaseVersion);
        Set<ConstraintViolation<Decklist>> violations = validator.validate(downloadedDecklist);
        if (!violations.isEmpty())
        {
            channel
                    .sendMessage(String.format(
                            "Your deck violates the following constraints for %s-Traditional:",
                            guildId
                    ))
                    .queue();
            final String errorMessage = violations
                    .stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(
                            System.lineSeparator(),
                            "```",
                            "```"
                    ));
            channel
                    .sendMessage(errorMessage)
                    .queue();
            return;
        }
        DiscordTournament discordTournament = item
                .getTournament();
        Tournament tournament = discordTournament
                .getTournament();
        Decklist decklist = decklistService.createDecklist(
                player,
                UUID
                        .randomUUID()
                        .toString(),
                deckUrl
        );
        downloadedDecklist
                .getCards()
                .forEach(it -> decklistService.attachCard(
                        decklist,
                        it.getCard(),
                        it.getCount(),
                        it.isSideboard()
                ));
        try
        {
            Role role = event
                    .getJDA()
                    .getRoleById(discordTournament
                            .getRoleSnowflake());
            Guild guild = event
                    .getJDA()
                    .getGuildById(guildId);
            guild
                    .addRoleToMember(
                            event
                                    .getAuthor()
                                    .getId(),
                            role
                    )
                    .complete();
            tournamentService.attachPlayer(
                    tournament,
                    player,
                    decklist
            );
            discordPlayerQueueService.removePlayerQueueItem(item);
            event
                    .getMessage()
                    .getChannel()
                    .sendMessage(String.format(
                            "You have entered %s on %s",
                            tournamentName,
                            guild.getName()
                    ))
                    .queue();
            event
                    .getJDA()
                    .getTextChannelById(discordTournament
                            .getDiscordGuild()
                            .getBackOfficeChannelId())
                    .sendMessage(String.format(
                            "<@%s> has entered %s with %s",
                            event
                                    .getAuthor()
                                    .getId(),
                            tournament.getName(),
                            deckUrl
                    ))
                    .queue();
        }
        catch (Throwable e)
        {
            UUID uuid = UUID.randomUUID();
            LOG.error(
                    String.format(
                            "Some operation failed %s",
                            uuid
                    ),
                    e
            );
            event
                    .getJDA()
                    .getTextChannelById(discordTournament
                            .getDiscordGuild()
                            .getBackOfficeChannelId())
                    .sendMessage(String.format(
                            "<@%s> failed to enter %s. Reason: %s. \n Please send %s to Dragas#1580",
                            event
                                    .getAuthor()
                                    .getId(),
                            tournamentName,
                            e.getMessage(),
                            uuid
                    ))
                    .queue();
        }
    }

    private void replaceMtgCardWithDatabaseVersion(MtgDeckCard it)
    {
        String cardName = it
                .getCard()
                .getName();
        MtgCard card = decklistService.fetchCard(cardName);
        it.setCard(card);
    }

    private void noOpForEach(MtgDeckCard mtgDeckCard)
    {

    }
}
