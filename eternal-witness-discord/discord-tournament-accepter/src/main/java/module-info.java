import lt.saltyjuice.dragas.eternalwitness.discord.core.EternalWitnessDiscordListener;
import lt.saltyjuice.dragas.eternalwitness.discord.message.tournament.accepter.TournamentAcceptingListener;

module eternal.witness.discord.tournament.accepter {
    uses lt.saltyjuice.dragas.eternalwitness.discord.core.player.DiscordPlayerQueueService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.PlayerService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.TournamentService;
    uses lt.saltyjuice.dragas.eternalwitness.discord.core.tournament.DiscordTournamentService;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.DeckService;
    uses lt.saltyjuice.dragas.eternalwitness.validator.api.ValidatorModule;
    uses lt.saltyjuice.dragas.eternalwitness.api.service.FormatService;
    requires net.dv8tion.jda;
    requires slf4j.api;
    requires eternal.witness.discord.core;
    requires eternal.witness.deck.downloader.api;
    requires eternal.witness.deck.downloader.deckstats;
    requires eternal.witness.deck.downloader.goldfish;
    requires eternal.witness.deck.downloader.tappedout;
    requires eternal.witness.validator.api;
    requires okhttp3;
    provides EternalWitnessDiscordListener with TournamentAcceptingListener;
}
